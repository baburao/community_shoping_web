import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { NavigationService } from "../../../shared/services/navigation.service";
import { ThemeService } from '../../services/theme.service';
import { Subscription } from "rxjs";
import { portaladminmenus } from '../../../views/services/menu-element';
import { usermenus } from '../../../views/services/menu-element';
import { vendormenus } from '../../../views/services/menu-element';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';

// import PerfectScrollbar from 'perfect-scrollbar';

@Component({
  selector: 'app-sidebar-side',
  templateUrl: './sidebar-side.component.html'
})
export class SidebarSideComponent implements OnInit, OnDestroy, AfterViewInit {
  loginname;
  public portaladminmenus = portaladminmenus;
  public usermenus = usermenus;
  public vendormenus = vendormenus;

  // private sidebarPS: PerfectScrollbar;
  public menuItems: any[];
  public hasIconTypeMenuItem: boolean;
  public iconTypeMenuTitle: string;
  private menuItemsSub: Subscription;
  constructor(
    private navService: NavigationService,
    public themeService: ThemeService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private router: Router
  ) { }

  ngOnInit() {

    this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
    this.loginname = this.storage.get('loginname');
    if(this.loginname!="123"){
      if(this.storage.get('userrole')=="user"){
        this.menuItems = usermenus;
      }else if(this.storage.get('userrole')=="vendor"){
        this.menuItems = vendormenus;
      }else{
        this.menuItems = portaladminmenus;
      }
    }else{
      this.router.navigateByUrl('/login')
    }
    
    
    // this.menuItemsSub = this.navService.menuItems$.subscribe(menuItem => {
    //   this.menuItems = menus;
    //   //Checks item list has any icon type.
    //   this.hasIconTypeMenuItem = !!this.menuItems.filter(item => item.type === 'icon').length;
    // });
  }
  ngAfterViewInit() {
    // setTimeout(() => {
    //   this.sidebarPS = new PerfectScrollbar('#scroll-area', {
    //     suppressScrollX: true
    //   })
    // })
  }
  ngOnDestroy() {
    // if(this.sidebarPS) {
    //   this.sidebarPS.destroy();
    // }
    if(this.menuItemsSub) {
      this.menuItemsSub.unsubscribe()
    }
  }

}
