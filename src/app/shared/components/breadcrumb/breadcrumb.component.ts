import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { RoutePartsService } from '../../../shared/services/route-parts.service';
import { LayoutService } from '../../../shared/services/layout.service';
import { Subscription } from "rxjs";
import { filter } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { SessionsRoutes } from 'app/views/sessions/sessions.routing';
import {portaladminmenus,usermenus,vendormenus} from 'app/views/services/menu-element'

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {
  public portaladminmenus = portaladminmenus;
  public usermenus = usermenus;
  public vendormenus = vendormenus;
  routeParts:any[];
  routerEventSub: Subscription;
  // public isEnabled: boolean = true;
  constructor(
    private router: Router,
    private routePartsService: RoutePartsService, 
    private activeRoute: ActivatedRoute,
    public layout: LayoutService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    // @Inject(SessionsRoutes) private storage: StorageService
  ) {
    this.routerEventSub = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((routeChange) => {
        this.routeParts = this.routePartsService.generateRouteParts(this.activeRoute.snapshot);
        let role =this.storage.get('userrole')
        let loginname = this.storage.get('loginname');
        if(loginname!="123"){
          if(this.routeParts[0].url == 'profile'){
            this.routeParts.reverse().map((item, i) => {
              item.breadcrumb = this.parseText(item);
              item.urlSegments.forEach((urlSegment, j) => {
                if(j === 0)
                  return item.url = `${urlSegment.path}`;
                item.url += `/${urlSegment.path}`
              });
              if(i === 0) {
                return item;
              }
              // prepend previous part to current part
              item.url = `${this.routeParts[i - 1].url}/${item.url}`;
              return item;
            });
          }else if(this.routeParts[0].url == 'changepassword'){
            this.routeParts.reverse().map((item, i) => {
              item.breadcrumb = this.parseText(item);
              item.urlSegments.forEach((urlSegment, j) => {
                if(j === 0)
                  return item.url = `${urlSegment.path}`;
                item.url += `/${urlSegment.path}`
              });
              if(i === 0) {
                return item;
              }
              // prepend previous part to current part
              item.url = `${this.routeParts[i - 1].url}/${item.url}`;
              return item;
            });
          } 
          else if(this.storage.get('userrole')=="user"){
            let usrArr = usermenus.filter(x =>x.state == this.routeParts[0].url)
            if(usrArr.length>0){
              this.routeParts.reverse().map((item, i) => {
                item.breadcrumb = this.parseText(item);
                item.urlSegments.forEach((urlSegment, j) => {
                  if(j === 0)
                    return item.url = `${urlSegment.path}`;
                  item.url += `/${urlSegment.path}`
                });
                if(i === 0) {
                  return item;
                }
                // prepend previous part to current part
                item.url = `${this.routeParts[i - 1].url}/${item.url}`;
                return item;
              });
            }else{
              this.router.navigateByUrl('/login')
            }
          }else if(this.storage.get('userrole')=="vendor"){
            let vndArr = vendormenus.filter(x =>x.state == this.routeParts[0].url)
            if(vndArr.length>0){
              this.routeParts.reverse().map((item, i) => {
                item.breadcrumb = this.parseText(item);
                item.urlSegments.forEach((urlSegment, j) => {
                  if(j === 0)
                    return item.url = `${urlSegment.path}`;
                  item.url += `/${urlSegment.path}`
                });
                if(i === 0) {
                  return item;
                }
                // prepend previous part to current part
                item.url = `${this.routeParts[i - 1].url}/${item.url}`;
                return item;
              });
            }else{
              this.router.navigateByUrl('/login')
            }
          }else{
            let portArr = portaladminmenus.filter(x =>x.state == this.routeParts[0].url)
            if(portArr.length>0){
              this.routeParts.reverse().map((item, i) => {
                item.breadcrumb = this.parseText(item);
                item.urlSegments.forEach((urlSegment, j) => {
                  if(j === 0)
                    return item.url = `${urlSegment.path}`;
                  item.url += `/${urlSegment.path}`
                });
                if(i === 0) {
                  return item;
                }
                // prepend previous part to current part
                item.url = `${this.routeParts[i - 1].url}/${item.url}`;
                return item;
              });
            }else{
              this.router.navigateByUrl('/portaladmin')
            }
            
          }
        }else{
          this.router.navigateByUrl('/login')
        }
        // generate url from parts
        // this.routeParts.reverse().map((item, i) => {
        //   item.breadcrumb = this.parseText(item);
        //   item.urlSegments.forEach((urlSegment, j) => {
        //     if(j === 0)
        //       return item.url = `${urlSegment.path}`;
        //     item.url += `/${urlSegment.path}`
        //   });
        //   if(i === 0) {
        //     return item;
        //   }
        //   // prepend previous part to current part
        //   item.url = `${this.routeParts[i - 1].url}/${item.url}`;
        //   return item;
        // });
      });
  }

  ngOnInit() {}
  ngOnDestroy() {
    if(this.routerEventSub) {
      this.routerEventSub.unsubscribe()
    }
  }

  parseText(part) {
    part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
      var r = part.params[b];
      return typeof r === 'string' ? r : a;
    });
    return part.breadcrumb;
  }

}
