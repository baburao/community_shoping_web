import { Component, OnInit, EventEmitter, Input, Output, Renderer2 } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { LayoutService } from '../../services/layout.service';
import { TranslateService } from '@ngx-translate/core';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ApiService } from 'app/views/services/api.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-header-side',
  templateUrl: './header-side.template.html'
})
export class HeaderSideComponent implements OnInit {
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  loginname;
  profimgurl: string = '';
  pendingorder = ''; 
  
  disablenotify = false;
  @Input() notificPanel;
  currentLang = 'en';
  public availableLangs = [{
    name: 'English',
    code: 'en',
  }, {
    name: 'Spanish',
    code: 'es',
  }]
  public egretThemes;
  public layoutConf: any;
  constructor(
    private themeService: ThemeService,
    private layout: LayoutService,
    public translate: TranslateService,
    private renderer: Renderer2,
    private router: Router,
    private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,

  ) { }
  ngOnInit() {

    this.egretThemes = this.themeService.egretThemes;
    this.layoutConf = this.layout.layoutConf;
    this.translate.use(this.currentLang);
    this.loginname = this.storage.get('loginname');
    let profPic = this.storage.get('profileing');
    if (profPic == '' || profPic == null) {

      this.profimgurl = '';
    } else {
      this.profimgurl = this.service.staticURL + 'profile_images/' + profPic;
    }


    if (this.storage.get('userrole') == "vendor") {
      this.disablenotify = true;
      this.timeout();
    }
    else {
      this.disablenotify = false;
    }

  }

  setLang(e) {
    console.log(e)
    this.translate.use(this.currentLang);
  }
  changeTheme(theme) {
    this.themeService.changeTheme(this.renderer, theme);
  }
  // toggleNotific() {
  //   this.notificPanel.toggle();
  // }

  toggleSidenav() {
    if (this.layoutConf.sidebarStyle === 'closed') {
      return this.layout.publishLayoutChange({
        sidebarStyle: 'full'
      })
    }
    this.layout.publishLayoutChange({
      sidebarStyle: 'closed'
    })
  }

  // NAVIGATING TO ORDERS PAGE
  navorders() {
    this.router.navigateByUrl('/vendororders');
  }

  checkpendingstatus() {
    let vndObj = {
      "vendor_id": this.storage.get('vendor_id'),
    }
    this.service.post(this.service.vendorpendingcount, vndObj).subscribe(res => {
      // console.log(res);
      this.pendingorder = res[0].order_status_count;

    })
  }

  timeout() {
    setTimeout(() => {
      // console.log('Test');
    
      this.checkpendingstatus();
    }, 1000);
  }

  toggleCollapse() {
    // compact --> full
    if (this.layoutConf.sidebarStyle === 'compact') {
      return this.layout.publishLayoutChange({
        sidebarStyle: 'full'
      }, { transitionClass: true })
    }

    // * --> compact
    this.layout.publishLayoutChange({
      sidebarStyle: 'compact'
    }, { transitionClass: true })

  }
  

  logout() {
    let data = "123";
    this.storage.clear();
    clearTimeout(null);
    this.storage.set('loginname', data);
    this.router.navigateByUrl('/login');

    
  }
}