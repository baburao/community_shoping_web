import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

import { RoutePartsService } from "./shared/services/route-parts.service";
import { ThemeService } from './shared/services/theme.service';

import { filter } from 'rxjs/operators';
// import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  appTitle = 'Community Shoping';
  pageTitle = '';

  constructor(
    public title: Title, 
    private router: Router, 
    private activeRoute: ActivatedRoute,
    private routePartsService: RoutePartsService,
    private themeService: ThemeService,
    private renderer: Renderer2
  ) {const config = {
    apiKey: "AIzaSyCXe0J8_K5miy5f4A-FfNj__cXCZGbxgiE",
    authDomain: "community-buy.firebaseapp.com",
    databaseURL: "https://community-buy.firebaseio.com",
    projectId: "community-buy",
    storageBucket: "community-buy.appspot.com",
    messagingSenderId: "715071280121",
    appId: "1:715071280121:web:f8ba8fb9ef3c4227482328",
    measurementId: "G-01QD1YQYTQ"
};
// firebase.initializeApp(config);  
}

  ngOnInit() {
    this.changePageTitle();
  }

  ngAfterViewInit() {
    this.themeService.applyMatTheme(this.renderer)
  }
  changePageTitle() {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((routeChange) => {
      var routeParts = this.routePartsService.generateRouteParts(this.activeRoute.snapshot);
      if (!routeParts.length)
        return this.title.setTitle(this.appTitle);
      // Extract title from parts;
      // this.pageTitle = routeParts
      //                 .reverse()
      //                 .map((part) => part.title )
      //                 .reduce((partA, partI) => {return `${partA} > ${partI}`});
      // this.pageTitle += ` | ${this.appTitle}`;
      this.pageTitle += `  ${this.appTitle}`;
      this.title.setTitle("Community Shoping");
    });
  }
  
}
