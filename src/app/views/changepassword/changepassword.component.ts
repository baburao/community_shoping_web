//NAME : BABURAO M
//DESCRIPTION : CHANGE PASSWORD
//DATE : 08/05/2019


import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSelect, MatDialog } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { Inject, Injectable } from '@angular/core';
@Component({
  selector: 'app-login',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangePasswordComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;


  oldpassword;
  newpassword;
  conformpassword;
  password;
  email_id;

  tableData: any;
  status;   // private message: MessageService

  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private message: MessageService,
    private toast: ToasterService
  ) { }

  ngOnInit() {

  }

  //change password
  changePassword() {

    if (this.oldpassword == "" || this.oldpassword == undefined) {
      this.toast.showwarning(this.message.oldPassword);
      return
    } else if (this.newpassword == "" || this.newpassword == undefined) {
      this.toast.showwarning(this.message.newPassword);
      return
    } else if (this.conformpassword == "" || this.conformpassword == undefined) {
      this.toast.showwarning(this.message.confirmPassword);
      return
    } else if (this.newpassword != this.conformpassword) {
      this.toast.showwarning(this.message.pswdmismatch);
    } else {
      let changepwdbj = {
        email_id: this.storage.get('emailid'),
        old_password: this.oldpassword,
        new_password: this.newpassword,
        new_re_password: this.conformpassword
      }
      if (this.storage.get('userrole') == "user") {
        this.service.post(this.service.changeuserpassword, changepwdbj).subscribe((res) => {
          if (res == "Password already exists") {
            this.toast.showwarning(this.message.pswdexists);
          } else {
            this.email_id = '';
            this.toast.showsuccess(this.message.changePassword);
          }
        });
      } else if (this.storage.get('userrole') == "vendor") {
        this.service.post(this.service.changevendorpassword, changepwdbj).subscribe((res) => {
          if (res == "Password already exists") {
            this.toast.showwarning(this.message.pswdexists);
          } else {
            this.email_id = '';
            this.toast.showsuccess(this.message.changePassword);
          }
        });
      } else {
        this.service.post(this.service.changeportaladminpassword, changepwdbj).subscribe((res) => {
          if (res == "Password already exists") {
            this.toast.showwarning(this.message.pswdexists);
          } else {
            this.email_id = '';
            this.toast.showsuccess(this.message.changePassword);
          }
        });
      }
    }
  }
}
