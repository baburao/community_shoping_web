import { Routes } from '@angular/router';

import { ChangePasswordComponent } from './changepassword.component';


export const ChangePasswordRoutes: Routes = [
  { path: '', component: ChangePasswordComponent }
];