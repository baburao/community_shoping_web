//NAME : BABURAO M
//DESCRIPTION : USER AND VENDOR PROFILE DATA
//DATE : 09/05/2019

import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from '../services/toaster.service';
import { MessageService } from '../services/message.service';
import { ValidatorService } from '../services/validators.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  prodImg: any;
  count = 0;
  states: any;
  cities: any; 
  selectedFile: File = null;
  fd = new FormData();
  imgURL: string = '';
  upImgName: string;
  prtUpImg: string;
  upPrtlImgName: string;
  userprofiledata;
  first_name;
  last_name;
  phone_no;
  email_id;
  block_name;
  community_name;
  flat_no;
  address;
  gst_no;
  pan_no;
  pincode;
  state_id;
  city_id;
  vendordata: boolean = true;
  userdata: boolean = true;
  adminProfileData: boolean = true;
  userid;
  vendorid;
  adminid;
  communitydata;
  community_id;
  selectedCity;
  filesToUpload;
  imageshow: boolean = true;
  constructor(private router: Router, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private spinner: NgxSpinnerService, private toast: ToasterService, private message: MessageService,
    private validations: ValidatorService,
  ) { }

  ngOnInit() {
    this.imgURL = '';
    this.spinner.show();
    this.getcities();
    this.getstates();
    this.getCommunityData();
    var userrole = this.storage.get('userrole');
    if (userrole == "user") {
      let userobj = {
        user_id: this.storage.get('user_id')
      }
      this.service.post(this.service.getuserbyid, userobj).subscribe((res) => {
        this.userid = res[0]['user_id']
        this.first_name = res[0]['first_name'];
        this.last_name = res[0]['last_name'];
        this.phone_no = res[0]['phone_no'];
        this.email_id = res[0]['email_id'];
        this.block_name = res[0]['block_name'];
        this.community_id = res[0]['community_id'];
        this.flat_no = res[0]['flat_no'];
        this.upImgName = res[0]['user_image'];
        this.prodImg = this.upImgName;
        if ((this.upImgName == "") || (this.upImgName == null)) {
          this.imgURL = "";
          this.imageshow = true;
        }
        else {
          this.imgURL = this.service.staticURL + 'profile_images/' + this.upImgName;
          this.imageshow = false;
        }

        // this.imgURL =  this.service.staticURL +res[0]['user_image']; 

        this.vendordata = true;
        this.adminProfileData = true;
        this.userdata = false;
        this.spinner.hide();
      });
    } else if (userrole == "vendor") {

      let vendorobj = {
        vendor_id: this.storage.get('vendor_id')
      }
      this.service.post(this.service.getvendorbyid, vendorobj).subscribe((res) => {
        this.vendorid = res[0]['vendor_id'];
        this.first_name = res[0]['first_name'];
        this.last_name = res[0]['last_name'];
        this.phone_no = res[0]['phone_no'];
        this.email_id = res[0]['email_id'];
        this.address = res[0]['address'];
        this.gst_no = res[0]['gst_no'];
        this.pan_no = res[0]['pan_no'];
        this.pincode = res[0]['pincode'];
        this.state_id = res[0]['state_id'];
        this.city_id = res[0]['city_id'];
        this.upImgName = res[0]['vendor_image'];
        if ((this.upImgName == "") || (this.upImgName == null)) {
          this.imgURL = "";
          this.imageshow = true;
        }
        else {
          this.imgURL = this.service.staticURL + 'profile_images/' + this.upImgName;
          this.imageshow = false;
        }
        this.userdata = true;
        this.adminProfileData = true;
        this.vendordata = false;
        this.spinner.hide();
      });
    } else {
      let portalAdminObj = {
        admin_id: this.storage.get('portal_admin_id')
      }
      this.service.post(this.service.getportaladminbyid, portalAdminObj).subscribe(res => {
        this.adminid = res[0]['admin_id']
        this.first_name = res[0]['first_name'];
        this.last_name = res[0]['last_name'];
        this.phone_no = res[0]['phone_no'];
        this.email_id = res[0]['email_id'];
        this.address = res[0]['address'];
        this.pan_no = res[0]['pan_no'];
        this.pincode = res[0]['pincode'];
        this.state_id = res[0]['state_id'];
        this.city_id = res[0]['city_id'];
        this.upImgName = res[0]['portal_user_image'];
        this.prodImg = this.upImgName;
        if ((this.upImgName == "") || (this.upImgName == null)) {
          this.imgURL = "";
          this.imageshow = true;
        }
        else {
          this.imgURL = this.service.staticURL + 'profile_images/' + this.upImgName;
          this.imageshow = false;
        }
        this.userdata = true;
        this.vendordata = true;
        this.adminProfileData = false;
        this.spinner.hide();
      });
    }
  }

  // UPDATING USER PROFILE
  updateUsrProfile(userid, first_name, last_name, phone_no, community_id, block_name, flat_no) {
    if (first_name == '' || first_name == undefined) {
      this.toast.showwarning(this.message.usrftName)
      return;
    } else if (last_name == '' || last_name == undefined) {
      this.toast.showwarning(this.message.usrltName)
      return;
    } else if (phone_no == '' || phone_no == undefined) {
      this.toast.showwarning(this.message.usrNum)
      return;
    } else if (community_id == '' || community_id == undefined) {
      this.toast.showwarning(this.message.usrCmtId)
      return;
    } else if (block_name == '' || block_name == undefined) {
      this.toast.showwarning(this.message.usrBlkName)
      return;
    } else if (flat_no == '' || flat_no == undefined) {
      this.toast.showwarning(this.message.usrFltNum)
      return;
    } else {
      let usrObj = {
        "user_id": userid,
        "first_name": first_name,
        "last_name": last_name,
        "phone_no": phone_no,
        "block_name": block_name,
        "community_id": community_id,
        "flat_no": flat_no,
        "updated_by": this.storage.get('user_id'),
        "upImgName": this.upImgName,
      }
      if (this.upImgName == this.prodImg) {
        this.prtUpImg = this.upImgName
        this.service.post(this.service.updateusrprofile, usrObj).subscribe(res => {
          if (res == 'Updated') {
            this.toast.showsuccess(this.message.usrprofUpdated);
            let sethdimgURL = this.service.staticURL + 'profile_images/' + this.prodImg
            let elem: HTMLElement = document.getElementById('hdusrprof');
            elem.setAttribute('src', sethdimgURL)
            this.storage.set('profileing', this.prodImg);
          }
        })
      } else {
        this.service.post(this.service.profinsertimg, this.fd).subscribe(response => {
          if (response == 'Updated') {
            let sethdimgURL = this.service.staticURL + 'profile_images/' + this.prodImg
            let elem: HTMLElement = document.getElementById('hdusrprof');
            elem.setAttribute('src', sethdimgURL)
            this.storage.set('profileing', this.prodImg);
            this.service.post(this.service.updateusrprofile, usrObj).subscribe(res => {
              if (res == 'Updated') {
                this.toast.showsuccess(this.message.usrprofUpdated);
              }
            })
          }
        })
      }
    }
  }
  // UPDATING PORTAL PROFILE
  updatePrtlProfile(adminid, first_name, last_name, phone_no, address, pincode, state_id, city_id) {
    let prtlObj = {
      first_name: first_name,
      last_name: last_name,
      phone_no: phone_no,
      address: address,
      state_id: state_id,
      city_id: city_id,
      pincode: pincode,
      updated_by: this.storage.get('portal_admin_id'),
      admin_id: adminid,
      prtlImg: this.prodImg
    }
    if (this.upImgName == this.prodImg) {
      this.prtUpImg = this.upImgName
      this.service.post(this.service.upprtladminprofile, prtlObj).subscribe(res => {
        if (res == 'Updated') {
          this.toast.showsuccess(this.message.prtlprofUpdated);

          let sethdimgURL = this.service.staticURL + 'profile_images/' + this.prodImg
          let elem: HTMLElement = document.getElementById('hdusrprof');
          elem.setAttribute('src', sethdimgURL)
          this.storage.set('profileing', this.prodImg);
        }
      })
    } else {
      this.service.post(this.service.profinsertimg, this.fd).subscribe(response => {
        if (response == 'Updated') {
          let sethdimgURL = this.service.staticURL + 'profile_images/' + this.prodImg
          let elem: HTMLElement = document.getElementById('hdusrprof');
          elem.setAttribute('src', sethdimgURL)
          this.storage.set('profileing', this.prodImg);
          this.service.post(this.service.upprtladminprofile, prtlObj).subscribe(res => {
            if (res == 'Updated') {
              this.toast.showsuccess(this.message.prtlprofUpdated);
            }
          })
        }
      })
    }

  }
  //GER COMMUNITITY DATA
  getCommunityData() {
    this.service.post(this.service.getallcommunity, '').subscribe((res) => {
      this.communitydata = res;
    });
  }

  //GET STATES
  getstates() {

    this.service.post(this.service.getallstate, '').subscribe((res) => {
      this.states = res;
    });
  }
  //GET CITYS BY STATE ID
  getcitiesbystateId(stateid) {
    this.state_id = stateid
    let stateobj = {
      state_id: stateid,
    }
    this.service.post(this.service.getcitiesbystate, stateobj).subscribe((res) => {
      this.cities = res;
    });
  }
  // GET CITIES
  getcities() {
    this.service.post(this.service.getallcities, '').subscribe((res) => {
      this.cities = res;
    });
  }

  //GET COMMUNITY ID
  getcommunityid(communityid) {
    this.community_id = communityid
  }


  openFile(event) {
    this.imageshow = false;
    this.filesToUpload = event.target.files;
    const files: Array<File> = this.filesToUpload;
    let time = new Date();
    let timestamp = time.getTime();
    this.prodImg = timestamp + '-' + files[0]['name'];
    this.storage.set('profpic', this.prodImg)
    if (files[0] == null) {
      //this.toast.showwarning('Please choose a file.')
      return;
    }
    var reader = new FileReader();
    reader.onload = (val: any) => {
      this.imgURL = val.target.result;
    }
    reader.readAsDataURL(event.target.files[0])

    this.fd.append("uploads", files[0], this.prodImg);
  }
  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }

  // CHECKING EMAIL VALIDATION
  chkemail(event): boolean {
    return this.validations.validateEmail(event)
  }
  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validations.nosplChar(event)
  }
  
}
