import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxLoadingModule } from 'ngx-loading';
//mport {MessageService } from '../services/message.service';
//import { ToasterService } from '../services/toaster.service';
import {
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,  
  MatChipsModule,
  MatListModule,
  MatGridListModule,
  MatTabsModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,

  MAT_DIALOG_DEFAULT_OPTIONS,
  MAT_DATE_LOCALE
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProfileComponent } from './profile.component';
import { ProfileRoutes } from './profile.routing';
import { HeaderSideComponent } from 'app/shared/components/header-side/header-side.component';
// import { LocalStorageService } from 'ngx-webstorage';
// import {BrowserModule} from '@angular/platform-browser';
// import {NgxWebstorageModule} from 'ngx-webstorage';
// import { CommonDirectivesModule } from './sdirectives/common/common-directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    MatButtonModule,  
    MatChipsModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    // LocalStorageService,
    // BrowserModule,
    // NgxWebstorageModule.forRoot(),
    RouterModule.forChild(ProfileRoutes),
    NgxLoadingModule.forRoot({})
  ],
  declarations: [ProfileComponent],
  providers: [
    
    //MessageService
   
  ],
})
export class ProfileModule { }