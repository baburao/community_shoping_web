


import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';

import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';

@Component({
  selector: 'app-paymentsattlement',
  templateUrl: './paymentsattlement.component.html',
  styleUrls: ['./paymentsattlement.component.css'],
  providers: [DatePipe],
})
export class PaymentSattlementComponent implements OnInit {
  payObj: any;
  payArray: any;
  orderDetArray: any;
  totalAmouunt: any;
  counter = 0;
  allpaymentData: any;
  paymentData;
  paymentTables: boolean = false;
  noRecords: boolean = true;
  paymenttypeObj = {
    chequeno: '',
    acno: '',
    bankacname: '',
    bankname: '',
    branchname: '',
    ifsccode: '',
    depositedate: '',
    amount: '',
    description: '',
    refno: '',
    vendor_id: '',
    payment_type_id: ''
  }

  constructor(private router: Router, private service: ApiService,
    public dialogue: MatDialog,
    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  //USER ORDERS DISPLAY TABLE
  displayedColumns: string[] = ['vendor_name', 'amount', 'Actions'];

  ngOnInit() {
    this.spinner.show();
    this.getpaymentdata();
  }

  // GET PAYMENT AMOUNT 
  getpaymentdata() {
    this.payArray = [];
    this.service.post(this.service.getallpayments, '').subscribe(res => {
      // GETTING BALANCE AMOUNT
      this.service.post(this.service.getbalanceamt, '').subscribe(response => {
        response.forEach(element => {
          this.payObj = [];
          this.counter = 0;
          res.forEach(elem => {
            if (element.vendor_id == elem.vendor_id) {
              this.counter++
              let finaleAmt = (parseFloat(element.balance_amount) + parseFloat(elem.price)).toString();
              let payableAmt = parseFloat(finaleAmt).toFixed(2)
              this.payObj = {
                "vendor_name": element.vendor_name,
                "balance_amount": payableAmt,
                "vendor_id": element.vendor_id
              }
              this.payArray.push(this.payObj)
            }
          });
          if (this.counter == 0 && element.balance_amount != 0) {
            this.payObj = {
              "vendor_name": element.vendor_name,
              "balance_amount": element.balance_amount,
              "vendor_id": element.vendor_id
            }
            this.payArray.push(this.payObj)
          }
        });
        if (this.payArray.length != 0) {
          this.paymentData = this.payArray;
          this.allpaymentData = this.payArray;
          this.paymentData = new MatTableDataSource(this.payArray);
          this.paymentData.paginator = this.paginator;
          this.spinner.hide();
          this.paymentTables = false;
        } else {
          this.noRecords = false;
          this.paymentTables = true;
          this.spinner.hide();
        }
      })

    })
  }
  //  PAYMENT MODAL POPUP OPENS
  payMoney(vendorid,balenceAmt) { 
    this.orderDetArray = [];
    this.totalAmouunt = 0;
    let paydet = {
      "vendor_id": vendorid
    }
    this.service.post(this.service.getpaymentdetailsbyid, paydet).subscribe(res => {
      for (var i = 0; i < res.length; i++) {
        this.orderDetArray.push(res[i].order_detail_id)
        this.totalAmouunt = (parseFloat(res[i].price) + parseFloat(this.totalAmouunt)).toFixed(2)
      }
      // this.totalAmouunt = parseFloat(this.totalAmouunt) + parseFloat(balenceAmt)

      const dialogRef = this.dialog.open(PaymentSettlementsDialog, {
        width: '750px',
        disableClose: true,
        data: {
          chequeno: this.paymenttypeObj.chequeno,
          acno: this.paymenttypeObj.acno,
          bankacname: this.paymenttypeObj.bankacname,
          bankname: this.paymenttypeObj.bankname,
          branchname: this.paymenttypeObj.branchname,
          ifsccode: this.paymenttypeObj.ifsccode,
          depositedate: this.paymenttypeObj.depositedate,
          amount: balenceAmt,
          description: this.paymenttypeObj.description,
          refno: this.paymenttypeObj.refno,
          vendor_id: vendorid,
          order_detail_id: this.orderDetArray,
          totAmt: this.totalAmouunt,
          payAmt : balenceAmt
        }

      });
      dialogRef.afterClosed().subscribe(res => {
        this.getpaymentdata();
      })
      let userpage = "add";
      this.storage.set('userpage', userpage);
      this.paymenttypeObj.vendor_id = vendorid;
    })
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allpaymentData.filter(function (categiry) {
        return (
          categiry.vendor_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.balance_amount.indexOf(event) > -1
        )
      });
      if (temp.length !== 0) {
        this.paymentData = new MatTableDataSource(temp);
        this.paymentData.paginator = this.paginator;
        this.paymentTables = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.paymentTables = true;
        this.noRecords = false;
        this.paymentData = new MatTableDataSource(temp);
        this.paymentData.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.paymentData = this.allpaymentData;
      this.paymentData = new MatTableDataSource(this.allpaymentData);
      this.paymentData.paginator = this.paginator;
      this.paymentTables = false;
      this.noRecords = true;
    }
  }
}


@Component({
  selector: 'paymentsettlements',
  templateUrl: 'paymentsettlements.html',
  providers: [DatePipe],
})
export class PaymentSettlementsDialog {

  constructor(private service: ApiService,
    private datePipe: DatePipe,
    private validations: ValidatorService,
    private toast: ToasterService, private message: MessageService,
    public dialogRef: MatDialogRef<PaymentSettlementsDialog>,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }
  userpage;
  status;
  chequehide: boolean;
  onlinehide: boolean;
  paymenttype;
  paymenttypeObj = {
    chequeno: '',
    acno: '',
    bankacname: '',
    bankname: '',
    branchname: '',
    ifsccode: '',
    depositedate: '',
    amount: '',
    description: '',
    refno: '',
    vendor_id: '',
    payment_type_id: '',
    updated_by: '',
    order_detail_id: [],
    totAmt: 0
  }
  ngOnInit() {
    this.storage.set('paymenttypeid', '2');
    this.chequehide = false;
    this.onlinehide = true;
    this.paymenttype = "cheque";
    this.userpage = this.storage.get('userpage');
    if (this.userpage == "add") {
      this.data.chequeno = '';
      this.data.acno = '';
      this.data.bankacname = '';
      this.data.bankname = '';
      this.data.branchname = '';
      this.data.ifsccode = '';
      this.data.depositedate = '';
      this.data.amount;
      this.data.description = '';
      this.data.refno = '';
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  statuschange(status, amount) {
    if (status == "cheque") {
      this.paymenttype = status;
      this.chequehide = false;
      this.onlinehide = true;
    } else {
      this.chequehide = true;
      this.onlinehide = false;
      this.paymenttype = status;
    }
  }

  payment(res, amt) {
    if (this.paymenttype == "cheque") {

      if (res.chequeno == null || res.chequeno == '') {
        this.toast.showwarning(this.message.chequeno);
        return
      } else if (res.acno == null || res.acno == '') {
        this.toast.showwarning(this.message.acno);
        return
      } else if (res.bankacname == null || res.bankacname == '') {
        this.toast.showwarning(this.message.bankAcName);
        return
      } else if (res.bankname == null || res.bankname == '') {
        this.toast.showwarning(this.message.bankName);
        return
      } else if (res.branchname == null || res.branchname == '') {
        this.toast.showwarning(this.message.BranchName);
        return
      } else if (res.ifsccode == null || res.ifsccode == '') {
        this.toast.showwarning(this.message.ifscCode);
        return
      } else if (res.depositedate == null || res.depositedate == '') {
        this.toast.showwarning(this.message.depositeDate);
        return
      } else if (amt == null || amt == '' || amt == 0) {
        this.toast.showwarning(this.message.amount);
        return
      } else if (res.description == null || res.description == '') {
        this.toast.showwarning(this.message.paydescp);
        return
      } else if (amt > res.payAmt) {
        this.toast.showwarning(this.message.payexcess);
        return;
      } else {
        this.paymenttypeObj.refno = '';
        this.paymenttypeObj.bankname = res.bankname;
        this.paymenttypeObj.branchname = res.branchname;
        this.paymenttypeObj.ifsccode = res.ifsccode;
        this.paymenttypeObj.depositedate = this.datePipe.transform(res.depositedate, 'yyyy-MM-dd')
        this.paymenttypeObj.amount = res.amount;
        this.paymenttypeObj.description = res.description;
        this.paymenttypeObj.bankacname = res.bankacname;
        this.paymenttypeObj.chequeno = res.chequeno;
        this.paymenttypeObj.acno = res.acno;
        this.paymenttypeObj.vendor_id = res.vendor_id;
        this.paymenttypeObj.updated_by = this.storage.get('portal_admin_id');
        this.paymenttypeObj.order_detail_id = res.order_detail_id;
        this.paymenttypeObj.totAmt = res.totAmt;
        this.paymenttypeObj.payment_type_id = '2';
        this.service.post(this.service.insertadminpayments, this.paymenttypeObj).subscribe(res1 => {
          if (res1 == 'Inserted') {
            this.toast.showsuccess(this.message.paysuccess);
            this.onNoClick();
          }
        });
      }

    } else {
      if (res.refno == null || res.refno == '') {
        this.toast.showwarning(this.message.refno);
        return
      } else if (res.depositedate == null || res.depositedate == '') {
        this.toast.showwarning(this.message.depositeDate);
        return
      } else if (amt == null || amt == '' || amt == 0) {
        this.toast.showwarning(this.message.amount);
        return
      } else if (res.description == null || res.description == '') {
        this.toast.showwarning(this.message.paydescp);
        return
      } else if (amt > res.payAmt) {
        this.toast.showwarning(this.message.payexcess);
        return;
      } else {
        this.paymenttypeObj.chequeno = '';
        this.paymenttypeObj.acno = '';
        this.paymenttypeObj.bankacname = '';
        this.paymenttypeObj.bankname = '';
        this.paymenttypeObj.branchname = '';
        this.paymenttypeObj.ifsccode = '';
        this.paymenttypeObj.refno = res.refno;
        this.paymenttypeObj.depositedate = this.datePipe.transform(res.depositedate, 'yyyy-MM-dd')
        this.paymenttypeObj.amount = res.amount;
        this.paymenttypeObj.description = res.description;
        this.paymenttypeObj.vendor_id = res.vendor_id;
        this.paymenttypeObj.updated_by = this.storage.get('portal_admin_id');
        this.paymenttypeObj.order_detail_id = res.order_detail_id;
        this.paymenttypeObj.totAmt = res.totAmt;
        this.paymenttypeObj.payment_type_id = '3';
        this.service.post(this.service.insertadminpayments, this.paymenttypeObj).subscribe(res1 => {
          if (res1 == 'Inserted') {
            this.toast.showsuccess(this.message.paysuccess);
            this.onNoClick();
          }
        });
      }
    }
  }

  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }

  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validations.nosplChar(event)
  }
  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }
}

export interface DialogData {
  chequeno;
  acno;
  bankacname;
  bankname;
  branchname;
  ifsccode;
  depositedate;
  amount;
  description;
  refno;
  updateby;
  updateddate;
  totAmt;
  payAmt;
}
