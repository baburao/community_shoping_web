import { Routes } from '@angular/router';

import { PaymentSattlementComponent } from './paymentsattlement.component';


export const PaymentSattlementRoutes: Routes = [
  { path: '', component: PaymentSattlementComponent }
];