import { Component, OnInit, Inject, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { ApiService } from '../../services/api.service';
import { MessageService } from '../../services/message.service';
import { ToasterService } from '../../services/toaster.service';
import { MatTableDataSource, MatDialog, MatDialogRef, MatPaginator, MAT_DIALOG_DATA, MatSort } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HeaderSideComponent } from 'app/shared/components/header-side/header-side.component';
@Component({
  selector: 'app-vendororders',
  templateUrl: './vendororders.component.html',
  styleUrls: ['./vendororders.component.scss']
})
export class VendorordersComponent implements OnInit {
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  vendorId: '';
  searchValue: string = '';
  vendorPndOrders: any;
  allvndPndOrdList: any;
  vendoracptOrders: any;
  allvndacptordlist: any;
  vndshporder: any;
  allvndshpordList: any;
  displayedColumns: string[] = ['User Name', 'Items', 'Amount', 'view_Orders']
  displayedColumns1: string[] = ['User Name', 'Items', 'Amount', 'view_Orders']
  records: boolean = false;
  norecords: boolean = true;
  records1: boolean = false;
  norecords1: boolean = true; 
  records2: boolean = false;
  norecords2: boolean = true;
  constructor(private loader: AppLoaderService, private service: ApiService, public dialog: MatDialog, private spinner: NgxSpinnerService,
    private message: MessageService,
    private toast: ToasterService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }
  ngOnInit() {
    this.spinner.show();

    this.vendorId = this.storage.get('vendor_id');

    this.getVndPendingOrders();
    this.getVndAcceptedOrders();
    this.getVndShippedOrders();
  }
  // GETTING VENDOR ORDERS
  getVndPendingOrders() {
    let vndObj = {
      "vendor_id": this.vendorId
    }
    this.service.post(this.service.pendingvendororders, vndObj).subscribe(res => {
      this.vendorPndOrders = new MatTableDataSource(res)
      this.allvndPndOrdList = res;
      this.vendorPndOrders.paginator = this.paginator.toArray()[0];
      this.vendorPndOrders.sort = this.sort.toArray()[0];
      if (res.length != 0) {
        this.records = false;
        this.norecords = true;
      }
      else {
        this.records = true;
        this.norecords = false;
      }
      this.spinner.hide();
    })
  }

  // GETTING ACCEPTED  VENDOR ORDERS
  getVndAcceptedOrders() {
    let vndObj = {
      "vendor_id": this.vendorId
    }
    this.service.post(this.service.acceptedvendororders, vndObj).subscribe(res => {
      this.vendoracptOrders = new MatTableDataSource(res)
      this.allvndacptordlist = res;
      this.vendoracptOrders.paginator = this.paginator.toArray()[1];
      this.vendoracptOrders.sort = this.sort.toArray()[1];
      if (res.length != 0) {
        this.records1 = false;
        this.norecords1 = true;
      }
      else {
        this.records1 = true;
        this.norecords1 = false;
      }
      this.spinner.hide();
    })
  }



  // GETTING SHIPPED  VENDOR ORDERS
  getVndShippedOrders() {
    let vndObj = {
      "vendor_id": this.vendorId
    }
    this.service.post(this.service.shippedvendororders, vndObj).subscribe(res => {
      this.vndshporder = new MatTableDataSource(res)
      this.allvndshpordList = res;
      this.vndshporder.paginator = this.paginator.toArray()[2];
      this.vndshporder.sort = this.sort.toArray()[2];
      if (res.length != 0) {
        this.records2 = false;
        this.norecords2 = true;
      }
      else {
        this.records2 = true;
        this.norecords2 = false;
      }
      this.spinner.hide();
    })
  }

  // DISPLAYING ORDERS by USER NAME
  showList(usrId, status) {
    this.spinner.show();
    let usrObj = {
      "vendor_id": this.vendorId,
      "user_id": usrId
    }
    if (status == "Pending") {
      this.service.post(this.service.vendorpndordersbyuserId, usrObj).subscribe(res => {
        const dialogRef = this.dialog.open(VendorordersmodalComponent, {
          width: '1050px',
          disableClose: true,
          autoFocus: true,
          data: {
            ordstatus: 'pending',
            ordArray: res
          }
        })
        dialogRef.afterClosed().subscribe(res => {
          this.getVndPendingOrders();
        })
        this.spinner.hide();
      })
    } else if (status == "Accepted") {
      this.service.post(this.service.vendoracptordersbyuserId, usrObj).subscribe(res => {
        const dialogRef = this.dialog.open(VendorordersmodalComponent, {
          width: '1050px',
          disableClose: true,
          autoFocus: true,
          data: {
            ordstatus: 'accepted',
            ordArray: res
          }
        })
        dialogRef.afterClosed().subscribe(res => {
          this.getVndAcceptedOrders();
        })
        this.spinner.hide();
      })
    } else {
      this.service.post(this.service.vendorshpordersbyuserId, usrObj).subscribe(res => {
        const dialogRef = this.dialog.open(VendorordersmodalComponent, {
          width: '1050px',
          disableClose: true,
          autoFocus: true,
          data: {
            ordstatus: 'shipped',
            ordArray: res
          }
        })
        dialogRef.afterClosed().subscribe(res => {
          this.getVndShippedOrders();
        })
        this.spinner.hide();
      })
    }
  }
  // LOADING BOTH TABLES ON TAB SELECTION
  loadtables(event) {
    this.searchValue = '';
    this.spinner.show();
    if (event.index == 0) {
      this.getVndPendingOrders();

    } else if (event.index == 1) {
      this.getVndAcceptedOrders();
    } else {
      this.getVndShippedOrders();
    }
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event, status) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (status == 'Pending') {
      if (eventlength != "" && eventlength > lengthofevent) {

        let temp = this.allvndPndOrdList.filter(function (categiry) {
          return (
            categiry.username.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
            categiry.noofitems.indexOf(event) > -1 ||
            categiry.total.indexOf(event) > -1
          )
        });

        if (temp.length !== 0) {
          this.vendorPndOrders = new MatTableDataSource(temp);
          this.vendorPndOrders.paginator = this.paginator.toArray()[0];
          this.vendorPndOrders.sort = this.sort.toArray()[0];
          this.records = false;
          this.norecords = true;
        } else if (temp.length == 0) {
          this.records = true;
          this.norecords = false;
          this.vendorPndOrders = new MatTableDataSource(temp);
          this.vendorPndOrders.paginator = this.paginator.toArray()[0];
          this.vendorPndOrders.sort = this.sort.toArray()[0];
        }
      } else if (eventlength <= lengthofevent) {
        this.vendorPndOrders = this.allvndPndOrdList;
        this.vendorPndOrders = new MatTableDataSource(this.allvndPndOrdList);
        this.vndshporder.paginator = this.paginator.toArray()[0];
        this.vndshporder.sort = this.sort.toArray()[0];
        this.records = false;
        this.norecords = true;
      }
    } else if (status == 'Confirmed') {
      if (eventlength != "" && eventlength > lengthofevent) {

        let temp = this.allvndacptordlist.filter(function (categiry) {
          return (
            categiry.username.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
            categiry.noofitems.indexOf(event) > -1 ||
            categiry.total.indexOf(event) > -1
          )
        });

        if (temp.length !== 0) {
          this.vendoracptOrders = new MatTableDataSource(temp);
          this.vendoracptOrders.paginator = this.paginator.toArray()[1];
          this.vendoracptOrders.sort = this.sort.toArray()[1];
          this.records1 = false;
          this.norecords1 = true;
        } else if (temp.length == 0) {
          this.records1 = true;
          this.norecords1 = false;
          this.vendoracptOrders = new MatTableDataSource(temp);
          this.vendoracptOrders.paginator = this.paginator.toArray()[1];
          this.vendoracptOrders.sort = this.sort.toArray()[1];
        }
      } else if (eventlength <= lengthofevent) {
        this.vendoracptOrders = this.allvndacptordlist;
        this.vendoracptOrders = new MatTableDataSource(this.allvndacptordlist);
        this.vendoracptOrders.paginator = this.paginator.toArray()[1];
        this.vendoracptOrders.sort = this.sort.toArray()[1];
        this.records1 = false;
        this.norecords1 = true;
      }
    } else if (status == 'Shipped') {
      if (eventlength != "" && eventlength > lengthofevent) {

        let temp = this.allvndshpordList.filter(function (categiry) {
          return (
            categiry.username.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
            categiry.noofitems.indexOf(event) > -1 ||
            categiry.total.indexOf(event) > -1
          )
        });

        if (temp.length !== 0) {
          this.vndshporder = new MatTableDataSource(temp);
          this.vndshporder.paginator = this.paginator.toArray()[2];
          this.vndshporder.sort = this.sort.toArray()[2];
          this.records2 = false;
          this.norecords2 = true;
        } else if (temp.length == 0) {
          this.records2 = true;
          this.norecords2 = false;
          this.vndshporder = new MatTableDataSource(temp);
          this.vndshporder.paginator = this.paginator.toArray()[2];
          this.vndshporder.sort = this.sort.toArray()[2];
        }
      } else if (eventlength <= lengthofevent) {
        this.vndshporder = this.allvndshpordList;
        this.vndshporder = new MatTableDataSource(this.allvndshpordList);
        this.vndshporder.paginator = this.paginator.toArray()[2];
        this.vndshporder.sort = this.sort.toArray()[2];
        this.records2 = false;
        this.norecords2 = true;
      }
    }
  }
}

@Component({
  selector: 'app-vendorordersmodal',
  templateUrl: './vendorordersmodal.component.html',
  styleUrls: ['./vendororders.component.scss'],
  providers: [HeaderSideComponent]
})
export class VendorordersmodalComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  vendorOrdersbyUserName: any;
  vendorId: any;
  displayedColumns: string[] = ['S No', 'Order Id', 'Delivery Date', 'Product Name', 'Weight', 'Price', 'Quantity', 'TotalPrice', 'Actions'];
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService,
    public dialog: MatDialogRef<VendorordersComponent>, private service: ApiService, private spinner: NgxSpinnerService,
    private message: MessageService, private toast: ToasterService, @Inject(MAT_DIALOG_DATA) public data: VndordData, private hdcomp: HeaderSideComponent
  ) { }
  ngOnInit() {
    this.vendorId = this.storage.get('vendor_id');
    this.vendorOrdersbyUserName = new MatTableDataSource(this.data.ordArray)
    this.vendorOrdersbyUserName.paginator = this.paginator;
  }

  // CLOSING DIALOG
  closeDialog() {
    this.dialog.close();
  }
  // ACCEPTING ORDERS
  acceptorders(grpId, orddetId, usrId, ordId) {
    this.spinner.show();
    let Grpobj = {
      "group_id": grpId,
      "ord_det_id": orddetId,
      "user_id": usrId,
      "order_id": ordId
    }
   this.service.post(this.service.acceptcordrs, Grpobj).subscribe(res => {
     if (res == "Updated") {
        this.toast.showsuccess(this.message.acceptorder);
        this.spinner.hide();
        this.getpndords(usrId, 'Pending')
       // this.hdcomp.checkpendingstatus(100);
        this.storage.set('vdpndcount',300);
      }
    })
  }
  // REJECTING ORDERS
  cancelorders(grpId, orddetId, usrId) {
    let Grpobj = {
      "group_id": grpId,
      "ord_det_id": orddetId,
      "user_id": usrId,
    }
    this.service.post(this.service.cancordrs, Grpobj).subscribe(res => {

      if (res == "Updated") {
        this.toast.showsuccess(this.message.cancorders);
        this.spinner.hide();
        this.getpndords(usrId, 'Pending')
        this.hdcomp.ngOnInit();
      }
    })
  }
  // DELIVERING ORDERS TO USERS
  displatchorder(orddetId, grpid, usrId) {
    this.spinner.show();
    let ordDet = {
      "order_detail_id": orddetId,
      "order_prod_grouping_id": grpid
    }
    this.service.post(this.service.dispatchorders, ordDet).subscribe(res => {
      if (res == "Updated") {
        this.toast.showsuccess(this.message.productsShipped)
        this.spinner.hide();
        this.getpndords(usrId, 'Accepted')
        // this.dialog.close();
      }
    })
  }
  // DELIVERED  USERS ORDERS  
  deliverorder(orddetId, ordId, usrId) {
    this.spinner.show();
    let ordObj = {
      "ord_det_id": orddetId,
      "order_id": ordId,
    }
    this.service.post(this.service.vendordelvordersbyuserId, ordObj).subscribe(res => {
      if (res == 'Updated') {
        this.toast.showsuccess(this.message.delivered)
        this.spinner.hide();
        this.getpndords(usrId, 'Delivered')
      }
    })
  }
  // GETTING PENDING ORDERS
  getpndords(usrId, status) {
    let usrObj = {
      "vendor_id": this.vendorId,
      "user_id": usrId
    }
    if (status == 'Pending') {

      this.service.post(this.service.vendorpndordersbyuserId, usrObj).subscribe(res => {
        if (res.length == 0) {
          this.closeDialog()
        } else {
          this.vendorOrdersbyUserName = new MatTableDataSource(res)
        }
      })
    } else if (status == 'Accepted') {
      this.service.post(this.service.vendoracptordersbyuserId, usrObj).subscribe(res => {
        if (res.length == 0) {
          this.closeDialog()
        } else {
          this.vendorOrdersbyUserName = new MatTableDataSource(res)
        }
      })
    } else {
      this.service.post(this.service.vendorshpordersbyuserId, usrObj).subscribe(res => {
        if (res.length == 0) {
          this.closeDialog()
        } else {
          this.vendorOrdersbyUserName = new MatTableDataSource(res)
        }
      })
    }
  }

}
export interface VndordData {
  ordstatus;
  ordArray;
}