import { vendororderroutes } from './vendororders.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VendorordersComponent, VendorordersmodalComponent } from './vendororders.component';
import { CommonModule } from '@angular/common';  
import {
    MatIconModule,
    MatDialogModule,
    MatButtonModule, 
    MatCardModule,
    MatListModule, 
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatTabsModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatOptionModule,
        MatCardModule,
        MatListModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule, 
        MatInputModule, 
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatCheckboxModule,
        CommonModule,
        MatTabsModule,
        FormsModule,
        RouterModule.forChild(vendororderroutes),
    ],
    providers:[],
    declarations: [VendorordersComponent,VendorordersmodalComponent
    ],
    entryComponents:[VendorordersmodalComponent]
   
})
export class VendorOrdersModule{}
