import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-vendorreport',
  templateUrl: './vendorreport.component.html',
  styleUrls: ['./vendorreport.component.scss']
})
export class VendorreportComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.hide();
  }

}
