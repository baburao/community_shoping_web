import { vendorhistoryroutes } from './vendorreport.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VendorreportComponent } from './vendorreport.component';
import { CommonModule } from '@angular/common';
import {
    MatDatepickerModule,
    MatFormFieldModule 
    // MatIconModule,
    // MatDialogModule,
    // MatButtonModule, 
    // MatCardModule,
    // MatListModule, 
    // MatToolbarModule,
    // MatInputModule,
    // MatDatepickerModule,
    // MatNativeDateModule,
    // MatOptionModule,
    // MatSelectModule,
    // MatTableModule,
    // MatPaginatorModule,
    // MatProgressSpinnerModule,
    // MatTooltipModule,
    // MatCheckboxModule,
} from '@angular/material';

@NgModule({
    imports: [
        MatDatepickerModule,
        MatFormFieldModule ,
        // MatIconModule,
        // MatDialogModule,
        // MatButtonModule,
        // MatOptionModule,
        // MatCardModule,
        // MatListModule,
        // MatToolbarModule,
        // MatDatepickerModule,
        // MatNativeDateModule,
        // MatInputModule,
        // MatSelectModule,
        // MatTableModule,
        // MatPaginatorModule,
        // MatProgressSpinnerModule,
        // MatTooltipModule,
        // MatCheckboxModule,
         CommonModule,
        RouterModule.forChild(vendorhistoryroutes),
    ],
    providers: [],
    declarations: [VendorreportComponent
    ],
})
export class VendorReportModule { }
