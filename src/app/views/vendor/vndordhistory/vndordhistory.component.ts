import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { MessageService } from '../../services/message.service'
import { ToasterService } from '../../services/toaster.service'
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';
import { MatTableDataSource, MatDialog, MatDialogRef, MatPaginator, MAT_DIALOG_DATA } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-vndordhistory',
  templateUrl: './vndordhistory.component.html',
  styleUrls: ['./vndordhistory.component.scss']
})
export class VndordhistoryComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  vendorId: '';
  vendorDispatchOrders: any;
  allvndDispatchordlist: any;
  records: boolean = false;
  norecords: boolean = true;
  displayedColumns: string[] = ['User Name', 'Orders', 'Amount', 'View Orders']
  constructor(private service: ApiService, public dialog: MatDialog, private spinner: NgxSpinnerService,
    private message: MessageService,
    private toast: ToasterService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.vendorId = this.storage.get('vendor_id');
    this.getDispatchVndOrders();
  }

  // GETTING VENDOR ORDERS
  getDispatchVndOrders() {
    let vndObj = {
      "vendor_id": this.vendorId
    }
    this.service.post(this.service.vnddispatchorders, vndObj).subscribe(res => {
      this.vendorDispatchOrders = new MatTableDataSource(res)
      this.allvndDispatchordlist = res;
      this.vendorDispatchOrders.paginator = this.paginator;
      if (res.length != 0) {
        this.records = false;
        this.norecords = true;
      }
      else {
        this.records = true;
        this.norecords = false;
      }
      this.spinner.hide();
    })
  }

  // DISPLAYING ORDERS by USER NAME
  showList(usrid) {
    this.spinner.show();
    let usrObj = {
      "vendor_id": this.vendorId,
      "user_id": usrid
    }
    this.service.post(this.service.vnddispatchorderdetails, usrObj).subscribe(res => {
      this.dialog.open(VndhisModalComponent, {
        width: '750px',
        disableClose: true,
        autoFocus: true,
        data: {
          "ordhisArray": res
        }
      })
      this.spinner.hide();
    })
  }
  // SEARCH FUNCTIONALTY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allvndDispatchordlist.filter(function (categiry) {
        return (
          categiry.username.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.totalprice.indexOf(event) > -1 ||
          categiry.nooforders.indexOf(event) > -1 
        )
      });
      if (temp.length !== 0) {
        this.vendorDispatchOrders = new MatTableDataSource(temp);
        this.vendorDispatchOrders.paginator = this.paginator;
        this.records = false;
        this.norecords = true;
      } else if (temp.length == 0) {
        this.records = true;
        this.norecords = false;
        this.vendorDispatchOrders = new MatTableDataSource(temp);
        this.vendorDispatchOrders.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.vendorDispatchOrders = this.allvndDispatchordlist;
      this.vendorDispatchOrders = new MatTableDataSource(this.allvndDispatchordlist);
      this.vendorDispatchOrders.paginator = this.paginator;
      this.records = false;
      this.norecords = true;
    }
  }
}

@Component({
  selector: 'app-vndordhisModal',
  templateUrl: './vndordhisModal.component.html',
  styleUrls: ['./vndordhistory.component.scss']
})
export class VndhisModalComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  vendorDispOrdersbyUserName: any
  displayedColumns: string[] = ['S No', 'Order Id', 'Date', 'Product Name', 'Weight', 'Price', 'Quantity', 'TotalPrice']
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, public dialog: MatDialogRef<VndordhistoryComponent>, @Inject(MAT_DIALOG_DATA) public data: vndordhis) {
  }
  ngOnInit() {
    this.vendorDispOrdersbyUserName = new MatTableDataSource(this.data.ordhisArray)
    this.vendorDispOrdersbyUserName.paginator = this.paginator;
  }
  closeDialog() {
    this.dialog.close();
  }
}
export interface vndordhis {
  ordhisArray;
}