import { vndordroutes } from './vndordhistory.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VndordhistoryComponent, VndhisModalComponent} from './vndordhistory.component'
import { CommonModule } from '@angular/common';


import {
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    // MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule,
} from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatFormFieldModule,
        MatCardModule,
        CommonModule,
        FormsModule,
        MatInputModule,
        MatTableModule, 
        MatTooltipModule,
        MatPaginatorModule,
        RouterModule.forChild(vndordroutes),
    ],
    providers: [],
    declarations: [VndordhistoryComponent,VndhisModalComponent],
    entryComponents:[VndhisModalComponent]
}) 

export class VndordhistoryModule { }