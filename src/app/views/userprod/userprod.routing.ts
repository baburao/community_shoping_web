import { Routes } from '@angular/router';

import { UserProductsComponent } from './userprod.component';

export const UserProductRoutes: Routes= [
  { path: 'userprod', component: UserProductsComponent }
    // { path: 'userprod/Regular', component: UserProductsComponent },
    // { path: 'userprod/Organic', component: UserProductsComponent },
    // { path: 'userprod/Millets', component: UserProductsComponent }
  ];
