import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
declare var Razorpay: any;

@Component({
  selector: 'app-login',
  templateUrl: './userprod.component.html',
  styleUrls: ['./userprod.component.css'],
  providers: [DatePipe]
})
export class UserProductsComponent implements OnInit {
  @ViewChild("#modal-close") paycls;


  paymentid = '';
  productList;
  carticon: boolean = false;
  productArray = [];
  countProducts;
  hideProductList: boolean = false;
  viewCartItems: boolean = true;
  productBack: boolean = true;
  viewProductList;
  increseCount;
  carticonhide = [];
  prodCount;
  shopingCartEmpty: boolean = true;
  prodIncrese;
  prodDecrese;
  viewProdDecrese;
  viewProdIncrese;
  categoryData = [];
  categoryNames;
  filterdata;
  dropdownSettings = {};
  seleted;
  brandName;
  categoryName;
  hsnCode;
  measurement;
  price;
  productCode;
  productImage;
  productDescription;
  productName;
  unitType;
  prodqty;
  menuname: string = "";
  prodIndex;
  subTotal: any;
  categoryList;
  prodInnerView: boolean = true;
  fullProdView: boolean = true;
  category = [];
  filterDataByCategoryName = [];
  selectalltablesdata = [];
  subTotal1 = 0;
  singleProductData;
  uncheckid;
  addCartBtn: boolean;
  indexdtlValue;
  selectedItems = [];
  copyproductlist = [];
  hours_arry = ['8 AM', '9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM', '3 PM', '4 PM', '5 PM', '6 PM', '7 PM', '8 PM'];
  selectedValue: any;
  // path;
  sysdate = new Date();
  cartdate: boolean = true;
  myDate = new Date();
  selected_ind_hrs: any = this.hours_arry[2];
  menutypeId: number = 0;
  records: boolean = false;
  norecords: boolean = true;
  product_id;
  totalamt;
  constructor(private router: Router, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService,
    private toast: ToasterService, private spinner: NgxSpinnerService, private datePipe: DatePipe,
  ) { }

  ngOnInit() {
    this.menuname = "";
    this.menuname = this.storage.get("menuname");
    this.countProducts = 0;
    this.getProductList();
    this.getCategorys();
    
  }

  //GET CATEGORYS BY PRODUCT TYPE
  getCategorys() {
    // this.menutypeId = 0;
    var temparrayselected = []
    let ctgObj = {
      "product_type_name": this.menuname
    }
    this.service.post(this.service.getusrprodctg, null).subscribe(res => {
      if (res.length != 0) {
        this.norecords = true;
        this.records = false;
        for (let i = 0; i < res.length; i++) {
          this.categoryData.push({
            'item_id': res[i].category_id,
            'item_text': res[i].category_name,
          });

          temparrayselected = [
            { item_id: res[i].category_id, item_text: res[i].category_name }
          ];
          this.selectedItems.push(temparrayselected[0]);
        }
        // this.selectedItems = temparrayselected
        // this.menutypeId = res[0].product_type_id;

        this.categoryNames = this.categoryData;
        this.onSelectedCheckBox(this.selectedItems)
        // this.onSelectedAllTables(this.selectedItems);

      } else {
        this.norecords = false;
        this.records = true;
        this.productList = [];
        this.spinner.hide();
      }
    })
  }


  //GET PRODUCT LIST DATA
  getProductList() {
    // this.spinner.show();
    this.service.post(this.service.getallproducts, '').subscribe((res) => {
      res.forEach(element => {
        element.product_image = this.service.staticURL + 'products_images/' + element.product_image;
      });
      this.filterdata = res;
      console.log('this.filterdata')
      console.log(this.filterdata)

      this.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true,
        enableCheckAll: true,
        closeDropDownOnSelection: false,
        // defaultOpen: false
      };
    });
  }


  //SEARCH BY CATEGORY NAME
  onSelectedCheckBox(seltable) {

    for (let i = 0; i < this.categoryNames.length; i++) {
      if (this.categoryNames[i].item_id == seltable.item_id) {
        this.category.push({ 'item_id': seltable.item_id, 'item_text': seltable.item_text });
      }
    }

    this.getFilterObjectProperies();
  }

  //CANCLE THE SELETED SEARCH ITEM
  onDeSelectedCheckBox(deseltab) {
    this.uncheckid = deseltab.item_id
    for (let i = 0; i < this.category.length; i++) {
      if (this.category[i].item_id == deseltab.item_id) {
        this.category.splice(i, 1);
        break;
      }
    }
    this.getFilterObjectProperies();
  }

  //FILTER DATA BY CATEGORY NAME
  getFilterObjectProperies() {
    this.filterDataByCategoryName = [];
    if (this.category.length != 0) {
      for (let i = 0; i < this.category.length; i++) {
        for (let j = 0; j < this.filterdata.length; j++) {
          if (this.category[i].item_id == this.filterdata[j].category_id) {
            this.filterDataByCategoryName.push(this.filterdata[j]);
          }
        }
      }
      this.productList = this.filterDataByCategoryName;
      this.spinner.hide();
    }
    else {
      let leng = this.productList.length
      let fildata = [];
      fildata = this.productList.filter(x => x.category_id != this.uncheckid)
      this.productList = fildata

    }
  }




  //select all table
  onSelectedAllTables(selalltab) {
    this.category = selalltab
    this.selectgetFilterObjectProperies();
  }

  //deselect all table
  onDeSelectedAllTables(deselalltab) {
    this.category = deselalltab
    this.selectgetFilterObjectProperies();
  }


  //CHECK ALL FUNCTIONALITY
  selectgetFilterObjectProperies() {
    this.selectalltablesdata = [];
    if (this.category.length != 0) {
      for (let i = 0; i < this.category.length; i++) {
        for (let j = 0; j < this.filterdata.length; j++) {
          if (this.category[i].item_id == this.filterdata[j].category_id) {
            this.selectalltablesdata.push(this.filterdata[j]);
          }
        }
      }
      this.productList = this.selectalltablesdata;

      this.copyproductlist = this.productList;

    } else {

      this.productList = [];

      // for (let i = 0; i < this.copyproductlist.length; i++) {
      //   for (let j = 0; j < this.filterdata.length; j++) {
      //     if (this.copyproductlist[i].item_id == this.filterdata[j].product_id) {
      //       let fildata = this.copyproductlist.filter(x => x.category_id != this.copyproductlist[i].item_id)
      //       this.productList = fildata;
      //     }
      //   }
      // }

      // let fildata = this.productList.filter(x => x.category_id != this.uncheckid)
      // console.log(this.productList)
      // this.productList = fildata;
      // this.productList = [];
      // this.category = [];
    }
  }

















  //ADD TO CART PRODUCT
  addToCartProducts(productData, productname, index) {
    let count = productData.qty * productData.price;
    productData.prod_total = count.toFixed(2);
    this.carticonhide[productname] = true;
    this.productArray.push(productData);
    this.storage.set("cartitem", this.productArray);
    if (this.productArray.length >= 1) {
      this.countProducts = this.productArray.length;
      this.carticon = false;
    }
  }
  //DISPLAY CART PRODUCT LIST
  showAddToCartItems() {
    var samplearry = [];
    if (this.productArray.length != 0) {
      for (let i = 0; i < this.productArray.length; i++) {
        let dateval = this.datePipe.transform(this.myDate, 'yyyy-MM-dd')
        this.selected_ind_hrs = this.hours_arry[2];
        var objprod = {
          brand_name: this.productArray[i].brand_name,
          category_id: this.productArray[i].category_id,
          category_name: this.productArray[i].category_name,
          is_active: this.productArray[i].is_active,
          price: this.productArray[i].price,
          prod_total: this.productArray[i].prod_total,
          product_id: this.productArray[i].product_id,
          product_image: this.productArray[i].product_image,
          product_name: this.productArray[i].product_name,
          qty: this.productArray[i].qty,
          weight: this.productArray[i].weight,
          delivery_date: dateval,
          delivery_time: this.selected_ind_hrs
        }
        samplearry.push(objprod);
      }
      this.productArray = samplearry;
      this.viewProductList = this.productArray;
      this.prodCount = this.viewProductList.length;
      this.calculateTotalAmt();
      this.cartdate = false;
      this.hideProductList = true;
      this.viewCartItems = false;
      this.carticon = true;
      this.productBack = false;
      this.prodInnerView = true;
      this.fullProdView = true;
      this.selectedValue = this.hours_arry[2];
    } else {
      this.toast.showwarning(this.message.orderproducts);
    }
  }
  //DECRESE COUNT FROM PRODUCT QUANTITY IN FIRST VIEW
  productCountDecrese(qty, index, prodPrice, prodid) {
    this.prodCount = qty;
    if (this.prodCount == "1") {
      this.productList[index]['qty'] = "1";
      this.prodIncrese = 1 * parseFloat(prodPrice);
    } else {
      this.increseCount = parseFloat(this.prodCount) - 1;
      this.productList[index]['qty'] = this.increseCount;
      this.prodDecrese = this.increseCount * parseFloat(prodPrice)
      this.productList[index]['prod_total'] = this.prodDecrese.toFixed(2);
      if (this.viewProductList) {
        this.viewProductList.forEach(element => {
          if (element.product_id == prodid) {
            element.qty = this.increseCount;
            element.prod_total = this.prodDecrese.toFixed(2);
          }
        });
      }
    }
  }
  //INCRESE COUNT FROM PRODUCT QUANTITY IN FIRST VIEW
  productCountIncrese(qty, index, prodPrice, prodid) {
    this.prodCount = qty;
    this.increseCount = parseFloat(this.prodCount) + 1;
    this.productList[index]['qty'] = this.increseCount;
    this.prodDecrese = this.increseCount * parseFloat(prodPrice)
    this.productList[index]['prod_total'] = this.prodDecrese.toFixed(2);
    if (this.viewProductList) {
      this.viewProductList.forEach(element => {
        if (element.product_id == prodid) {
          element.qty = this.increseCount;
          element.prod_total = this.prodDecrese.toFixed(2);
        }
      });
    }
  }
  //DECRESE COUNT FROM PRODUCT QUANTITY IN CART VIEW
  viewDecreaseCount(qtyCount, index, viewProductPrice, prdid) {
    this.prodCount = qtyCount;
    if (this.prodCount == "1") {
      this.productList[index]['qty'] = "1";
      this.viewProductList[index]['qty'] = "1";
    } else {
      this.increseCount = parseFloat(this.prodCount) - 1;
      this.viewProdDecrese = this.increseCount * parseFloat(viewProductPrice)
      this.viewProductList.forEach(element => {
        if (element.product_id == prdid) {
          element.qty = this.increseCount;
          element.prod_total = this.viewProdDecrese.toFixed(2);
        }
      });
      this.productList.forEach(element => {
        if (element.product_id == prdid) {
          element.qty = this.increseCount;
          element.prod_total = this.viewProdDecrese.toFixed(2);
        }
      });

    }
    this.calculateTotalAmt();
  }

  //INCRESE COUNT FROM PRODUCT QUANTITY IN CART VIEW
  viewIncreaseCount(qtyCount, index, viewProductPrice, prdid) {
    this.prodCount = qtyCount;
    this.increseCount = parseFloat(this.prodCount) + 1;
    this.viewProdIncrese = this.increseCount * parseFloat(viewProductPrice)
    this.viewProductList.forEach(element => {
      if (element.product_id == prdid) {
        element.qty = this.increseCount;
        element.prod_total = this.viewProdIncrese.toFixed(2);
      }
    });
    this.productList.forEach(element => {
      if (element.product_id == prdid) {
        element.qty = this.increseCount;
        element.prod_total = this.viewProdIncrese.toFixed(2);
      }
    });
    this.calculateTotalAmt();
  }

  //BACK BUTTON
  showProductsBackBtn() {
    this.hideProductList = false;
    this.viewCartItems = true;
    this.carticon = false;
    this.productBack = true;
    this.shopingCartEmpty = true;
    this.prodInnerView = true;
    this.fullProdView = true;
    this.cartdate = true;
  }

  // individual date selection on cart
  onDate(dateval, indexval) {
    // this.viewProductList["delivery_date"] = event;
    // this.getData(this.viewProductList["delivery_date"]);
    this.viewProductList[indexval]["delivery_date"] = this.datePipe.transform(dateval, 'yyyy-MM-dd');
  }

  // global date selection on cart
  onglobalDate(gbldateval) {
    this.viewProductList.forEach(element => {
      element.delivery_date = this.datePipe.transform(gbldateval, 'yyyy-MM-dd')
    });
  }

  // assiging individual products delivery time
  individual_hrs(hrs_val, indx, prod_id) {
    this.viewProductList[indx]["delivery_time"] = hrs_val;
  }

  // assiging products delivery time global
  global_hrs(hrs_val) {
    this.viewProductList.forEach(element => {
      element.delivery_time = hrs_val;
      this.selected_ind_hrs = this.selectedValue;
    });
  }

  //REMOVE PRODUCT FROM PRODUCT LIST IN CART VIEW
  removeProductFromProductList(index, productname, prid) {
    this.productArray.splice(index, 1);
    this.countProducts = this.productArray.length;
    this.prodCount = this.countProducts;
    this.calculateTotalAmt();
    this.storage.set("cartitem", this.productArray);
    this.carticonhide[productname] = false;
    this.productList.forEach(element => {
      if (element.product_id == prid) {
        element.qty = 1;
      }
    });

    if (this.productArray.length == 0) {
      this.shopingCartEmpty = false;
    }
  }

  //CALCULATE THE TOTAL AMOUNT FROM ALL PRODUCTS
  calculateTotalAmt() {
    this.subTotal1 = 0;
    for (let i = 0; i <= this.viewProductList.length - 1; i++) {
      this.subTotal1 += parseFloat(this.viewProductList[i]['prod_total']);
    }
    this.subTotal = this.subTotal1.toFixed(2);
  }

  //CREATE ORDER GENERATE
  createOrder(resp) {
    if (resp.razorpay_payment_id == 'undefined' || resp.razorpay_payment_id < 1) {
      this.toast.showsuccess(this.message.orderUnsuccessfull);
    }
    else {
      this.spinner.show();
      let data = this.viewProductList;
      for (let i = 0; i <= data.length - 1; i++) {
        this.carticonhide[data[i]['product_name']] = false;
        data[i].prod_total = data[i].price * data[i].qty
      }
      let createOrderObj = {
        user_id: this.storage.get('user_id'),
        transaction_status: "SUCCESS",
        total_amount: this.subTotal,
        total_products: data.length,
        order_detail: data,
        payment_id: resp.razorpay_payment_id
      }
      this.service.post(this.service.insertuserorder, createOrderObj).subscribe((res) => {
        if (res = "Inserted") {
          this.spinner.show();
          this.viewCartItems = true;
          this.carticon = false;
          this.productBack = true;
          this.shopingCartEmpty = true;
          this.prodInnerView = true;
          this.fullProdView = true;
          this.storage.remove('cartitem');
          this.toast.showsuccess(this.message.orderCreated);
          this.countProducts = 0;
          this.productArray = [];
          this.viewProductList = [];
          this.category = []
          this.hideProductList = false;
          this.cartdate = true;
          this.getProductList();
        }
      });
    }
  }

  //DISPLAY PRODUCT DETAILS BY ID
  showProddtl(prodid, index) {
    this.spinner.show();
    this.prodIndex = index;
    this.hideProductList = true;
    this.fullProdView = false;
    // this.addCartBtn = false;
    let prodObj = {
      product_id: prodid
    }
    this.service.post(this.service.getproductbyid, prodObj).subscribe((res) => {
      this.singleProductData = res;
      this.brandName = res[0]['brand_name'];
      this.categoryName = res[0]['category_name'];
      this.hsnCode = res[0]['hsn_code'];
      this.measurement = res[0]['measurement'] + '' + res[0]['unit_type'];
      this.price = res[0]['price'];
      this.productCode = res[0]['product_code'];
      this.productDescription = res[0]['product_description'];
      this.productImage = this.service.staticURL + 'products_images/' + res[0]['product_image'];
      this.productName = res[0]['product_name'];
      this.unitType = res[0]['unit_type'];
      this.prodqty = this.productList[index]['qty'];
      this.product_id = this.productList[index]['product_id'];
      this.indexdtlValue = index;
      this.prodInnerView = false;
      for (let i = 0; i <= this.productArray.length - 1; i++) {
        if (this.productArray[i]['product_id'] == this.product_id) {
          this.addCartBtn = true;
          this.spinner.hide();
          return;
        } else {
          this.addCartBtn = false;
        }
      }
      this.spinner.hide();
    });
  }

  //DECRESE COUNT FROM PRODUCT IN PRODUCT DETAILS VIEW 
  viewProdDtlDecrese(prodid) {
    this.prodCount = this.prodqty;
    if (this.prodCount == 1) {
      this.productList[this.prodIndex]['qty'] = 1;
      this.prodqty = 1;
    } else {
      if (this.productList) {
        this.productList.forEach(element => {
          if (element.product_id == prodid) {
            this.increseCount = parseFloat(this.prodCount) - 1;
            element.qty = this.increseCount;
            this.prodDecrese = this.increseCount * parseFloat(element.prod_total);
            element.prod_total = this.prodDecrese.toFixed(2);
            this.prodqty = this.increseCount;
          }
        });
      }
    }
  }
  //INCRESE COUNT FROM PRODUCT IN PRODUCT DETAILS VIEW 
  viewProdDtlIncrese(prodid) {
    this.prodCount = this.prodqty;
    this.productList.forEach(element => {
      if (element.product_id == prodid) {
        this.increseCount = parseFloat(this.prodCount) + 1;
        element.qty = this.increseCount;
        this.prodDecrese = this.increseCount * parseFloat(element.prod_total);
        element.prod_total = this.prodDecrese.toFixed(2);
        this.prodqty = this.increseCount;
      }
    });
  }

  // ADDING PRODUCTS TO CART
  addToCart(productImage, productName, categoryName, brandName, price, measurement, prodqty, indexdtlValue, product_id) {
    this.prodDecrese = prodqty * parseFloat(price)
    this.totalamt = this.prodDecrese.toFixed(2);
    this.productArray.push({
      "product_name": productName,
      "product_image": productImage,
      "qty": prodqty,
      "price": price,
      "measurement": measurement,
      "prod_total": this.totalamt,
      "product_id": product_id
    });
    if (this.productArray.length >= 1) {
      this.countProducts = this.productArray.length;
      this.carticon = false;
      this.addCartBtn = true;
      this.carticonhide[productName] = true;
      this.storage.set("cartitem", this.productArray);
    }
  }
  // BACK BUTTON INSIDE CART VIEW
  showFullProductsBackBtn() {
    this.productBack = true;
    this.fullProdView = true;
    this.hideProductList = false;
    this.prodInnerView = true;
  }

  // PAYMENT METHOD
  payNow() {
    if (this.viewProductList.length == 0 || this.viewProductList.length == [] ||
      this.viewProductList.length == undefined) {
      this.toast.showwarning(this.message.orderproducts);
    }
    else {
      var options = {
        "key": "rzp_test_7kMLm6TgFDyDqi",
        "amount": this.subTotal * 100, // 2000 paise = INR 20
        "name": this.storage.get('loginname'),
        "description": "Order #",
        "handler": this.createOrder.bind(this),
        "prefill": {
          "name": this.storage.get('loginname'),
          "email": this.storage.get('email_id'),
          "contact": this.storage.get('mobileNum')
        },
        "notes": {},
        "theme": {
          "color": "#b9a76e"
        }
      };
      var rzp1 = new Razorpay(options);
      rzp1.open();
    }
  }
}