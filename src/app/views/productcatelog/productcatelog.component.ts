//NAME : BABURAO M
//DESCRIPTION : ADDING PRODUCT CATALOG AND MODIFICATIONS
//DATE : 09/05/2019

import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { FormGroup } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { } from '@angular/material'

import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';

@Component({
  selector: 'app-login',
  templateUrl: './productcatelog.component.html',
  styleUrls: ['./productcatelog.component.css']
})
export class ProductCatelogComponent implements OnInit {




  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;


  productcatelogtable: boolean;
  prodcategoryList: any;
  noRecords: boolean;
  product_image;
  productCatelog: any;
  // PRODUCT CATALOG OBJECT
  productCatelogObj = {
    product_id: '',
    product_name: '',
    product_code: '',
    hsn_code: '',
    product_category_id: '',
    product_type:'',
    brand_id: '',
    unit_type_id: '',
    measurement: '',
    product_price: '',
    product_description: '',
    imgURL: '',
    created_by: '',
    updated_by: ''
  }

  constructor(private router: Router, private service: ApiService,

    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private spinner: NgxSpinnerService,

  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['product_name', 'category_name', 'brand_name', 'measurement', 'price', 'Actions'];

  ngOnInit() {
    this.spinner.show();
    this.getProductList();
  }

  //get user lisr data
  getProductList() {
    this.service.post(this.service.adminproducts, '').subscribe((res) => {
      if (res.length >= 1) {
        this.productCatelog = res;
        this.prodcategoryList = res;
        this.productCatelog = new MatTableDataSource(res);
        this.productCatelog.paginator = this.paginator;
        this.spinner.hide();
        this.productcatelogtable = false;
        this.noRecords = true;
      } else {
        this.spinner.hide();
        this.productcatelogtable = true;
        this.noRecords = false;
      }
    });
  }

  //ADDING PRODUCT CATALOG
  addProductCatelog() {
    this.openDialog1('add');
    let userpage = "add";
    this.storage.set('userpage', userpage);
  }

  //INACTIVE PRODUCT CATALOG
  inactiveProduct(product_id) {
    this.spinner.show();
    let statusProductObj = {
      product_id: product_id,
      is_active: "N"
    }

    this.service.post(this.service.updateproductstatus, statusProductObj).subscribe((res) => {
      this.toast.showsuccess(this.message.deactiveProduct);
      this.getProductList();
    });
  }

  //ACTIVE PRODUCT CATALOG
  activeProduct(product_id) {
    this.spinner.show();
    let statusProductObj = {
      product_id: product_id,
      is_active: "Y"
    }

    this.service.post(this.service.updateproductstatus, statusProductObj).subscribe((res) => {
      this.toast.showsuccess(this.message.activeProduct);
      this.getProductList();
    });
  }
  //EDIT PRODUCT CATALOG
  editProduct(product_id) {
    this.spinner.show();
    let editProductCategoryObj = {
      product_id: product_id,
    }
    this.service.post(this.service.getproductbyid, editProductCategoryObj).subscribe((res) => {
      this.productCatelogObj.product_id = res[0]['product_id'];
      this.productCatelogObj.product_name = res[0]['product_name'];
      this.productCatelogObj.product_code = res[0]['product_code'];
      this.productCatelogObj.product_description = res[0]['product_description'];
      this.product_image = this.service.staticURL + `${'products_images/'}` + res[0]['product_image'];
      this.productCatelogObj.product_category_id = res[0]['product_category_id'];
      this.productCatelogObj.product_type = res[0]['product_type'];
      
      this.productCatelogObj.brand_id = res[0]['brand_id'];
      this.productCatelogObj.measurement = res[0]['measurement'];
      this.productCatelogObj.unit_type_id = res[0]['unit_type_id'];
      this.productCatelogObj.product_price = res[0]['price'];
      this.productCatelogObj.hsn_code = res[0]['hsn_code'];
      this.storage.set("productCatalogImg", res[0]['product_image']);
      this.spinner.hide();

      this.openDialog1('edit');
      let userpage = "edit";
      this.storage.set('userpage', userpage);
    });

  }

  //DIALOG BOX
  openDialog1(val): void {

    const dialogRef = this.dialog.open(ProductCatelogDialog, {
      width: '750px',

      disableClose: true,
      data: {
        product_id: this.productCatelogObj.product_id,
        product_name: this.productCatelogObj.product_name,
        product_code: this.productCatelogObj.product_code,
        product_description: this.productCatelogObj.product_description,
        product_category_id: this.productCatelogObj.product_category_id,
        product_type:this.productCatelogObj.product_type,
        brand_id: this.productCatelogObj.brand_id,
        measurement: this.productCatelogObj.measurement,
        unit_type_id: this.productCatelogObj.unit_type_id,
        product_price: this.productCatelogObj.product_price,
        hsn_code: this.productCatelogObj.hsn_code,
        created_by: this.productCatelogObj.created_by,
        updated_by: this.productCatelogObj.updated_by,
        product_image: this.productCatelogObj.product_name,
        status :val
      },

    });
    dialogRef.afterClosed().subscribe(res => {
      this.getProductList();
    });
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.prodcategoryList.filter(function (categiry) {
        return (
          categiry.product_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.weight.indexOf(event) > -1 ||
          categiry.category_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.price.indexOf(event) > -1 ||
          categiry.brand_name.toLowerCase().indexOf(event.toLowerCase()) > -1
        )
      });
      if (temp.length !== 0) {
        this.productCatelog = new MatTableDataSource(temp);
        this.productCatelog.paginator = this.paginator;
        this.productcatelogtable = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.productcatelogtable = true;
        this.noRecords = false;
        this.productCatelog = new MatTableDataSource(temp);
        this.productCatelog.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.productCatelog = this.prodcategoryList;
      this.productCatelog = new MatTableDataSource(this.prodcategoryList);
      this.productCatelog.paginator = this.paginator;
      this.productcatelogtable = false;
      this.noRecords = true;
    }
  }
}

@Component({
  selector: 'productcatelog',
  templateUrl: 'productcatelog.html',
})
export class ProductCatelogDialog {

  productCatelogObj = {
    product_id: '',
    product_name: '',
    product_code: '',
    hsn_code: '',
    product_category_id: '',
    product_type:'',
    brand_id: '',
    unit_type_id: '',
    measurement: '',
    product_price: '',
    product_description: '',
    imgURL: '',
    created_by: '',
    updated_by: ''
  }

  productCategoryData;
  brandData;
  unitData;
  product_category_id;
  product_type;
  brand_id;
  unit_type_id;
  prodImg;
  count = 0;
  selectedFile;
  filesToUpload;
  file;
  fd = new FormData();
  upImgName;
  imgURL;
  userpage;
  imageshow: boolean = true;
  allprodmenu:any;
  constructor(private service: ApiService,
    public dialogRef: MatDialogRef<ProductCatelogDialog>, private validations: ValidatorService,
    private message: MessageService,
    private toast: ToasterService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }

  ngOnInit() {
    this.imageshow = true;
    this.getProductCategorys();
    this.getBrandsData();
    this.getUnitTypesData();
    this.getallprodmenu();

    this.userpage = this.storage.get('userpage');
    if (this.userpage == "add") {
      this.data.product_name = '';
      this.data.product_code = '';
      this.data.product_description = '';
      this.data.product_category_id = '';
      this.data.product_type = '';
      this.data.brand_id = '';
      this.data.measurement = '';
      this.data.unit_type_id = '';
      this.data.product_price = '';
      this.data.hsn_code = '';
      this.storage.clear();
    } else {
      this.imgURL = this.service.staticURL + `${'products_images/'}` + this.storage.get("productCatalogImg");

      this.imageshow = false;
      this.prodImg = this.data.product_image
    }
  }


  onNoClick(): void {
    this.dialogRef.close();

  }

  //GET PRODUCT CATEGORYS
  getProductCategorys() {
    this.service.post(this.service.getallactivectgs, '').subscribe((res) => {
      this.productCategoryData = res;
    });
  }
  //GET BRANDS DATA
  getBrandsData() {
    this.service.post(this.service.getallbrand, '').subscribe((res) => {
      this.brandData = res;
    });
  }
  //GET UNIT TYPES DATA
  getUnitTypesData() {
    this.service.post(this.service.getallunit, '').subscribe((res) => {
      this.unitData = res;
    });
  }
  //GET PRODUCT CATEGORY ID
  getProductcategoryId(productCategoryId) {
    this.product_category_id = productCategoryId;
  }
  //GET BRAND ID
  getBrandId(brandid) {
    this.brand_id = brandid;
  }
  //GET UNIT TYPE ID
  getUnitTypeId(unittypeid) {
    this.unit_type_id = unittypeid;
  }
  // GET PRODUCT TYPE ID
  getprodtype(prodtypeid){
    this.product_type = prodtypeid;
  }

  //GET ALL PRODUCT tYPE ID
  getallprodmenu(){
    this.service.post(this.service.getallprodmenus,'').subscribe(res=>{
      this.allprodmenu = res;
    })
  }


  openFile(event) {
    this.filesToUpload = event.target.files;
    //this.excelfile  = event.target.files[0].name
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    this.prodImg = files[0]['name'];
    if (files[0] == null) {
      //this.toast.showwarning('Please choose a file.')
      return;
    }
    var reader = new FileReader();
    reader.onload = (val: any) => {
      this.imgURL = val.target.result;
    }
    reader.readAsDataURL(event.target.files[0])

    formData.append("uploads", files[0], files[0]['name']);
    this.service.post(this.service.insertimg, formData)
      .subscribe(res => {
        // let imgpath = 'http://127.0.0.1:9000/static/'+res; 
        this.storage.set("productCatalogImg", res);
        this.imageshow = false;
      });
  }

  // INSERTING AND UPDATING PRODUCTS
  insertProductCatelog(res) {
    if (res.product_id == '') {
      this.productCatelogObj.product_name = res.product_name;
      if (res.product_name == null || res.product_name == '') {
        this.toast.showwarning(this.message.productName);
        return 
      }
      this.productCatelogObj.product_code = res.product_code;
      if (res.product_code == null || res.product_code == '') {
        this.toast.showwarning(this.message.productCode);
        return
      }
      this.productCatelogObj.product_description = res.product_description;
      if (res.product_description == null || res.product_description == '') {
        this.productCatelogObj.product_description = '';
      }

      this.productCatelogObj.hsn_code = res.hsn_code;
      if (res.hsn_code == null || res.hsn_code == '') {
        this.toast.showwarning(this.message.productHSNCode);
        return
      }
      this.productCatelogObj.product_category_id = res.product_category_id;
      if (res.product_category_id == null || res.product_category_id == '') {
        this.toast.showwarning(this.message.categoryName);
        return
      }
      this.productCatelogObj.product_type = res.product_type;
      if (res.product_type == null || res.product_type == '') {
        this.toast.showwarning(this.message.prodType);
        return
      }
      this.productCatelogObj.product_type = res.product_type;
      this.productCatelogObj.brand_id = res.brand_id;
      if (res.brand_id == null || res.brand_id == '') {
        this.toast.showwarning(this.message.brandName);
        return
      }
      this.productCatelogObj.unit_type_id = res.unit_type_id;
      if (res.unit_type_id == null || res.unit_type_id == '') {
        this.toast.showwarning(this.message.unitType);
        return
      }
      this.productCatelogObj.measurement = res.measurement;
      if (res.measurement == null || res.measurement == '') {
        this.toast.showwarning(this.message.measurement);
        return
      }

      this.productCatelogObj.product_price = res.product_price;
      if (res.product_price == null || res.product_price == '') {
        this.toast.showwarning(this.message.productprice);
        return
      }

      this.productCatelogObj.product_description = res.product_description;
      if (res.product_description == null || res.product_description == '') {
        this.toast.showwarning(this.message.productdesc);
        return
      }
      
      this.productCatelogObj.imgURL = this.storage.get("productCatalogImg");;
      if (this.storage.get("productCatalogImg") == null || this.storage.get("productCatalogImg") == '') {
        this.toast.showwarning(this.message.imageUpload);
        return
      }

      this.productCatelogObj.imgURL = this.storage.get("productCatalogImg");
      this.productCatelogObj.created_by = "1";
      this.productCatelogObj.updated_by = "1";
      
      this.service.post(this.service.insertproduct, this.productCatelogObj).subscribe((res) => {
        if (res == "Duplicate") {
          this.toast.showwarning(this.message.duplicateProduct);
        } else {
          this.toast.showsuccess(this.message.insertProduct);
          this.onNoClick();
          this.storage.remove('productCatalogImg');
        }

      })
    } else {

      if (res.product_name == null || res.product_name == '') {
        this.toast.showwarning(this.message.productName);
        return 
      }
      if (res.product_code == null || res.product_code == '') {
        this.toast.showwarning(this.message.productCode);
        return
      }
      if (res.product_description == null || res.product_description == '') {
        this.productCatelogObj.product_description = '';
      }
      if (res.hsn_code == null || res.hsn_code == '') {
        this.toast.showwarning(this.message.productHSNCode);
        return
      }
      if (res.product_category_id == null || res.product_category_id == '') {
        this.toast.showwarning(this.message.categoryName);
        return
      }
      if (res.product_type == null || res.product_type == '') {
        this.toast.showwarning(this.message.prodType);
        return
      }
      if (res.brand_id == null || res.brand_id == '') {
        this.toast.showwarning(this.message.brandName);
        return
      }
      if (res.unit_type_id == null || res.unit_type_id == '') {
        this.toast.showwarning(this.message.unitType);
        return
      }
      if (res.measurement == null || res.measurement == '') {
        this.toast.showwarning(this.message.measurement);
        return
      }
      if (res.product_price == null || res.product_price == '') {
        this.toast.showwarning(this.message.productprice);
        return
      }
      if (res.product_description == null || res.product_description == '') {
        this.toast.showwarning(this.message.productdesc);
        return
      }
      if (this.storage.get("productCatalogImg") == null || this.storage.get("productCatalogImg") == '') {
        this.toast.showwarning(this.message.imageUpload);
        return
      }
      this.productCatelogObj.product_id = res.product_id;
      this.productCatelogObj.product_name = res.product_name;
      this.productCatelogObj.product_code = res.product_code;
      this.productCatelogObj.product_description = res.product_description;
      this.productCatelogObj.product_category_id = res.product_category_id;
      this.productCatelogObj.product_type = res.product_type;
      this.productCatelogObj.brand_id = res.brand_id;
      this.productCatelogObj.measurement = res.measurement;
      this.productCatelogObj.unit_type_id = res.unit_type_id;
      this.productCatelogObj.product_price = res.product_price;
      this.productCatelogObj.hsn_code = res.hsn_code;
      this.productCatelogObj.created_by = "1";
      this.productCatelogObj.updated_by = "1";
      this.productCatelogObj.imgURL = this.storage.get("productCatalogImg");
      this.service.post(this.service.updateproductdetails, this.productCatelogObj).subscribe((res) => {
        if (res == "Duplicate") {
          this.toast.showwarning(this.message.duplicateProduct);
        } else {
          this.toast.showsuccess(this.message.updateProduct);
          this.onNoClick();
        }
      })
    }
  }

  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }

  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validations.nosplChar(event)
  }

  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }


}

export interface DialogData {
  product_name;
  product_code;
  hsn_code;
  product_category_id;
  brand_id;
  unit_type_id;
  measurement;
  product_price;
  product_description;
  product_image;
  product_type;
}