//NAME : BABURAO M
//DESCRIPTION : LOGIN USER,VENDOR AND PORTAL ADMIN
//DATE : 08/05/2019


import { Component, OnInit, ViewChild,TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
  selector: 'app-adminpayments',
  templateUrl: './adminpayments.component.html',
  styleUrls: ['./adminpayments.component.css']
})
export class AdminPaymentComponent implements OnInit {

  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;
  displayedColumns: string[] = ['depositedate','name', 'payment_type','amount'];
  adminPaymentList;
  adminPaymentsListTable:boolean;
  noRecords:boolean;
  alladmpayList:any;

  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService,
    private toast: ToasterService,private spinner: NgxSpinnerService,
  ) { }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.adminPaymentsListTable = false;
    this.noRecords = true;
    this.getadmpayments();
  }

  getadmpayments() {
    this.service.post(this.service.getadminpayments, '').subscribe((res) => {
      if(res.length!=0){
        this.adminPaymentList = res;
        this.alladmpayList = res;
        this.adminPaymentList = new MatTableDataSource(res);
        this.adminPaymentList.paginator = this.paginator;
        this.adminPaymentsListTable = false;
        this.noRecords = true;
      }else{
        this.adminPaymentsListTable = true;
        this.noRecords = false;
      }

    });
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.alladmpayList.filter(function (categiry) {
        return (
          categiry.name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.depositedate.indexOf(event) > -1 ||
          categiry.payment_type.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.amount.indexOf(event) > -1 
        )
      });
      if (temp.length !== 0) {
        this.adminPaymentList = new MatTableDataSource(temp);
        this.adminPaymentList.paginator = this.paginator;
        this.adminPaymentsListTable = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.adminPaymentsListTable = true;
        this.noRecords = false;
        this.adminPaymentList = new MatTableDataSource(temp);
        this.adminPaymentList.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.adminPaymentList = this.alladmpayList;
      this.adminPaymentList = new MatTableDataSource(this.alladmpayList);
      this.adminPaymentList.paginator = this.paginator;
      this.adminPaymentsListTable = false;
      this.noRecords = true;
    }
  }

}



