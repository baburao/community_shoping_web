import { Routes } from '@angular/router';

import { AdminPaymentComponent } from './adminpayments.component';


export const AdminPaymentsRoutes: Routes = [
  { path: '', component: AdminPaymentComponent }
];