import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApiService } from '../services/api.service';
import { FormGroup } from '@angular/forms';
import { ToasterService } from '../services/toaster.service';
import { MessageService } from '../services/message.service';
import { ValidatorService } from '../services/validators.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment'


@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  //formData: any = new FormData();
  communityForm: FormGroup
  prodImg;
  regNo: string = ''
  image: string = '';
  blocks: string = '';
  flats: string = '';
  PhoneNum: string = '';
  cmtEmail: string = '';
  gstNum: string = '';
  pannum: string = '';
  addrs: string = '';
  pincode: string = '';
  cntName: string = '';
  Cntnum: string = '';
  cntdesign: string = '';
  cntMail: string = '';
  Cmtname: string = '';
  CmtId: null;
  imgURL: string = '';
  fd: any = new FormData();
  selectedFile: File = null;
  img: string;
  add: string;
  upImgName: string;
  close: string;
  allitems: any;
  allstates: any;
  allcities: any;
  allcommunities: any;
  communityList :any;
  indvCommunity: any;
  stateId: null;
  cityId: null;
  showform: boolean = true;
  showtbl: boolean = true;
  backbtn: boolean = true;
  activebtn: boolean = true;
  active: boolean = true;
  inactive: boolean = true;
  shweditCmt: boolean = true;
  addSubmitbtn: boolean = false;
  editSubmitbtn: boolean = false;
  addbtn: boolean = false;
  records: boolean = false;
  norecords: boolean = true;
  srcbtn:boolean = false;
  filesToUpload;
  count = 0;
  allcmtList :any;


  newCommunity = {
    "community_id": '',
    "community_name": "",
    "registration_no": "",
    "total_blocks": "",
    "total_flats": "",
    "phone_no": "",
    "email_id": "",
    "address": "",
    "state_id": "",
    "city_id": "",
    "pincode": "",
    "pan_no": "",
    "gst_no": "",
    "community_image": "",
    "contact_person": "",
    "mobile_no": "",
    "designation": "",
    "contact_email_id": "",
    "created_by": "1",
    "updated_by": "1",
  }
  constructor(private service: ApiService, private toast: ToasterService, private message: MessageService, private validations: ValidatorService, private spinner: NgxSpinnerService) { }
  displayedColumns: string[] = ['community_name', 'total_flats', 'email_id', 'mobile_no', 'Actions',];

  // ADDING COMMUNITY
  crCommunity(regNo, Cmtname, blocks, flats, addrs, PhoneNum, cmtEmail, gstNum, pannum, pincode, cntName, Cntnum, cntdesign, cntMail, ) {

    if (this.regNo == "" || this.regNo == undefined) {
      this.toast.showwarning(this.message.regNumber);
      return
    } else if (this.Cmtname == "" || this.Cmtname == undefined) {
      this.toast.showwarning(this.message.cmmtName);
      return
    } else if (this.addrs == "" || this.addrs == undefined) {
      this.toast.showwarning(this.message.cmmtAddress);
      return
    } else if (this.pincode == "" || this.pincode == undefined) {
      this.toast.showwarning(this.message.cmmtPincode);
      return
    } else if (this.stateId == "" || this.stateId == undefined) {
      this.toast.showwarning(this.message.cmmtState);
      return
    } else if (this.cityId == "" || this.cityId == undefined) {
      this.toast.showwarning(this.message.cmmtCity);
      return
    } else if (this.cntName == "" || this.cntName == undefined) {
      this.toast.showwarning(this.message.cntPersonName);
      return
    } else if (this.Cntnum == "" || this.Cntnum == undefined) {
      this.toast.showwarning(this.message.cntPersonNumber);
      return
    } else if (this.cntdesign == "" || this.cntdesign == undefined) {
      this.toast.showwarning(this.message.cntPersonDesgn);
      return
    } else if (this.cntMail == "" || this.cntMail == undefined) {
      this.toast.showwarning(this.message.cntPersonEmail);
      return
    } else {
      this.spinner.show();
      if (this.count > 0) {
        this.service.post(this.service.cmtinsertimg, this.fd)
          .subscribe(res => {
          });
      }
      else {
        this.upImgName = null;
 
      }
      this.newCommunity = {
        "community_id": '',
        "community_name": Cmtname,
        "registration_no": regNo,
        "total_blocks": blocks,
        "total_flats": flats,
        "phone_no": PhoneNum,
        "email_id": cmtEmail,
        "address": addrs,
        "state_id": this.stateId,
        "city_id": this.cityId,
        "pincode": pincode,
        "pan_no": pannum,
        "gst_no": gstNum,
        "community_image": this.upImgName,
        "contact_person": cntName,
        "mobile_no": Cntnum,
        "designation": cntdesign,
        "contact_email_id": cntMail,
        "created_by": "1",
        "updated_by": "1",
      }
      this.service.post(this.service.insertcommunity, this.newCommunity).subscribe(x => {
        if (x == "Inserted") {
          this.toast.showsuccess(this.message.CmtRegistered);
          this.getcommunities()
          this.backtotbl()
          this.spinner.hide();
        }
        else if (x == "Duplicate")
          this.toast.showwarning(this.message.CmtDuplicate);
        this.spinner.hide();
      })
    }

  }

  // FILTERING CITIES BY ID
  selectedstate(event) {
    this.spinner.show();
    this.stateId = event;
    let obj = {
      "state_id": event
    }
    this.service.post(this.service.getcitiesbystate, obj).subscribe(res => {
      this.allcities = res
      this.spinner.hide();
    })
  }

  selectedCity(event) {
    this.cityId = event;
  }
  // GETTING ALL COMMUNITIES
  getcommunities() {
    this.service.post(this.service.allcommunities, []).subscribe(res => {
      this.allcmtList = res;
      this.allcommunities = new MatTableDataSource(res)
      this.allcommunities.paginator = this.paginator;
      if (res.length != 0) {
        this.records = false;
        this.norecords = true;
      }
      else {
        this.records = true;
        this.norecords = false;
      }
      this.addbtn = false;
      this.srcbtn = false;
      this.spinner.hide();
    })
  }

  ngOnInit() {
    this.spinner.show();
    this.backbtn = true;
    this.showtbl = false;
    this.showform = true;
    this.active = false;
    this.inactive = true;
    this.service.post(this.service.getallstate, []).subscribe(x => {
      this.allstates = x;
    })
    this.getcommunities()

  }
  // HANDLING FORM AND TABLE
  displayingForm(value) {
    if (value == "addbutton") {
      this.addbtn = true;
      this.addSubmitbtn = false;
      this.editSubmitbtn = true;
      this.backbtn = false;
      this.showform = false;
      this.showtbl = true;
      this.shweditCmt = true;
      this.CmtId = null;
      this.Cmtname = null;
      this.regNo = null;
      this.blocks = null;
      this.flats = null;
      this.PhoneNum = null;
      this.cmtEmail = null;
      this.addrs = null;
      this.stateId = null;
      this.cityId = null;
      this.pincode = null;
      this.pannum = null;
      this.gstNum = null;
      this.upImgName = null;
      this.cntName = null;
      this.Cntnum = null;
      this.cntdesign = null;
      this.cntMail = null;
      this.imgURL = '';
      this.count = 0;
      this.prodImg = '';
    }
    else {
      this.addbtn = false;
      this.addSubmitbtn = true;
      this.editSubmitbtn = true;
      this.srcbtn = false;
      this.backbtn = true;
      this.showform = true;
      this.showtbl = false;
      this.shweditCmt = true;
    }

  }
  // ACTIVE OR INACTIVE
  isactive(cmtid, isactive) {
    this.spinner.show();
    this.allcommunities = [];
    let obj = {
      "community_id": cmtid,
      "is_active": ''
    }

    if (isactive == 'Y') {
      obj['is_active'] = 'N';
    }
    else {
      obj['is_active'] = 'Y';
    }
    // UPDATE THE COMMUNITY STATUS
    this.service.post(this.service.updatecommunitystatus, obj).subscribe(res => {
      if (res == 'Updated') {
        if (isactive == 'N') {
          this.toast.showsuccess(this.message.CmtActive);
        }
        else {
          this.toast.showsuccess(this.message.CmtInActive);
        }
        this.getcommunities();
      }
      else
        return;
    })

  }
  // EDIT COMMUNITY
  editCommunity(cmtid) {
    this.addbtn = true;
    this.srcbtn = true;
    this.spinner.hide();
    this.addSubmitbtn = true;
    this.editSubmitbtn = false;
    let obj1 = {
      "community_id": cmtid
    }
    this.CmtId = cmtid
    this.service.post(this.service.getcommunitybyid, obj1).subscribe(res => {
      this.indvCommunity = res;
      this.Cmtname = this.indvCommunity[0]["community_name"]
      this.regNo = this.indvCommunity[0]["registration_no"]
      this.image = this.indvCommunity[0]["community_image"]
      this.blocks = this.indvCommunity[0]["total_blocks"]
      this.flats = this.indvCommunity[0]["total_flats"]
      this.PhoneNum = this.indvCommunity[0]["mobile_no"]
      this.cmtEmail = this.indvCommunity[0]["email_id"]
      this.cityId = this.indvCommunity[0]["city_id"]
      this.stateId = this.indvCommunity[0]["state_id"]
      this.gstNum = this.indvCommunity[0]["gst_no"]
      this.pannum = this.indvCommunity[0]["pan_no"]
      this.addrs = this.indvCommunity[0]["address"]
      this.pincode = this.indvCommunity[0]["pincode"]
      this.cntName = this.indvCommunity[0]["contact_person"]
      this.Cntnum = this.indvCommunity[0]["mobile_no"]
      this.cntdesign = this.indvCommunity[0]["designation"]
      this.cntMail = this.indvCommunity[0]["contact_email_id"]
      this.selectedstate(this.stateId)
      this.upImgName = this.image
      if ((this.image == null) || (this.image == "")) {
        this.imgURL = "";
        this.prodImg = "";
      }
      else {
        this.imgURL = this.service.staticURL + 'cmmt_images/' + `${this.image}`
        this.prodImg = this.image
      }

      this.spinner.hide();
    })

    this.showform = false;
    this.showtbl = true
    this.shweditCmt = false;
  }
  // UPDATING COMMUNITY
  upCommunity(regNo, Cmtname, blocks, flats, addrs, PhoneNum, cmtEmail, gstNum, pannum, pincode, cntName, Cntnum, cntdesign, cntMail, ) {
    if (this.regNo == "" || this.regNo == undefined) {
      this.toast.showwarning(this.message.regNumber);
      return
    }
    if (this.Cmtname == "" || this.Cmtname == undefined) {
      this.toast.showwarning(this.message.cmmtName);
      return
    }
    if (this.addrs == "" || this.addrs == undefined) {
      this.toast.showwarning(this.message.cmmtAddress);
      return
    }
    if (this.stateId == "" || this.stateId == undefined) {
      this.toast.showwarning(this.message.cmmtPincode);
      return
    }
    if (this.pincode == "" || this.pincode == undefined) {
      this.toast.showwarning(this.message.cmmtState);
      return
    }
    if (this.cityId == "" || this.cityId == undefined) {
      this.toast.showwarning(this.message.cmmtCity);
      return
    }
    if (this.cntName == "" || this.cntName == undefined) {
      this.toast.showwarning(this.message.cntPersonName);
      return
    }
    if (this.Cntnum == "" || this.Cntnum == undefined) {
      this.toast.showwarning(this.message.cntPersonNumber);
      return
    }
    if (this.cntdesign == "" || this.cntdesign == undefined) {
      this.toast.showwarning(this.message.cntPersonDesgn);
      return
    }
    if (this.cntMail == "" || this.cntMail == undefined) {
      this.toast.showwarning(this.message.cntPersonEmail);
      return
    }
    else {
      this.spinner.show();
      if (this.image == this.upImgName) {
        this.upImgName = this.image
      }
      else {
        this.service.post(this.service.cmtinsertimg, this.fd)
          .subscribe(res => {
          });
      }
      this.newCommunity = {
        "community_id": this.CmtId,
        "community_name": Cmtname,
        "registration_no": regNo,
        "total_blocks": blocks,
        "total_flats": flats,
        "phone_no": PhoneNum,
        "email_id": cmtEmail,
        "address": addrs,
        "state_id": this.stateId,
        "city_id": this.cityId,
        "pincode": pincode,
        "pan_no": pannum,
        "gst_no": gstNum,
        "community_image": this.upImgName,
        "contact_person": cntName,
        "mobile_no": Cntnum,
        "designation": cntdesign,
        "contact_email_id": cntMail,
        "created_by": "1",
        "updated_by": "1",
      }
      this.service.post(this.service.updatecommunities, this.newCommunity).subscribe(res => {
        if (res == "Updated") {
          this.toast.showsuccess(this.message.UpCommunity);
          this.getcommunities();
          this.backtotbl();
        }
        else {
          this.toast.showwarning(this.message.CmtDuplicate);
          this.spinner.hide();
        }
      })
    }

  }
  // BACK TO DISPLAY TABLE WHILE EDITING
  backtotbl() {
    this.addbtn = false;
    this.addSubmitbtn = true;
    this.editSubmitbtn = false;
    this.showform = true;
    this.showtbl = false
    this.shweditCmt = true;
    this.srcbtn = false;
  }
  // ALLOWING ONLY NUMBERS
  returnnumberOnly(event): boolean {
    return this.validations.numberOnly(event)
  }
  // ONLY NUMERICS AND ALPHABETS
  returnnosplChar(event): boolean {
    return this.validations.nosplChar(event)
  } 
  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }
  // EMAIL VALIDATIONS ALLOWING CHARACTERS & NUMERICS & @ & .
  returnvalidateEmail(event): boolean {
    return this.validations.validateEmail(event)
  }

  openFile(event) {
    this.count++
    this.filesToUpload = event.target.files;
    const files: Array<File> = this.filesToUpload;
    let time = new Date();
    let timestamp = time.getTime();
    // let prodImg = timestamp + '-' + files[0]['name'];
    this.prodImg = timestamp + '-' + files[0]['name'];
    this.upImgName = files[0]['name'];
    if (files[0] == null) {
      return;
    }
    var reader = new FileReader();
    reader.onload = (val: any) => {
      this.imgURL = val.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
    this.fd.append("uploads", files[0], this.prodImg);
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allcmtList.filter(function (categiry) {
        return (
          categiry.community_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          ((categiry.total_flats == null) ? '': categiry.total_flats.indexOf(event) > -1  ) ||
          ((categiry.email_id == null) ? '': categiry.email_id.indexOf(event) > -1  ) ||
          categiry.mobile_no.indexOf(event) > -1 
        )
      });
      if (temp.length !== 0) {
        this.allcommunities = new MatTableDataSource(temp);
        this.allcommunities.paginator = this.paginator;
        this.records = false;
        this.norecords = true;
      } else if (temp.length == 0) {
        this.records = true;
        this.norecords = false;
        this.allcommunities = new MatTableDataSource(temp);
        this.allcommunities.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.allcommunities = this.allcmtList;
      this.allcommunities = new MatTableDataSource(this.allcmtList);
      this.allcommunities.paginator = this.paginator;
      this.records = false;
      this.norecords = true;
    }
  }


}
