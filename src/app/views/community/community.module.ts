import { communityroutes } from './community.routing';
import { ApiService } from '../services/api.service';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommunityComponent } from './community.component';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms'

import {
    MatIconModule,
    MatDialogModule,
    MatButtonModule, 
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
} from '@angular/material';
import { ToasterService } from '../services/toaster.service';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatOptionModule,
        MatCardModule,
        MatListModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        CommonModule,
        MatInputModule,
        FormsModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        RouterModule.forChild(communityroutes),
    ],
    providers: [ApiService,ToasterService],
    declarations: [CommunityComponent
    ]
}) 

export class CommunityModule { }