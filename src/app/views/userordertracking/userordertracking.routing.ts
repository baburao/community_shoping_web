import { Routes } from '@angular/router';

import { UserOrderTrackingComponent } from './userordertracking.component';


export const UserOrderTrackingRoutes: Routes = [
  { path: '', component: UserOrderTrackingComponent }
];