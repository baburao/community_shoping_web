//NAME : BABURAO M
//DESCRIPTION : USER ORDER TRACKING
//DATE : 01/06/2019


import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup, MatDialogConfig } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
// import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';

import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';

import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-userordertracking',
  templateUrl: './userordertracking.component.html',
  styleUrls: ['./userordertracking.component.css']
})
export class UserOrderTrackingComponent implements OnInit {
  userOrderTrackingData;
  allusrordtrackinglist:any;
  productOPrderTrackingStatus;
  orderTables: boolean = false;
  orderDetailsTables: boolean = true;
  backtrack: boolean = true;
  noRecords: boolean = true;
  countProducts = 0;
  cancelbtnpath;

  constructor(private router: Router, private service: ApiService,
    public dialogue: MatDialog,
    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) paginator1: MatPaginator;
  //USER ORDERS DISPLAY TABLE
  displayedColumns: string[] = ['order_id', 'order_date', 'total_products', 'amount', 'transaction_status', 'order_status', 'Actions'];

  // PRODUCT STATUS IN ORDER TRACKING BY ORDER ID
  displayedColumns1: string[] = ['product_name', 'price', 'quantity', 'totalprice', 'order_date', 'order_time', 'order_detail_status'];


  ngOnInit() {
    this.cancelbtnpath = this.service.staticURL + `${'products_images/order-cancel.png'}`;

    if ((this.storage.get("cartitem") == null)) {
      this.countProducts = 0;
    } else {
      this.countProducts = this.storage.get("cartitem").length;
    }
    this.spinner.show();
    this.getuserlisrdata();
  }

  //get user lisr data
  getuserlisrdata() {
    let userObj = {
      user_id: this.storage.get('user_id')
    }
    this.service.post(this.service.getordertrackingdatabyuserid, userObj).subscribe((res) => {
      if (res.length != 0) {
        this.userOrderTrackingData = res;
        this.allusrordtrackinglist = res;
        this.userOrderTrackingData = new MatTableDataSource(res);
        this.userOrderTrackingData.paginator = this.paginator;
        this.spinner.hide();
      } else {
        this.noRecords = false;
        this.orderTables = true;
        this.spinner.hide();
      }
    });
  }

  //PRODUCT ORDER TRACKING BY ORDER ID
  showProductsByOrderId(orderid) {
    this.spinner.show();
    let orderObj = {
      order_id: orderid
    }
    this.service.post(this.service.getproductsordertrackingbyorderid, orderObj).subscribe((res1) => {
      this.productOPrderTrackingStatus = res1;
      this.productOPrderTrackingStatus = new MatTableDataSource(res1);
      this.productOPrderTrackingStatus.paginator1 = this.paginator1;
      this.orderTables = true;
      this.orderDetailsTables = false;
      this.backtrack = false;
      this.spinner.hide();
    });
  }

  showProductsBackBtn() {
    this.orderTables = false;
    this.orderDetailsTables = true;
    this.backtrack = true;
  }


  // cancel order 
  cancel_order(ord_id) {
    this.spinner.show();
    let cancel_obj = {
      order_id: ord_id
    }
    this.service.post(this.service.getuserordercancellation, cancel_obj).subscribe((res) => {
      if (res = 'Cancelled') {
        this.getuserlisrdata();
        this.spinner.hide();
        this.toast.showsuccess(this.message.orderCancelled);
      }
    })
  }

 // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allusrordtrackinglist.filter(function (categiry) {
        return (
          categiry.transaction_status.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.order_id.indexOf(event) > -1 ||
          categiry.order_date.indexOf(event) > -1 ||
          categiry.total_products.indexOf(event) > -1 ||
          categiry.amount.indexOf(event) > -1 ||
          categiry.products_detail_status.toLowerCase().indexOf(event.toLowerCase()) > -1
        )
      });
      if (temp.length !== 0) {
        this.userOrderTrackingData = new MatTableDataSource(temp);
        this.userOrderTrackingData.paginator = this.paginator;
        this.orderTables = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.orderTables = true;
        this.noRecords = false;
        this.userOrderTrackingData = new MatTableDataSource(temp);
        this.userOrderTrackingData.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.userOrderTrackingData = this.allusrordtrackinglist
      this.userOrderTrackingData = new MatTableDataSource(this.allusrordtrackinglist);
      this.userOrderTrackingData.paginator = this.paginator;
      this.orderTables = false;
      this.noRecords = true;
    }
  }
}


