import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//mport {MessageService } from '../services/message.service';
//import { ToasterService } from '../services/toaster.service';
import {
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,  
  MatChipsModule,
  MatListModule,
  MatGridListModule,
  MatTabsModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,

  MAT_DIALOG_DEFAULT_OPTIONS,
  MAT_DATE_LOCALE
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserOrderTrackingComponent } from './userordertracking.component';
import { UserOrderTrackingRoutes } from './userordertracking.routing';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
// import { CommonDirectivesModule } from './sdirectives/common/common-directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    MatButtonModule,  
    MatChipsModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    

    RouterModule.forChild(UserOrderTrackingRoutes),
    NgxLoadingModule.forRoot({animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'})
  ],
  declarations: [UserOrderTrackingComponent],
  providers: [
    
    //MessageService
   
  ],
  entryComponents:[]
})
export class UserOrderTrackingModule { }