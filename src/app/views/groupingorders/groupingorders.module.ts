import {groupingordersroutes  } from './groupingorders.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GroupingordersComponent, GroupingorderModalComponent } from './groupingorders.component';
import {FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';  
import {
    MatIconModule,
    MatDialogModule,
    MatButtonModule, 
    MatCardModule,
    MatListModule, 
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatCheckboxModule,
} from '@angular/material';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatOptionModule,
        MatCardModule,
        MatListModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatCheckboxModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild(groupingordersroutes),
    ],
    providers:[],
    declarations: [GroupingordersComponent,GroupingorderModalComponent
    ],
    entryComponents:[GroupingorderModalComponent]
})
export class GroupingordersModule{}
