import { Component, OnInit, Inject, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ToasterService } from '../services/toaster.service';
import { MessageService } from '../services/message.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-groupingorders',
  templateUrl: './groupingorders.component.html',
  styleUrls: ['./groupingorders.component.scss']
})
export class GroupingordersComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  allOrders: any;
  allordersList: any;
  vndbyprodId: any;
  mainOrders: boolean = true;
  vndOrders: boolean = true;
  records: boolean = false;
  norecords: boolean = true;

  displayedColumns: string[] = ['Product Name', 'Quantity', 'View'];

  constructor(private service: ApiService, private dialog: MatDialog, private toast: ToasterService,
    private message: MessageService, @Inject(LOCAL_STORAGE) private storage: StorageService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.mainOrders = false;
    this.vndOrders = true;
    this.getallorders();

  }
  // GETTING OVERALL PRODUCTS
  getallorders() {
    this.service.post(this.service.getoverallorders, []).subscribe(res => {
      this.allOrders = new MatTableDataSource(res)
      this.allordersList = res;
      this.allOrders.paginator = this.paginator
      if (res.length != 0) {
        this.records = false;
        this.norecords = true;
      }
      else {
        this.records = true;
        this.norecords = false;
      }
      this.spinner.hide();
    })
  }

  // PRODUCT ID WISE ORDERS
  showList(prodId) {
    this.spinner.show();
    let prodObj = {
      "product_id": prodId
    }
    this.service.post(this.service.getordersbyProdid, prodObj).subscribe(response => {
      this.vndbyprodId = [];
      this.service.post(this.service.getvndsbyProdid, prodObj).subscribe(res => {
        this.vndbyprodId = response
        for (var i = 0; i < this.vndbyprodId.length; i++) {
          this.vndbyprodId[i].vendor_id = null;
          this.vndbyprodId[i].vendor_price = null;
        }
        const dialogRef = this.dialog.open(GroupingorderModalComponent, {
          width: '950px',
          disableClose: true,
          autoFocus: true,
          data: {
            status: 'view',
            ordArray: this.vndbyprodId,
            vndArry: res
          }
        })
        dialogRef.afterClosed().subscribe(result => {
          if (result == true) {
            this.getallorders();
          }
        });
      })
      this.spinner.hide();
    })
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allordersList.filter(function (categiry) {
        return (
          categiry.product_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.quantity.indexOf(event) > -1
        )
      });
      if (temp.length !== 0) {
        this.allOrders = temp;
        this.allOrders = new MatTableDataSource(temp);
        this.allOrders.paginator = this.paginator;
        this.records = false;
        this.norecords = true;
      } else if (temp.length == 0) {
        this.records = true;
        this.norecords = false;
        this.allOrders = new MatTableDataSource(temp);
        this.allOrders.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.allOrders = this.allordersList;
      this.allOrders = new MatTableDataSource(this.allordersList);
      this.allOrders.paginator = this.paginator;
      this.records = false;
      this.norecords = true;
    }
  }
}

@Component({
  selector: 'app-groupingorder-modal',

  templateUrl: './groupingorder-modal.component.html',
  styleUrls: ['./groupingorders.component.scss']
})
export class GroupingorderModalComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  paymentArray = [];
  dt: any;
  vndId: any;
  odersData: any;
  tableData: any;
  count: number;
  counter1: number;
  odersbyProdId: any;
  vendorsbyprod: any;
  vendor_id: null;
  displayedColumns1: string[] = ['order id', 'order Date', 'User Id', 'Product Id', 'price', 'vendor price', 'Quantity', 'Vendors'];
  displayedColumns2: string[] = ['Product Id ', 'Vendor Name', 'Actions']
  constructor(private service: ApiService, private toast: ToasterService, private message: MessageService, private spinner: NgxSpinnerService,
    public dialog: MatDialogRef<GroupingordersComponent>,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: GroupingOrdersData,

  ) { }
  ngOnInit() {
    this.count = 0;
    this.counter1 = 0;
    this.loadtables();
    // this.getVendors();


  }
  // Loading Tables
  loadtables() {
    this.getordsData()
    this.vendorsbyprod = this.data.vndArry
    this.odersData.paginator = this.paginator;
  }

  // GETTING ORDERD
  getordsData() {
    this.odersData = new MatTableDataSource(this.data.ordArray)
  }




  // CLOSING DIALOG
  closeDialog(flag) {
    this.dialog.close(flag);
  }

  // SELECTING VENDOR NAME
  selectvndName(vndId, index) {
    // this.spinner.show();

    let vndObj = {
      "vendor_id": vndId,
      "product_id": this.odersData.data[index]['product_id']
    }
    this.service.post(this.service.getvndPrice, vndObj).subscribe(res => {
      let vndPrice = res[0]["vendor_price"];
      this.counter1 = 0;
      if (this.count == 0) {
        for (var i = 0; i < this.odersData.data.length; i++) {
          this.count++
          this.vndId = vndId;
          var id = 'vndprice' + i;
          let elem: HTMLElement = document.getElementById(id);
          elem.innerHTML = vndPrice;
          this.odersData.data[i]['vendor_id'] = vndId;
          this.odersData.data[i]['vendor_price'] = vndPrice;
          // var id = 'vnd' + i;
          // let elem2: HTMLElement = document.getElementById(id);
          // elem2.innerText=vnd['name']
        }
      } else {
        var id = 'vndprice' + index;
        let elem: HTMLElement = document.getElementById(id);
        elem.innerHTML = vndPrice;
        this.odersData.data[index]['vendor_id'] = vndId;
        this.odersData.data[index]['vendor_price'] = vndPrice;
      }
      this.getordsData();
    })
  }
  // ASSIGNING ORDERS TO VENDOR
  assgnordstovnd() {
    for (var i = 0; i < this.odersData.data.length; i++) {
      if ((this.odersData.data[i]['vendor_id'] == null) || (this.odersData.data[i]['vendor_id'] == '')) {
        this.counter1++
        this.toast.showwarning(this.message.selVendor)
      }
    }
    if (this.counter1 == 0) {
      this.spinner.show();
      let ordgrpObj = {
        "OrdData": this.odersData.data,
        "createdBy": this.storage.get('portal_admin_id'),
        "updateBy": this.storage.get('portal_admin_id')
      }
      // console.log(ordgrpObj)
      this.service.post(this.service.orderprodvndassign, ordgrpObj).subscribe(res => {
        if (res == 'Inserted') {
          this.spinner.hide();
          this.dialog.close(true);
        }
      })

    }
  }



}

export interface GroupingOrdersData {
  status: string,
  ProductId: number,
  ProductName: string,
  Quantity: number,
  vndArry: any
  ordArray: any
}




