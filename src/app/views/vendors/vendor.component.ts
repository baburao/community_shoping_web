//NAME : BABURAO M
//DESCRIPTION : VENDOR CREATIONS AND MODIFICATIONS
//DATE : 12/05/2019


import { Component, OnInit, Inject, ViewChildren, QueryList, } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatPaginator, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSort } from '@angular/material';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';
@Component({
  selector: 'app-login',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  vendor_id;
  vendortable: boolean;
  vendorcreation: boolean;
  addVendorBtn: boolean;
  vendorBack: boolean;
  subbtn: boolean;
  updbtn: boolean;
  vndCreation: boolean = true;
  vndProducts: boolean = true;
  showtbl; boolean = true;
  addsingleProd: boolean = true;
  addtolist: boolean = true;
  prodAddbtn: boolean = true;
  backprodList: boolean = true;
  vndrecords: boolean = false;
  vndnorecords: boolean = true;
  vndprodrecords: boolean = false;
  vndprodnorecords: boolean = true;
  vndProductsList = [];
  vendorData: any;
  status;   // private message: MessageService
  vndTblData: any;
  prodCategory: any;
  prodDetails: any;
  prdName = "";
  prdCategory = "";
  vndProdtblData: any;
  SelectedProdArray: any
  bckvnd: boolean = true
  srcbtn: boolean = false;
  srcbtn1: boolean = true;
  allvndList : any;
  searchvalue :string = '';

  //VENDOR OBJECT
  vendorObj = {
    first_name: '',
    last_name: '',
    phone_no: '',
    email_id: '',
    password: '',
    address: '',
    re_password: '',
    state_id: '',
    city_id: '',
    pincode: '',
    pan_no: '',
    gst_no: '',
    account_no: '',
    ifsccode: '',
    branch: '',
    bankname: '',
    holdername: '',
    vendor_id: '',
    created_by: '',
    updated_by: '',
    accounttype: '',
  }



  constructor(private router: Router, private service: ApiService, private spinner: NgxSpinnerService,
    private validations: ValidatorService,
    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
  ) { }



  displayedColumns: string[] = ['name', 'phone_no', 'email_id', 'address', 'gst_no', 'pan_no', 'Products', 'Actions'];
  // DISPLAYING SINGLE VENDOR PRODUCTS
  displayedColumns1: string[] = ['product_name', 'weight', 'price', 'vendor price', 'brand_name', 'Actions'];


  displayedColumns2: string[] = ['prodName', 'weight', 'price', 'vendor price', 'brand_name', 'Actions'];
  ngOnInit() {
    this.spinner.show();
    this.getvendorlist();
    this.vendortable = false;
    this.vendorcreation = true;
    this.addVendorBtn = false;
    this.vendorBack = true;
    this.vndCreation = false;
    this.vndProducts = true;
    this.showtbl = true;


  }

  //GET VENDOR LIST
  getvendorlist() {
    this.service.post(this.service.getallvendor, '').subscribe((res) => {
      this.allvndList = res;
      this.vendorData = new MatTableDataSource(res);
      this.vendorData.paginator = this.paginator.toArray()[0];
      this.vendorData.sort = this.sort.toArray()[0];
      if (res.length != 0) {
        this.vendortable = false;
        this.vndnorecords = true;
      }
      else {
        this.vendortable = true;
        this.vndnorecords = false;
      }
      this.spinner.hide();
    });
  }
  //ADD VENDORS
  addVendor() {
    this.openDialog1('add');
  }
  //MODEL DIALOG CODE HERE 
  openDialog1(val): void {
    const dialogRef = this.dialog.open(VendorDialog, {
      width: '850px',
      disableClose: true,
      data: {
        vendor_id: this.vendorObj.vendor_id,
        first_name: this.vendorObj.first_name,
        last_name: this.vendorObj.last_name,
        phone_no: this.vendorObj.phone_no,
        address: this.vendorObj.address,
        state_id: this.vendorObj.state_id,
        city_id: this.vendorObj.city_id,
        pincode: this.vendorObj.pincode,
        pan_no: this.vendorObj.pan_no,
        gst_no: this.vendorObj.gst_no,
        account_no: this.vendorObj.account_no,
        ifsccode: this.vendorObj.ifsccode,
        branch: this.vendorObj.branch,
        bankname: this.vendorObj.bankname,
        holdername: this.vendorObj.holdername,
        accounttype: this.vendorObj.accounttype,
        email_id: this.vendorObj.email_id,
        password: this.vendorObj.password,
        re_password: this.vendorObj.re_password,
        status: val,
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      this.getvendorlist();
      this.vendorObj.vendor_id = '';
      this.vendorObj.first_name = '';
      this.vendorObj.last_name = '';
      this.vendorObj.phone_no = '';
      this.vendorObj.address = '';
      this.vendorObj.state_id = '';
      this.vendorObj.city_id = '';
      this.vendorObj.pincode = '';
      this.vendorObj.pan_no = '';
      this.vendorObj.gst_no = '';
      this.vendorObj.gst_no = '';
      this.vendorObj.account_no = '';
      this.vendorObj.accounttype = '';
      this.vendorObj.ifsccode = '';
      this.vendorObj.branch = '';
      this.vendorObj.bankname = '';
      this.vendorObj.holdername = '';
      this.vendorObj.email_id = '';
      this.vendorObj.password = '';
      this.vendorObj.re_password = '';

    });
  }

  //INACTIVE VENDOR
  inactiveVendor(vendorid) {
    this.spinner.show();
    let statusVendorObj = {
      vendor_id: vendorid,
      is_active: "N"
    }
    this.service.post(this.service.updatevendorstatus, statusVendorObj).subscribe((res) => {
      this.toast.showsuccess(this.message.vndInactive);
      this.getvendorlist();
    });
  }

  //ACTIVE VENDOR
  activeVendor(vendorid) {
    this.spinner.show();
    let statusVendorObj = {
      vendor_id: vendorid,
      is_active: "Y"
    }
    this.service.post(this.service.updatevendorstatus, statusVendorObj).subscribe((res) => {
      this.toast.showsuccess(this.message.vndActive);
      this.getvendorlist();
    });
  }

  //EDIT VENDOR
  editVendor(vendorid) {
    this.spinner.show();
    let editVendorObj = {
      vendor_id: vendorid,
      is_active: "Y"
    }
    this.service.post(this.service.getvendorbyid, editVendorObj).subscribe((res) => {
      this.vendorObj.vendor_id = res[0]['vendor_id'];
      this.vendorObj.first_name = res[0]['first_name'];
      this.vendorObj.last_name = res[0]['last_name'];
      this.vendorObj.phone_no = res[0]['phone_no'];
      this.vendorObj.email_id = res[0]['email_id'];

      this.vendorObj.address = res[0]['address'];
      this.vendorObj.state_id = res[0]['state_id'];
      this.vendorObj.city_id = res[0]['city_id'];
      this.vendorObj.pincode = res[0]['pincode'];
      this.vendorObj.pan_no = res[0]['pan_no'];
      this.vendorObj.gst_no = res[0]['gst_no'];

      this.vendorObj.account_no = res[0]['accountno'];
      this.vendorObj.accounttype = res[0]['accounttype'];
      this.vendorObj.ifsccode = res[0]['ifsccode'];
      this.vendorObj.branch = res[0]['branch'];
      this.vendorObj.bankname = res[0]['bankname'];
      this.vendorObj.holdername = res[0]['holdername'];
      this.openDialog1('edit');
      this.vendortable = false;
      this.vendorcreation = false;
      this.addVendorBtn = false;
      this.subbtn = false;
      this.updbtn = true
      this.spinner.hide();
    });
  }


  // VENDOR WISE PRODUCTS
  displayProducts(vndId) {
    this.spinner.show();
    this.addVendorBtn = true;
    this.vndProdList(vndId)
    this.vendor_id = vndId
    this.prodAddbtn = false;
    this.vndCreation = true;
    this.vndProducts = false;
    this.showtbl = false;
    this.backprodList = true;
    this.bckvnd = false;
    this.srcbtn = true;
    this.srcbtn1 = false;
    this.searchvalue = '';
  }
  // GETTING VENDOR PRODUCTS
  vndProdList(id) {
    let obj = {
      "vendor_id": id
    }
    this.service.post(this.service.getvndProducts, obj).subscribe(res1 => {
      this.vndProductsList = res1
      this.vndTblData = new MatTableDataSource(res1)
      this.vndTblData.paginator = this.paginator.toArray()[1];
      this.vndTblData.sort = this.sort.toArray()[1];
      if (res1.length != 0) {
        this.vndprodrecords = false;
        this.vndprodnorecords = true;
      }
      else {
        this.vndprodrecords = true;
        this.vndprodnorecords = false;
      }
      this.spinner.hide();
    })

  }
  // ADDING PRODUCTS TO VENDOR
  addingproducts() {
    this.spinner.show();
    this.SelectedProdArray = [];
    this.vndProdtblData = [];
    this.prdCategory = '';
    this.prdName = '';
    this.addsingleProd = false;
    this.vndCreation = true;
    this.vndProducts = false;
    this.showtbl = true;
    this.prodAddbtn = true;
    this.backprodList = false;
    this.bckvnd = true;
    this.srcbtn = true;
    this.srcbtn1 = true;
    // SERVICE TO CALL PRODUCT CATEGORY
    this.service.post(this.service.getActiveCategory, []).subscribe(res => {
      this.prodCategory = res
      this.spinner.hide();
    })

  }
  // CATEGORY WISE PRODUCTS
  prodbyCategory(value) {
    this.spinner.show();
    let obj = {
      "product_category_id": value
    }
    this.service.post(this.service.getproductsbycategory, obj).subscribe(res => {
      this.prodDetails = res
      this.spinner.hide();
    })
  }

  // ADD PRODUCTS INTO LIST
  insertproduct(value) {

    if ((value == '') || (value == undefined))
      return;
    this.spinner.show();
    let dupProduct = this.SelectedProdArray.filter(x => x.product_name == value)
    if (dupProduct.length > 0) {
      this.toast.showwarning(this.message.prodSelected);
      this.spinner.hide();
    }
    else {
      let singleProd = this.prodDetails.filter(m => m.product_name == value)
      let obj = {
        "brand_name": singleProd[0]["brand_name"],
        "category_name": singleProd[0]["category_name"],
        "price": singleProd[0]["price"],
        "vendor_price": 0,
        "product_id": singleProd[0]["product_id"],
        "product_image": singleProd[0]["product_image"],
        "product_name": singleProd[0]["product_name"],
        "weight": singleProd[0]["weight"]
      }

      this.SelectedProdArray.push(obj)
      this.vndProdtblData = new MatTableDataSource(this.SelectedProdArray)
      this.vndProdtblData.paginator = this.paginator.toArray()[2];
      this.vndProdtblData.sort = this.sort.toArray()[2];
      if (this.SelectedProdArray.length > 0)
        this.addtolist = false
      this.spinner.hide();
    }
  }
  // DELEDTING THE PRODUCT FROM THE SELECTED LIST
  deleteSelprod(prodId) {
    let prodArray = [];
    for (var i = 0; i < this.SelectedProdArray.length; i++) {
      if (this.SelectedProdArray[i]["product_id"] == prodId) {
        continue
      }
      else {
        prodArray.push(this.SelectedProdArray[i])
      }
    }
    this.SelectedProdArray = prodArray
    this.vndProdtblData = new MatTableDataSource(prodArray)
    if (this.SelectedProdArray.length == 0)
      this.addtolist = true
  }
  // BACK TO VENDOR PRODUCTS LIST
  backtoprodList() {
    this.prodAddbtn = false;
    this.vndCreation = true;
    this.vndProducts = false;
    this.showtbl = false;
    this.backprodList = true;
    this.addsingleProd = true;
    this.bckvnd = false;
    this.srcbtn = true;
    this.srcbtn1 = false;
    this.addtolist = true
    this.searchvalue = '';
  }

  // ASSIGNING SELECTED PRODUCTS TO VENDOR LIST
  addtoVndProdList() {
    this.spinner.show();
    // CHECKING DUPLICATE PRODUCT WITH THE VENDOR-PRODUCT LIST
    var count = 0;
    var pricecounter = 0;
    for (var i = 0; i < this.vndProdtblData.data.length; i++) {
      let dupRow = this.vndTblData.data.filter(m => m.product_name == this.vndProdtblData.data[i]["product_name"])
      if (dupRow.length > 0) {
        count++;
        var id = 'row' + i
        let elem: HTMLElement = document.getElementById(id);
        elem.setAttribute("style", "background-color:#ff000045;");
      }
      if (this.vndProdtblData.data[i]['vendor_price'] == 0) {
        pricecounter++;
      }
    }
    if (count > 0) {
      this.toast.showwarning(this.message.dupProdExits)
      this.spinner.hide();
      return
    } else if (pricecounter > 0) {
      this.toast.showwarning(this.message.vndPrice)
      this.spinner.hide();
    }
    else if (count == 0 && pricecounter == 0) {
      let obj = {
        "vendor_id": this.vendor_id,
        "created_by": 1,
        "updated_by": 1,
        "products": this.SelectedProdArray,
      }
      this.service.post(this.service.insertvendorprodmap, obj).subscribe(res => {
        this.vndProdList(this.vendor_id);
      })
      this.backtoprodList()
    }


  }

  // DELETING PRODUCT FROM VENDOR'S LIST
  deleteprod(id) {
    this.spinner.show();
    let obj = {
      "vendor_id": this.vendor_id,
      "product_id": id
    }
    this.service.post(this.service.delvndProduct, obj).subscribe(res => {
    })
    this.vndProdList(this.vendor_id)
  }
  // BACK TO VENDOR LIST FROM PRDUCTS
  backtovnd() {
    this.srcbtn = false;
    this.srcbtn1 = true;
    this.addVendorBtn = false;
    this.vndProducts = true;
    this.vndCreation = false;
    this.searchvalue = '';
  }
  // ASSIGNING VENDOR PRICE TO VENDOR
  vndPrice(event, index) {
    this.vndProdtblData.data[index]["vendor_price"] = event
  }

  // SEARCH FUNCTIONALITY
  applyFilter(event,status) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if(status == 'vendor'){
      if (eventlength != "" && eventlength > lengthofevent) {
        let temp = this.allvndList.filter(function (categiry) {
          return (
            categiry.name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
            categiry.phone_no.indexOf(event) > -1 ||
            categiry.email_id.indexOf(event) > -1 ||
            categiry.address.indexOf(event) > -1 ||
            categiry.gst_no.indexOf(event) > -1 ||
            categiry.pan_no.indexOf(event) > -1 ||
            categiry.gst_no.indexOf(event) > -1 
          )
        });
        if (temp.length !== 0) {
          this.vendorData = new MatTableDataSource(temp);
          this.vendorData.paginator = this.paginator;
          this.vendortable = false;
          this.vndnorecords = true;
        } else if (temp.length == 0) {
          this.vendortable = true;
          this.vndnorecords = false;
          this.vendorData = new MatTableDataSource(temp);
          this.vendorData.paginator = this.paginator;
        }
      } else if (eventlength <= lengthofevent) {
        this.vendorData = this.allvndList;
        this.vendorData = new MatTableDataSource(this.allvndList);
        this.vendorData.paginator = this.paginator;
        this.vendortable = false;
        this.vndnorecords = true;
      }
    }else if(status == 'vndproducts'){
      if (eventlength != "" && eventlength > lengthofevent) {
        let temp = this.vndProductsList.filter(function (categiry) {
          return (
            categiry.product_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
            categiry.weight.indexOf(event) > -1 ||
            categiry.price.indexOf(event) > -1 ||
            categiry.vendor_price.indexOf(event) > -1 ||
            categiry.brand_name.toLowerCase().indexOf(event.toLowerCase()) > -1
          )
        });
        if (temp.length !== 0) {
          this.vndTblData = new MatTableDataSource(temp);
          this.vndTblData.paginator = this.paginator;
          this.vndprodrecords = false;
          this.vndprodnorecords = true;
        } else if (temp.length == 0) {
          this.vndprodrecords = true;
          this.vndprodnorecords = false;
          this.vndTblData = new MatTableDataSource(temp);
          this.vndTblData.paginator = this.paginator;
        }
      } else if (eventlength <= lengthofevent) {
        this.vndTblData = this.vndProductsList;
        this.vndTblData = new MatTableDataSource(this.vndProductsList);
        this.vndTblData.paginator = this.paginator;
        this.vndprodrecords = false;
        this.vndprodnorecords = true;
      }
    }
    
  }
}

@Component({
  selector: 'vendor-model',
  templateUrl: 'vendor-model.html',
})
export class VendorDialog {
  constructor(private service: ApiService,
    public dialogRef: MatDialogRef<VendorDialog>,
    private toast: ToasterService, private message: MessageService, private validations: ValidatorService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.data.payment_amount = this.data.balance_amount
  }

  //VENDOR OBJECT
  vendorObj = {
    first_name: '',
    last_name: '',
    phone_no: '',
    email_id: '',
    password: '',
    address: '',
    re_password: '',
    state_id: '',
    city_id: '',
    pincode: '',
    pan_no: '',
    gst_no: '',
    account_no: '',
    ifsccode: '',
    branch: '',
    bankname: '',
    holdername: '',
    vendor_id: '',
    created_by: '',
    updated_by: '',
    accounttype: '',
  }
  states;
  cityes;
  city_id;
  state_id;
  ngOnInit() {
    this.getstates();
    this.getcities();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  //get states
  getstates() {

    this.service.post(this.service.getallstate, '').subscribe((res) => {
      this.states = res;
    });
  }

  //cities
  getcities() {

    this.service.post(this.service.getallcities, '').subscribe((res) => {
      this.cityes = res;
    });
  }
  //GET CITY ID
  selectedCity(event) {
    this.city_id = event;
  }
  //GET CITYS BY STATE ID
  getcitiesbystateId(stateid) {
    this.state_id = stateid
    let stateobj = {
      state_id: stateid,
    }
    this.service.post(this.service.getcitiesbystate, stateobj).subscribe((res) => {
      this.cityes = res;
    });
  }

  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }


  // CHECKING EMAIL VALIDATION
  chkemail(event): boolean {
    return this.validations.validateEmail(event)
  }

  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validations.nosplChar(event)
  }

  // ALLOWING ONLY NUMBERS
  returnalphaOnly(event): boolean {
    return this.validations.alphabetsonly(event)
  }
  vendorinsert(element) {
    if (element.first_name == null || element.first_name == '') {
      this.toast.showwarning(this.message.vndftName);
      return
    } else if (element.last_name == null || element.last_name == '') {
      this.toast.showwarning(this.message.vndltName);
      return
    } else if (element.phone_no == null || element.phone_no == '') {
      this.toast.showwarning(this.message.vndNum);
      return
    } else if (element.state_id == null || element.state_id == '') {
      this.toast.showwarning(this.message.stateid);
      return
    } else if (element.city_id == null || element.city_id == '') {
      this.toast.showwarning(this.message.cityid);
      return
    } else if (element.account_no == null || element.account_no == '') {
      this.toast.showwarning(this.message.account_no);
      return
    } else if (element.accounttype == null || element.accounttype == '') {
      this.toast.showwarning(this.message.accounttype);
      return
    } else if (element.ifsccode == null || element.ifsccode == '') {
      this.toast.showwarning(this.message.ifsccode);
      return
    } else if (element.branch == null || element.branch == '') {
      this.toast.showwarning(this.message.branch);
      return
    } else if (element.bankname == null || element.bankname == '') {
      this.toast.showwarning(this.message.bankname);
      return
    } else if (element.holdername == null || element.holdername == '') {
      this.toast.showwarning(this.message.holdername);
      return
    } else if (element.email_id == null || element.email_id == '') {
      this.toast.showwarning(this.message.vndEmail);
      return
    } else if (element.password == null || element.password == '') {
      this.toast.showwarning(this.message.vndPswd);
      return
    } else if (element.re_password == null || element.re_password == '') {
      this.toast.showwarning(this.message.vndRePswd);
      return
    } else {
      this.vendorObj.first_name = element.first_name;
      this.vendorObj.last_name = element.last_name;
      this.vendorObj.phone_no = element.phone_no;
      this.vendorObj.state_id = element.state_id;
      this.vendorObj.city_id = element.city_id;
      this.vendorObj.account_no = element.account_no;
      this.vendorObj.accounttype = element.accounttype;
      this.vendorObj.ifsccode = element.ifsccode;
      this.vendorObj.branch = element.branch;
      this.vendorObj.bankname = element.bankname;
      this.vendorObj.holdername = element.holdername;
      this.vendorObj.email_id = element.email_id;
      this.vendorObj.password = element.password;
      this.vendorObj.re_password = element.re_password;
      this.vendorObj.created_by = "1";
      this.vendorObj.updated_by = "1";
      this.vendorObj.address = element.address;
      this.vendorObj.city_id = element.city_id;
      this.vendorObj.gst_no = element.gst_no;
      this.vendorObj.pan_no = element.pan_no;
      this.vendorObj.pincode = element.pincode;
      this.vendorObj.state_id = element.state_id;
      this.service.post(this.service.insertvendor, this.vendorObj).subscribe((res) => {
        if (res == 'Duplicate') {
          this.toast.showsuccess(this.message.CmtDuplicate);
        } else {
          this.toast.showsuccess(this.message.crtVendor);
          this.onNoClick()
        }
      })
    }
  }
  // UPDATE VENDOR
  updatevendor(element) {
    if (element.first_name == null || element.first_name == '') {
      this.toast.showwarning(this.message.vndftName);
      return
    } else if (element.last_name == null || element.last_name == '') {
      this.toast.showwarning(this.message.vndltName);
      return
    } else if (element.phone_no == null || element.phone_no == '') {
      this.toast.showwarning(this.message.vndNum);
      return
    } else if (element.state_id == null || element.state_id == '') {
      this.toast.showwarning(this.message.stateid);
      return
    } else if (element.city_id == null || element.city_id == '') {
      this.toast.showwarning(this.message.cityid);
      return
    } else if (element.account_no == null || element.account_no == '') {
      this.toast.showwarning(this.message.account_no);
      return
    } else if (element.accounttype == null || element.accounttype == '') {
      this.toast.showwarning(this.message.accounttype);
      return
    } else if (element.ifsccode == null || element.ifsccode == '') {
      this.toast.showwarning(this.message.ifsccode);
      return
    } else if (element.branch == null || element.branch == '') {
      this.toast.showwarning(this.message.branch);
      return
    } else if (element.bankname == null || element.bankname == '') {
      this.toast.showwarning(this.message.bankname);
      return
    } else if (element.holdername == null || element.holdername == '') {
      this.toast.showwarning(this.message.holdername);
      return
    } else if (element.email_id == null || element.email_id == '') {
      this.toast.showwarning(this.message.vndEmail);
      return
    } else {
      this.vendorObj.vendor_id = element.vendor_id;
      this.vendorObj.first_name = element.first_name;
      this.vendorObj.last_name = element.last_name;
      this.vendorObj.phone_no = element.phone_no;
      this.vendorObj.address = element.address;
      this.vendorObj.state_id = element.state_id;
      this.vendorObj.city_id = element.city_id;
      this.vendorObj.pincode = element.pincode;
      this.vendorObj.pan_no = element.pan_no;
      this.vendorObj.gst_no = element.gst_no;
      this.vendorObj.account_no = element.account_no;
      this.vendorObj.accounttype = element.accounttype;
      this.vendorObj.ifsccode = element.ifsccode;
      this.vendorObj.branch = element.branch;
      this.vendorObj.bankname = element.bankname;
      this.vendorObj.holdername = element.holdername;
      this.vendorObj.email_id = element.email_id;
      this.vendorObj.password = element.password;
      this.vendorObj.re_password = element.re_password;
      this.vendorObj.updated_by = "1";
      this.service.post(this.service.updatevendordetails, this.vendorObj).subscribe((res) => {
        var data = res;
        this.toast.showsuccess(this.message.upVendor);
        this.onNoClick()
      });
    }
  }
}





export interface DialogData {
  vendor_id;
  first_name;
  last_name;
  phone_no;
  address;
  state_id;
  city_id;
  pincode;
  pan_no;
  gst_no;
  account_no;
  ifsccode;
  branch;
  bankname;
  holdername;
  email_id;
  password;
  re_password;
  payment_amount;
  balance_amount;
  accounttype;
  status;

}