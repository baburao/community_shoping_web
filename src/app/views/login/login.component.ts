//NAME : BABURAO M
//DESCRIPTION : LOGIN USER,VENDOR AND PORTAL ADMIN
//DATE : 08/05/2019


import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';
// import FirebaseTokenGenerator = require("firebase-token-generator");
// import firebase = require('firebase');
// import {FirebaseProvider} from 'angular-firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;
  // @ViewChild('dropselect') dpselect :MatSelect;
  emailid = "";
  selectedItems: any;
  mobilenumber;
  password;
  status;   // private message: MessageService
  id;
  loginid;
  firstname;
  lastname;
  phone_no;
  block_name;
  flat_no;
  communitydata;
  community_id;
  created_by;
  updated_by;
  flatnumber;
  blockname;
  state_id = "";
  city_id = "";
  pincode = "";
  userObj = {
    first_name: '',
    last_name: '',
    phone_no: '',
    email_id: '',
    password: '',
    re_password: '',
    user_id: '',
    created_by: '',
    updated_by: '',
    community_id: '',
    block_name: '',
    flat_no: '',
    state_id: '',
    city_id: '',
    pincode: ''
  }
  vendorObj = {
    first_name: '',
    last_name: '',
    phone_no: '',
    email_id: '',
    password: '',
    address: '',
    re_password: '',
    state_id: '',
    city_id: '',
    pincode: '',
    pan_no: '',
    gst_no: '',
    vendor_id: '',
    created_by: '',
    updated_by: ''
  }
  dropdownSettings;
  mulcommunityData = [];
  categoryNames;
  states = [];
  cities = [];
  hidefirst: boolean;
  hidesecond: boolean;

  community: boolean = false;

  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService, private validation: ValidatorService,
    private toast: ToasterService, private spinner: NgxSpinnerService
    // ,private fb: FirebaseProvider
  ) { }

  ngOnInit() {
    this.hidefirst = false;
    this.hidesecond = true;
    this.getstates();
    this.storage.remove('user_id');
    this.storage.remove('loginname');
    this.storage.remove('user_community_id');
    this.storage.remove('user_flat_no');
    this.storage.remove('mobileNum');
    this.storage.remove('userrole');
    let data = "123";
    this.storage.set('loginname', data);
    this.status = "user";
    this.loginid = 2;
    // this.getCommunityData();
    this.dropdownSettings = {
      singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true,
      idField: 'item_id',
      textField: 'item_text',
    };
  }

  //GET STATES
  getstates() {
    this.service.post(this.service.getallstate, '').subscribe((res) => {
      this.states = res;
    });
  }


  //GET CITYS BY STATE ID
  getcitiesbystateId(stateid) {
    this.state_id = stateid
    let stateobj = {
      state_id: stateid,
    }
    this.service.post(this.service.getcitiesbystate, stateobj).subscribe((res) => {
      this.cities = res;
    });
  }

  //GET COMMUNITY DATA
  getCommunityData() {

    if (this.state_id == "" || this.state_id == undefined) {
      this.toast.showwarning(this.message.cmmtState);
      return
    }
    if (this.city_id == "" || this.city_id == undefined) {
      this.toast.showwarning(this.message.cmmtCity);
      return
    }
    if (this.pincode == "" || this.pincode == undefined) {
      this.toast.showwarning(this.message.cmmtPincode);
      return
    }


    let commobj = {
      state_id: this.state_id,
      city_id: this.city_id,
      pincode: this.pincode
    }
    this.service.post(this.service.getactivecommunity, commobj).subscribe((res) => {
      this.communitydata = res;
      for (let i = 0; i < this.communitydata.length; i++) {
        this.mulcommunityData.push(
          {
            'item_id': this.communitydata[i].community_id,
            'item_text': this.communitydata[i].community_name,
          }
        );
      }
      this.categoryNames = this.mulcommunityData;
      this.hidefirst = true;
      this.hidesecond = false;
    });
  }

  //LOGIN FORM
  loginuservendor() {
    if (this.mobilenumber == "" || this.mobilenumber == undefined) {
      this.toast.showwarning(this.message.mobilenum);
      return
    }
    if (this.password == "" || this.password == undefined) {
      this.toast.showwarning(this.message.Password);
      return
    }
    if (this.status == "" || this.status == undefined) {
      this.toast.showwarning(this.message.selectrole);
      return
    }
    let loginobj = {
      mobile_num: this.mobilenumber,
      password: this.password
    }

    this.service.post(this.service.userlogin, loginobj).subscribe((res) => {
      if ((res == "Invaild_UserName") || (res == "Invalid username or password.Enter correct credentials")) {
        this.toast.showwarning(this.message.Invalid_Login)
      }
      else {
        var data = res;
        if (res[0]['role'] == "user") {
          this.storage.set('profileing', res[0]['user_image']);
          this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
          this.storage.set('user_id', res[0]['user_id']);
          this.storage.set('user_flat_no', res[0]['flat_no']);
          this.storage.set('user_community_id', res[0]['community_id']);
          this.storage.set('userrole', res[0]['role']);
          this.storage.set('mobileNum', res[0]['phone_no']);
          this.storage.set('email_id', res[0]['email_id']);
          this.router.navigateByUrl('/userprod')
        }

        else if (res[0]['role'] == "vendor") {
          this.storage.set('profileing', res[0]['vendor_image']);
          this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
          this.storage.set('vendor_id', res[0]['vendor_id']);
          this.storage.set('userrole', res[0]['role']);
          this.storage.set('mobileNum', res[0]['phone_no']);
          this.storage.set('email_id', res[0]['email_id']);
          this.router.navigateByUrl('/dashboard')
        }

      }
    });

    // if (this.status == "user") {
    //   this.service.post(this.service.userlogin, loginobj).subscribe((res) => {
    //     if ((res == "Invaild_UserName") || (res == "Invalid username or password.Enter correct credentials")) {
    //       this.toast.showwarning(this.message.Invalid_Login)
    //     }
    //     else {
    //       var data = res;
    //       this.storage.set('profileing', res[0]['user_image']);
    //       this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
    //       this.storage.set('user_id', res[0]['user_id']);
    //       this.storage.set('user_flat_no', res[0]['flat_no']);
    //       this.storage.set('user_community_id', res[0]['community_id']);
    //       this.storage.set('userrole', res[0]['role']);
    //       this.storage.set('mobileNum', res[0]['phone_no']);
    //       this.router.navigateByUrl('/dashboard')
    //     }
    //   });
    // }
    // else if (this.status == "vendor") {
    //   this.service.post(this.service.vendorlogin, loginobj).subscribe((res) => {
    //     if ((res == "Invaild_UserName") || (res == "Invalid username or password.Enter correct credentials")) {
    //       this.toast.showwarning(this.message.Invalid_Login)
    //     }
    //     else {
    //       this.storage.set('profileing', res[0]['vendor_image']);
    //       this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
    //       this.storage.set('vendor_id', res[0]['vendor_id']);
    //       this.storage.set('userrole', res[0]['role']);
    //       this.storage.set('mobileNum', res[0]['phone_no']);
    //       this.router.navigateByUrl('/dashboard')
    //     }
    //   });
    // } 
    // else {
    //   this.service.post(this.service.portaladminlogin, loginobj).subscribe((res) => {
    //     if (res == "Invaild UserName") {
    //       this.toast.showwarning(this.message.Invalid_Login)
    //     }
    //     else {
    //       var data = res;
    //       this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
    //       this.storage.set('vendor_id', res[0]['vendor_id']);
    //       this.storage.set('userrole', res[0]['role']);
    //       this.storage.set('mobileNum', res[0]['phone_no']);
    //       this.router.navigateByUrl('/dashboard')
    //     }
    //   });
    // }

    // var tokenGenerator = FirebaseTokenGenerator.FirebaseTokenGenerator("rpuBcEs1SRH1uShlPibQmpKNkNaKar0wkHAbiEdL");
    // var token = tokenGenerator.createToken({uid: "h0TGndkbWYTH6zT4zZKX44MTiCh1", some: "arbitrary", data: "here"});
    // FirebaseTokenGenerator.FirebaseTokenGenerator("rpuBcEs1SRH1uShlPibQmpKNkNaKar0wkHAbiEdL")
    // FirebaseTokenGenerator.createToken({uid: "h0TGndkbWYTH6zT4zZKX44MTiCh1", some: "arbitrary", data: "here"})
    var cadmemail = 'chengaiahk@newmeksolutions.com';
    var cadmpswd = '123456';
    // firebase.auth().createUserWithEmailAndPassword(cadmemail, cadmpswd)
    // var v =this.fb.signupMail(cadmemail,cadmpswd).then(
    //        response => {
    //         console.log(response)  
    //        })
    console.log("v")

  }
  // signinUser(email: string, password: string) {
  //   firebase.auth().signInWithEmailAndPassword(email, password)
  //   .then(
  //      response => {
  //         firebase.auth().currentUser.getToken()
  //            .then(
  //               (token: string) => this.token = token
  //            )
  //      }
  //   )
  //   .catch(
  //      error => console.log(error)
  //   );
  // }
  loginEvent() {
    this.id = '';
    this.loginid = 2;
    this.firstname = '';
    this.phone_no = '';
    this.mobilenumber = '';
    this.password = '';
    this.community_id = '';
  }

  signupEvent() {
    this.id = 1;
    this.loginid = 1;
    this.hidefirst = false;
    this.hidesecond = true;
    this.firstname = '';
    this.phone_no = '';
    this.mobilenumber = '';
    this.password = '';
    this.community_id = '';
    this.state_id = '';
    this.city_id = '';
    this.pincode = '';
  }

  backtostates() {
    this.state_id = '';
    this.city_id = '';
    this.pincode = '';
    this.hidefirst = false;
    this.hidesecond = true;
    this.categoryNames = [];
    this.selectedItems = '';
    this.mulcommunityData = [];
    

  }

  //GET COMMUNITY ID
  getcommunityid(communityid) {
    this.community_id = communityid
  }

  radio(name) {
    if (name == "user") {
      this.community = false;
    } else {
      this.community = true;
    }
  }

  onItemSelect(cmidval) {
    this.community_id = cmidval.item_id;
    this.selectedItems = [
      { item_id: cmidval.item_id, item_text: cmidval.item_text }

    ];
  }

  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validation.numberOnly(event)
  }

  // CHECKING EMAIL VALIDATION
  chkemail(event): boolean {
    return this.validation.validateEmail(event)
  }
  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validation.nosplChar(event)
  }
  signupUserVendor() {

    if (this.community_id == "" || this.community_id == undefined) {
      this.toast.showwarning(this.message.usrCmtId);
      return
    }
    this.userObj.community_id = this.community_id;

    if (this.firstname == "" || this.firstname == undefined) {
      this.toast.showwarning(this.message.usrftName);
      return
    }
    this.userObj.first_name = this.firstname;

    if (this.phone_no == "" || this.phone_no == undefined) {
      this.toast.showwarning(this.message.vndNum);
      return
    }
    this.userObj.phone_no = this.phone_no;

    if (this.emailid == "" || this.emailid == undefined) {
      this.toast.showwarning(this.message.emailid);
      return
    }
    this.userObj.email_id = this.emailid;

    if (this.password == "" || this.password == undefined) {
      this.toast.showwarning(this.message.Password);
      return
    }
    this.userObj.password = this.password;

    if (this.status == "" || this.status == undefined) {
      this.toast.showwarning(this.message.selectrole);
      return
    }

    if (this.blockname == "" || this.blockname == undefined) {
      this.toast.showwarning(this.message.usrBlkName);
      return
    }
    if (this.flatnumber == "" || this.flatnumber == undefined) {
      this.toast.showwarning(this.message.usrFltNum);
      return
    }

    this.spinner.show();
    this.userObj.last_name = '';
    this.userObj.block_name = this.blockname;
    this.userObj.flat_no = this.flatnumber;
    this.userObj.created_by = "1";
    this.userObj.updated_by = "1";
    this.userObj.state_id = this.state_id;
    this.userObj.city_id = this.city_id;
    this.userObj.pincode = this.pincode;
    this.service.post(this.service.insertuser, this.userObj).subscribe((res) => {
      var data = res;
      if (res == "Inserted") {
        this.toast.showsuccess(this.message.usrCreate);
        this.firstname = '';
        this.phone_no = '';
        this.emailid = '';
        this.password = '';
        this.community_id = '';
        this.blockname = '';
        this.flatnumber = '';
        this.selectedItems = '';
        this.spinner.hide();
      }
      else {
        this.toast.showwarning(this.message.usrDuplicateExists);
        this.spinner.hide();
      }
    });


  }


}



