//NAME : BABURAO M
//DESCRIPTION : USER CREATIONS AND MODIFICATIONS
//DATE : 20/05/2019

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSelect, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { NgxSpinnerService } from 'ngx-spinner';




@Component({
  selector: 'app-login',
  templateUrl: './vendorprod.component.html',
  styleUrls: ['./vendorprod.component.css']
})
export class VendorProductsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  imgPath;
  vendorProductList;
  carticon: boolean = false;
  productArray = [];
  countProducts;
  hideProductList: boolean = false;
  // viewCartItems:boolean = true;
  productBack: boolean = true;
  viewProductList;
  increseCount;
  carticonhide = [];
  prodCount;
  shopingCartEmpty: boolean = true;
  prodIncrese;
  prodDecrese;
  viewProdDecrese;
  viewProdIncrese
  prodView: boolean = false;
  noRecords: boolean = true;
  productlist: any;
  vndprodpriceArray = new Array();
  vndProdList1: any;
  allproductlist:any;
 
  displayedColumns: string[] = ['product_name', 'category_name', 'brand_name', 'measurement', 'price'];
  constructor(private router: Router, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService, public dialog: MatDialog,
    private spinner: NgxSpinnerService, private toast: ToasterService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.imgPath = this.service.staticURL + `${'products_images/'}`;
    this.countProducts = 0;
    this.getProductList();

  }

  //GET VENDOR PRODUCTS BY VENDOR ID
  getProductList() {
    let getProdByVidObj = {
      vendor_id: this.storage.get('vendor_id')

    }
    this.service.post(this.service.getvndProducts, getProdByVidObj).subscribe((res) => {
      this.vendorProductList = res;
      this.vndProdList1 = res
      this.allproductlist = res;
      this.productlist = new MatTableDataSource(res);
      if (this.productlist.filteredData.length != 0) {
        this.noRecords = true
      } else {
        this.noRecords = false;
      }
      this.spinner.hide();
    });
  }
  //OPEN DIALOG 
  editproducts(val) {
    if (val == 'edit') {
      this.prodView = true;
      this.vndprodpriceArray = [];
    } else {
      this.getProductList();
      this.prodView = false;
    }

  }
  // TO CHANGE THE PRODUCT PRICE
  prodpricechange(val, inx) {
    let tempprodIdv = this.vndProdList1[inx].product_id;
    let tempArr = this.vndprodpriceArray.filter(x => x.product_id == tempprodIdv)
    if (tempArr.length > 0) {
      for (var i = 0; i < this.vndprodpriceArray.length; i++) {
        this.vndprodpriceArray[i].vendor_price =  this.vndProdList1[inx].vendor_price;
      }
      
    } else {
      this.vndprodpriceArray.push(this.vndProdList1[inx]);
    }

  }

  // TO SAVE THE VENDOR PRODUCT PRICE
  savevndprodprice() {
    this.spinner.show();
    let vndPriceObj={
      "prodPricesArray":this.vndprodpriceArray,
      "vendor_id":this.storage.get('vendor_id')
    }
    this.service.post(this.service.updatevndprodprice,vndPriceObj).subscribe(res=>{
      this.editproducts('back')
      this.getProductList();
      this.vndprodpriceArray = [];
    })
  }
   // SEARCH FUNCTIONALITY
   applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allproductlist.filter(function (categiry) {
        return (
          categiry.brand_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.category_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.product_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.price.indexOf(event) > -1 ||
          categiry.price.indexOf(event) > -1
        )
      });
      if (temp.length !== 0) {
        this.productlist = temp;
        this.productlist = new MatTableDataSource(temp);
        this.productlist.paginator = this.paginator;
        //this.records = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        //this.records = true;
        this.noRecords = false;
        this.productlist = new MatTableDataSource(temp);
        this.productlist.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.productlist = this.allproductlist;
      this.productlist = new MatTableDataSource(this.allproductlist);
      this.productlist.paginator = this.paginator;
      //this.records = false;
      this.noRecords = true;
    }
  }
}

