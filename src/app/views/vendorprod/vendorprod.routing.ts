import { Routes } from '@angular/router';

import { VendorProductsComponent } from './vendorprod.component';


export const VendorProductRoutes: Routes = [
  { path: '', component: VendorProductsComponent }
];