//NAME : BABURAO M
//DESCRIPTION : DISPLAY UNIT TYPES DATA
//DATE : 11/05/2019


import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
// import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ValidatorService } from '../services/validators.service';
@Component({
  selector: 'app-login',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {

  unitObj = {
    unit_type: '',
    // unit_id: '',
    created_by: '',
    updated_by: ''
  }
  unittable: boolean;
  noRecords: boolean;
  unit_type;
  unitList: any;
  allunitList: any;
  constructor(private router: Router, private service: ApiService,
    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog, private spinner: NgxSpinnerService,
    @Inject(LOCAL_STORAGE) private storage: StorageService

  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['unit_type'];

  ngOnInit() {
    this.spinner.show();
    this.getUnitList();
  }

  //GET UNIT TYPES DATA
  getUnitList() {
    this.service.post(this.service.getallunit, '').subscribe((res) => {
      if (res.length >= 1) {
        this.unitList = res;
        this.allunitList = res;
        this.unitList = new MatTableDataSource(res);
        this.unitList.paginator = this.paginator;
        this.unittable = false;
        this.noRecords = true;
        this.spinner.hide();
      } else {
        this.noRecords = false;
        this.unittable = true;
        this.spinner.hide();
      }
    });
  }

  adduinit() {
    this.openDialog1();
  }
  openDialog1(): void {
    const dialogRef = this.dialog.open(UnitsDialog, {
      width: '750px',
      disableClose: true,
      data: {
        unit_type: this.unitObj.unit_type,
        created_by: this.unitObj.created_by,
        updated_by: this.unitObj.updated_by,
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getUnitList();
      this.unitObj.unit_type = '';
    });
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allunitList.filter(function (categiry) {
        return (
          categiry.unit_type.toLowerCase().indexOf(event.toLowerCase()) > -1 
        )
      });
      if (temp.length !== 0) {
        this.unitList = new MatTableDataSource(temp);
        this.unitList.paginator = this.paginator;
        this.unittable = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.unittable = true;
        this.noRecords = false;
        this.unitList = new MatTableDataSource(temp);
        this.unitList.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.unitList = this.allunitList;
      this.unitList = new MatTableDataSource(this.allunitList);
      this.unitList.paginator = this.paginator;
      this.unittable = false;
      this.noRecords = true;
    }
  }

}

@Component({
  selector: 'units',
  templateUrl: 'units.html',
})
export class UnitsDialog {

  constructor(private service: ApiService,
    private toast: ToasterService, private message: MessageService,
    private validations: ValidatorService,
    public dialogRef: MatDialogRef<UnitsDialog>,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }
  userpage;
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  // ADDING UNITS
  unitsform(unit_type) {
    if (unit_type == null || unit_type == '') {
      this.toast.showwarning(this.message.addUnittypeName);
      return
    }
    let unitObj = {
      "unit_type": unit_type
    }
    this.service.post(this.service.insertunit, unitObj).subscribe(res => {
      if (res == 'Inserted') {
        this.toast.showsuccess(this.message.insertUnitType);
        this.onNoClick();
      }
      else {
        this.toast.showsuccess(this.message.dupUnitName)
      }
    })
  }

  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }
}

export interface DialogData {
  unit_type;
  unit_id;
}