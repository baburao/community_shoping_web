import { Component, OnInit, ViewChild, Inject, ViewChildren, QueryList } from '@angular/core';
import { ApiService } from '../services/api.service';
import { DatePipe } from '@angular/common';
import { ToasterService } from '../services/toaster.service';
import { MessageService } from '../services/message.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSort } from '@angular/material';
import { ValidatorService } from '../services/validators.service';

@Component({
  selector: 'app-portalorders',
  templateUrl: './portalorders.component.html',
  styleUrls: ['./portalorders.component.scss']
})
export class PortalordersComponent implements OnInit {
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  displayedColumns = ['S No', 'Customer Name', 'Orders', 'Amount', 'Actions']
  fm_date = new Date();
  to_date = new Date();
  vendor_id: number = null;
  cust_id: number = null;
  status_id: number = null;
  order_id: number = null;
  allvendors: any;
  allusers: any;
  orderData: any;
  usrorderData: any;
  status_state: string = "";
  records: boolean = true;
  norecords: boolean = false;
  records1: boolean = true;
  norecords1: boolean = false;
  status = ['Pending', 'Confirmed', 'Shipped', 'Delivered']
  usrstatus = ['Open', 'In-Progress', 'CLOSED']
  constructor(private service: ApiService, public datepipe: DatePipe, private message: MessageService, public dialog: MatDialog,
    private toast: ToasterService, private validations: ValidatorService,
    private spinner: NgxSpinnerService, ) { }

  ngOnInit() { 
    this.getvendors();
    this.getusers();
    this.fm_date.setDate(this.fm_date.getDate() - 15);
    this.portalOrders(this.fm_date, this.to_date, null, null, null);
  }

  // GETTING VENDORS 
  getvendors() {
    this.service.post(this.service.getallvendor, '').subscribe(res => {
      this.allvendors = res;
    })
  }
  // GETTING USERS
  getusers() {
    this.service.post(this.service.getalluser, '').subscribe(res => {
      this.allusers = res;
    })
  }
  // SEARCHING ORDERS
  portalOrders(fm_date, to_date, vendor_id, cust_id, status_id) {
    this.spinner.show();
    this.status_state = '';
    let prtObj = {
      "from_date": this.datepipe.transform(fm_date, 'yyyy-MM-dd'),
      "to_date": this.datepipe.transform(to_date, 'yyyy-MM-dd'),
      "vendor_id": vendor_id,
      "user_id": cust_id,
      "status": status_id,
    }
    this.status_state = status_id
    this.service.post(this.service.getprtlpndordbydt, prtObj).subscribe(res => {
      this.orderData = new MatTableDataSource(res);
      this.orderData.paginator = this.paginator.toArray()[0];
      this.orderData.sort = this.sort.toArray()[0];
      this.gettbldatastatus(res.length)
    })
  }

  // SEARCHING USER ORDERS
  portalUsrOrders(fm_date, to_date, cust_id, status_id, order_id) {
    this.spinner.show();
    if (order_id != null || order_id != undefined) {
      let usrObj = {
        "order_id": order_id
      }
      // this.spinner.show();
      this.service.post(this.service.getordcountbyordid, usrObj).subscribe(res => {
        this.usrorderData = new MatTableDataSource(res);
        this.usrorderData.paginator = this.paginator.toArray()[1];
        this.usrorderData.sort = this.sort.toArray()[1];
        this.getusrdatastatus(res)
      })
    } else {
      let usrObj = {
        "from_date": fm_date,
        "to_date": to_date,
        "user_id": cust_id,
        "status": status_id,
      }
      this.service.post(this.service.getusrordbydatebyusrid, usrObj).subscribe(res => {
        this.usrorderData = new MatTableDataSource(res);
        this.usrorderData.paginator = this.paginator.toArray()[1];
        this.usrorderData.sort = this.sort.toArray()[1];
        this.getusrdatastatus(res)
      })
    }
  }
  // DISPLAYING ORDER DETAILS
  showList(usrid) {
    let usrObj = {
      "vendor_id": this.vendor_id,
      "user_id": usrid,
      "status": this.status_state,
      "from_date": this.fm_date,
      "to_date": this.to_date
    }
    this.service.post(this.service.getprtlorddetbyusr, usrObj).subscribe(res => {
      const dialogRef = this.dialog.open(PortalorderModalComponent, {
        width: '750px',
        disableClose: true,
        autoFocus: true,
        data: {
          "status": 'vendor',
          "ordhisArray": res
        }
      })
      dialogRef.afterClosed().subscribe(res => {
      })
      this.spinner.hide();
    })
  }
  // DISPLAYING USER ORDER DETAILS
  showusrList(ordid) {
    // this.spinner.show();
    let usrObj = {
      "order_id": ordid,
      "order_status": this.status_id
    }
    this.service.post(this.service.getprtlusrdet, usrObj).subscribe(res => {
      const dialogRef = this.dialog.open(PortalorderModalComponent, {
        width: '1050px',
        disableClose: true,
        autoFocus: true,
        data: {
          "status": 'user',
          "ordhisArray": res
        }
      })
      dialogRef.afterClosed().subscribe(res => {

      })
      this.spinner.hide();
    })
  }
  // GETTING DATA STATUS
  gettbldatastatus(response) {
    if (response != 0) {
      this.records = false;
      this.norecords = true;
      this.spinner.hide();
    } else {
      this.records = true;
      this.norecords = false;
      this.spinner.hide();
    }
  }
  // GETTING USER DATA STATUS
  getusrdatastatus(response) {
    if (response != 0) {
      this.records1 = false;
      this.norecords1 = true;
      this.spinner.hide();
    } else {
      this.records1 = true;
      this.norecords1 = false;
      this.spinner.hide();
    }
  }

  // loading tabs
  loadtabs(event) {
    this.fm_date = new Date();
    this.fm_date.setDate(this.fm_date.getDate() - 15);
    this.to_date = new Date();
    this.vendor_id = null;
    this.cust_id = null;
    this.status_id = null;
    this.order_id = null;
    this.usrorderData = [];
    this.orderData = [];
    this.gettbldatastatus(0)
    this.getusrdatastatus(0)
    if(event.tab.textLabel == "User"){
      this.portalUsrOrders(this.fm_date,this.to_date, null, null, null)
    }else{
      this.portalOrders(this.fm_date, this.to_date, null, null, null);
    }
  }
  // CHECKING ORDER ID TXT LENGTH
  chktxt(event) {
    if (event == '' || event == null || event == undefined) {
      this.order_id = null;
    }
  }
  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }
}

@Component({
  selector: 'app-portalordermodal',
  templateUrl: './portalordersmodal.component.html',
  styleUrls: ['./portalorders.component.scss']
})
export class PortalorderModalComponent implements OnInit {
 @ViewChild(MatPaginator) paginator :MatPaginator
  portalorderdetails: any
  displayedColumns: string[] = [];
  constructor(public dialog: MatDialogRef<PortalorderModalComponent>, @Inject(MAT_DIALOG_DATA) public data: vndordhis) {
  }
  ngOnInit() {
    this.loadtblbyrole(this.data.status);


  }
  closeDialog() {
    this.dialog.close();
  }
  // LOADING TABLES BY ROLE
  loadtblbyrole(role) {
    if (role == 'vendor') {
      this.displayedColumns = ['S No', 'Order Id', 'Delivery Date', 'Product Name', 'Weight', 'Price', 'Quantity', 'TotalPrice']
      this.portalorderdetails = new MatTableDataSource(this.data.ordhisArray);
       this.portalorderdetails.paginator = this.paginator;
    } else {
      if (this.data.ordhisArray[0].order_detail_status == 'OPEN') {
        this.displayedColumns = ['S No', 'Order Detail Id', 'Product Name', 'Weight', 'Price', 'Quantity', 'TotalPrice', 'Delivery Date', 'Status']
      }
      else {
        // this.displayedColumns = ['S No', 'Customer Name', 'Order Detail Id', 'Product Name', 'Brand', 'Weight', 'Price', 'Quantity', 'TotalPrice', 'Delivery Date', 'Delivery Time', 'Status','VendorName','VndStatus']
        this.displayedColumns = ['S No', 'Order Detail Id', 'Product Name', 'Weight', 'Price', 'Quantity', 'TotalPrice', 'Delivery Date', 'Status', 'VendorName', 'VndStatus']
      }
      this.portalorderdetails = new MatTableDataSource(this.data.ordhisArray)
      this.portalorderdetails.paginator = this.paginator;
    }

  }
}
export interface vndordhis {
  ordhisArray;
  status;
}
