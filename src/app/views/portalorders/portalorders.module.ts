import { Portalordroutes } from './portalorders.routing';
import { ApiService } from '../services/api.service';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { PortalordersComponent, PortalorderModalComponent } from './portalorders.component';
import { CommonModule, DatePipe } from '@angular/common';
import {FormsModule} from '@angular/forms'

import {
    MatIconModule,
    MatDialogModule,
    MatButtonModule, 
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
} from '@angular/material';
import { ToasterService } from '../services/toaster.service';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatOptionModule,
        MatCardModule,
        MatListModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        CommonModule,
        MatInputModule,
        FormsModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatTabsModule,
        RouterModule.forChild(Portalordroutes),
    ],
    providers: [ApiService,ToasterService,DatePipe],
    declarations: [PortalordersComponent,PortalorderModalComponent
    ],
    entryComponents:[PortalorderModalComponent]
}) 

export class PortalorderModule { }