import { Routes } from '@angular/router';

import { PBrandComponent } from './pbrand.component';


export const PBrandRoutes: Routes = [
  { path: '', component: PBrandComponent }
];