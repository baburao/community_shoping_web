import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup, MatExpansionPanel } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSelect, MatDialog } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../../services/message.service';
import { ToasterService } from '../../services/toaster.service';
import { Inject, Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './pbrand.component.html',
  styleUrls: ['./pbrand.component.css']
})
export class PBrandComponent implements OnInit {

  mobilenumber;
  password;
  loginobj = {
    mobile_number: "",
    password: ""
  }
  status;

  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService,
    private toast: ToasterService,private spinner: NgxSpinnerService,

  ) { }



  ngOnInit() {

    

  }

  //LOGIN FORM
  loginuservendor() {
    
    if (this.mobilenumber == "" || this.mobilenumber == undefined) {
      this.toast.showwarning(this.message.mobilenum);
      return
    }
    if (this.password == "" || this.password == undefined) {
      this.toast.showwarning(this.message.Password);
      return
    }

    let loginobj = {
      mobile_num: this.mobilenumber,
      password: this.password
    } 


      this.service.post(this.service.portaladminlogin, loginobj).subscribe((res) => {
        this.spinner.show();
        if(res == "Invaild_credentials"){
          
          this.toast.showwarning(this.message.Invalid_Login);
          this.spinner.hide();
        
        }
        else{
          var data = res;
          this.storage.set('profileing',res[0]['portal_user_image']);
          this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
          this.storage.set('portal_admin_id', res[0]['admin_id']);
  
          this.storage.set('userrole', res[0]['role']);
          this.storage.set('emailid', res[0]['email_id']);
          
          // this.loader.open();
          this.router.navigateByUrl('/dashboard') 
          // this.loader.close();
          this.spinner.hide();
        }
       
      });
     
    
  }
}
