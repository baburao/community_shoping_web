import { Routes } from '@angular/router';

import { PortalLoginComponent } from './portallogin.component';


export const PortalLoginRoutes: Routes = [
  { path: '', component: PortalLoginComponent }
];