import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//import { ToasterService } from '../services/toaster.service';
import { 
  MatInputModule,
  MatDatepickerModule, 
  MatNativeDateModule,
  MatListModule,
  MatCardModule,
  MatProgressBarModule,
  MatRadioModule,
  MatCheckboxModule,
  MatButtonModule,
  MatIconModule,
  MatStepperModule,
  } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PortalLoginComponent } from './portallogin.component';
import { PortalLoginRoutes } from './portallogin.routing';
// import { LocalStorageService } from 'ngx-webstorage';
// import {BrowserModule} from '@angular/platform-browser';
// import {NgxWebstorageModule} from 'ngx-webstorage';
// import { CommonDirectivesModule } from './sdirectives/common/common-directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    MatIconModule,
    FlexLayoutModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatRadioModule,
    MatStepperModule,
    // LocalStorageService,
    // BrowserModule,
    // NgxWebstorageModule.forRoot(),
    RouterModule.forChild(PortalLoginRoutes)
  ],
  declarations: [PortalLoginComponent],
  providers: [
    
    // MessageService
   
  ],
})
export class PortalLoginModule { }