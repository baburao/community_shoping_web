import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
// import { Router } from '@angular/router';
// import { Inject, Injectable } from '@angular/core';
// import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
// import { ToasterService } from 'app/views/services/toaster.service';
// import { MessageService } from 'app/views/services/message.service';
// import { ApiService } from 'app/views/services/api.service';
// import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../../services/message.service';
import { ToasterService } from '../../services/toaster.service';
@Component({
  selector: 'app-login',
  templateUrl: './portallogin.component.html',
  styleUrls: ['./portallogin.component.css']
})
export class PortalLoginComponent implements OnInit {

  emailid;
  password;
  loginobj = {
    emailid: "",
    password: ""
  }
  status;   // private message: MessageService

  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private message: MessageService,
    private toast: ToasterService
  ) { }

  ngOnInit() {


    this.storage.clear();
    let data = "123";
    this.storage.set('loginname', data);
    this.status = "user";
  }

  //LOGIN FORM
  loginuservendor() {
    if (this.emailid == "" || this.emailid == undefined) {
      this.toast.showwarning(this.message.emailid);
      return
    }
    if (this.password == "" || this.password == undefined) {
      this.toast.showwarning(this.message.Password);
      return
    }
    if (this.status == "" || this.status == undefined) {
      this.toast.showwarning(this.message.selectrole);
      return
    }
    let loginobj = {
      email_id: this.emailid,
      password: this.password
    }

    if (this.status == "user") {
      this.service.post(this.service.userlogin, loginobj).subscribe((res) => {
        var data = res;
        this.storage.set('loginname', res[0]['first_name'] + ' ' + res[0]['last_name']);
        this.storage.set('user_id', res[0]['user_id']);
        this.storage.set('user_flat_no', res[0]['flat_no']);
        this.storage.set('user_community_id', res[0]['community_id']);
        this.storage.set('userrole', res[0]['role']);
        this.storage.set('emailid', res[0]['email_id']);
        // this.loader.open();
        this.router.navigateByUrl('/dashboard')
        // this.loader.close();
      });
    } 
  }
  // RESETTING PASSWORD

  // CREATING NEW PASSWORD FORM
  
}
