export const portaladminmenus = [
  {
    name: 'Dashboard',
    type: 'link',
    tooltip: 'Dashboard',
    icon: 'dashboard',
    state: 'dashboard',
    // badges: [{ color: 'accent', value: '100+' }],
  },
  // {
  //   name: 'States',
  //   type: 'link',
  //   tooltip: 'state',
  //   icon: 'language',
  //   state: 'states',
  //   // badges: [{ color: 'warn', value: '1' }]
  // },
  {
    name: 'Users',
    type: 'link',
    tooltip: 'user',
    icon: 'person',
    state: 'user',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Vendors',
    type: 'link',
    tooltip: 'vendor',
    icon: 'supervisor_account',
    state: 'vendor',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Product Category',
    type: 'link',
    tooltip: 'productcategory',
    icon: 'blur_linear',
    state: 'productcategory',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Product Catalog',
    type: 'link',
    tooltip: 'productcatelog',
    icon: 'gradient',
    state: 'productcatelog',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Brands',
    type: 'link',
    tooltip: 'brands',
    icon: 'format_bold',
    state: 'brands',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Units',
    type: 'link',
    tooltip: 'units',
    icon: 'view_comfy',
    state: 'units',
    // badges: [{ color: 'warn', value: '1' }]
  },
  {
    name: 'Communities',
    type: 'link',
    tooltip: 'communities',
    icon: 'account_balance',
    state: 'community',
  },
  {
    name: 'Grouping Orders',
    type: 'link',
    tooltip: 'Grouping Orders',
    icon: 'toc',
    state: 'groupingorders',
  },
  {
    name: 'Payments',
    type: 'link',
    tooltip: 'Payments',
    icon: 'payment',
    state: 'paymeny',
  },
  {
    name: 'Vendor Payments',
    type: 'link',
    tooltip: 'adminpaymeny',
    icon: 'payment',
    state: 'adminpaymeny',
  },
  {
    name: 'Orders',
    type: 'link',
    tooltip: 'portalorders',
    icon: 'border_color',
    state: 'portalorders',
  }



];
export const usermenus = [
  {
    name: 'Dashboard',
    type: 'link',
    tooltip: 'Dashboard',
    icon: 'dashboard',
    state: 'dashboard',
    // badges: [{ color: 'accent', value: '100+' }],
  },
   {
    name: 'Product Catalog',
    type: 'link',
    tooltip: 'Products',
    icon: 'dashboard',
    state: 'userprod',
    // sub: [
    //   { name: 'Regular',type:'link',state:'Regular' }, 
    //   { name: 'Organic',type:'link',state:'Organic' },
    //   { name: 'Millets',type:'link',state:'Millets' },
    // ]

  },
  {
    name: 'My Orders',
    type: 'link',
    tooltip: 'userordertracking',
    icon: 'toc',
    state: 'userordertracking',
  },
  // {
  //   name: 'Report an Issue',
  //   type: 'link',
  //   tooltip: 'userissue',
  //   icon: 'border_color',
  //   state: 'userissue',
  // },

];
export const vendormenus = [
  {
    name: 'Dashboard',
    type: 'link',
    tooltip: 'Dashboard',
    icon: 'dashboard',
    state: 'dashboard',
    // badges: [{ color: 'accent', value: '100+' }],
  },
  {
    name: 'Products',
    type: 'link',
    tooltip: 'vendor',
    icon: 'gradient',
    state: 'vendorprod',
  },
  {
    name: 'Your Orders',
    type: 'link',
    tooltip: 'Vendor Orders',
    icon: 'toc',
    state: 'vendororders'
  },
  {
    name: 'Orders History',
    type: 'link',
    tooltip: 'Orders History',
    icon: 'blur_linear',
    state: 'vndorderhistory'
  }
  // {
  //   name: 'Reports',
  //   type: 'link',
  //   tooltip: 'Vendor Reports',
  //   icon: 'border_color',
  //   state: 'vndorderreports'
  // },
];