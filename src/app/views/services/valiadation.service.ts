﻿import { Injectable, OnInit } from '@angular/core';
import {  AbstractControl } from '@angular/forms';

//import { Control, ControlGroup, FormBuilder, Validators, NgForm, NgClass } from '@angular/common';
@Injectable()

export class ValiadationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {

            'required': 'This Field is Required.',
            'invalidEmailAddress': 'Please Enter Valid EmailId',
            'UserName': 'Please Enter UserName',
            'UserOrganizationIdentifier': 'Please Enter FirstName',
            'LastName': 'Please Enter LastName',
            'Email': 'Please Enter EmailId',
            'Password': 'Please Enter Password',
            'MobileNo': 'Please Enter MobileNo',
            'invalidmobileno': 'Please Enter Valid Phone Number.',
            'minlength': `Minimum length ${validatorValue.requiredLength}`,

            //Request for Prayer Start
            'Name': 'Please Enter Name',
            'Address': 'Please Enter Address',
            'City': 'Please Enter City Name',
            'ZipCode': 'Please Enter Zip Code',
            'PrayerCategory': 'Please Select Prayer Category',
            'Country': 'Please Select Country',
            'Description': 'Please Enter Description',
            'invalidZipCode': 'Please Enter Valid  ZipCode(Only Numbers)',
            'OldPassword': 'Please Enter OldPassword',
            'NewPassword': 'Please Enter NewPassword',
            'ConformPassword': 'Please Enter ConformPassword',
            'Channel': 'Please Enter Channel Name',
            'ShoutcastUrl': 'Please Enter Shoutcast Url',
            'WebsiteUrl':'Please Enter Website Url',
            'Timezone': 'Please Select Time Zone',
            'Channeltype': 'Please Select Channel Type',
            'AccountName': 'Please Enter Account Name',
            'FTPURL': 'Please Enter FTP URL',
            'FTPUserName': 'Please Enter FTP User Name',
            'FTPPassword': 'Please FTP Password'
            //Request for Prayer End
        };

        return config[validatorName];
    }

    static creditCardValidator(control) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }

    static phonenoValiadator(control) {

        if (control.value.match(/^[0-9]*$/)) {
            return null;
        } else {
            return { 'invalidmobileno': true };
        }
    }

    static zipcodevalidator(control) {

        if (control.value.match(/^[0-9]*$/)) {
            return null;
        } else {
            return { 'invalidZipCode': true };
        }
    }

    static specialCharValidator(control): { [key: string]: any } {
        if (control.value) {
            if (!control.value.match(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/)) {
                return null;
            }
            else {
                return { 'invalidChar': true };
            }
        }

    }
   

    static UserOrganizationIdentifier(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'FirstName': true };
        }
    }
    static OldPassword(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'OldPassword': true };
        }
    }
    static NewPassword(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'NewPassword': true };
        }
    }


    //static MatchPassword(AC: AbstractControl) {
    //    let password = AC.get('password').value; // to get value in input tag
    //    let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    //    if (password != confirmPassword) {
    //        console.log('false');
    //        AC.get('confirmPassword').setErrors({ MatchPassword: true })
    //    } else {
    //        console.log('true');
    //        return null
    //    }
    //}



   

   






    static Channel(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Channel': true };
        }
    }
    
    static Channeltype(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Channeltype': true };
        }
    }
    static Timezone(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Timezone': true };
        }
    }
    
    static ConformPassword(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'ConformPassword': true };
        }
    }
    static LastName(control) {

        if (control.value) {
            return null;
        }
        else {
            return { 'LastName': true };
        }
    }
    static UserName(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'UserName': true };
        }
    }
    static Email(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Email': true };
        }
    }
    static Password(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Password': true };
        }
    }
    static Mobile(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'MobileNo': true };
        }
    }

    static Name(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Name': true };
        }
    }
    static Address(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Address': true };
        }
    }
    static City(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'City': true };
        }
    }
    static ZipCode(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'ZipCode': true };
        }
    }
    static PrayerCategory(control) {

        if (control.value !== '') {
            return null;
        }
        else {
            return { 'PrayerCategory': true };
        }
    }
    static Country(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Country': true };
        }
    }
    static Description(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'Description': true };
        }
    }
    static ShoutcastUrl(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'ShoutcastUrl': true };
        }
    }
    static WebsiteUrl(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'WebsiteUrl': true };
        }
    }

    static AccountName(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'AccountName': true };
        }
    }

    static FTPURL(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'FTPURL': true };
        }
    }

    static FTPUserName(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'FTPUserName': true };
        }
    }

    static FTPPassword(control) {
        if (control.value) {
            return null;
        }
        else {
            return { 'FTPPassword': true };
        }
    }
}