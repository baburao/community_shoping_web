import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ToasterService } from './toaster.service';
import { MessageService } from './message.service';
// import { LocalStorageService } from 'ngx-webstorage';






@Injectable({
  providedIn: 'root'
})
export class ApiService {


  public apiUrl = environment.apiEndpoint;
  public staticURL = environment.staticURL;
  //user login
  userlogin = `${this.apiUrl}userlogin/`;
  //vendor login
  vendorlogin = `${this.apiUrl}vendorlogin/`;
  getvendorbyid = `${this.apiUrl}getvendorbyid/`;
  //states
  getallstate = `${this.apiUrl}getallstate/`;
  

  //user
  getalluser = `${this.apiUrl}getalluser/`;
  getallcommunity = `${this.apiUrl}getallcommunity/`;
  insertuser = `${this.apiUrl}insertuser/`;
  getuserbyid = `${this.apiUrl}getuserbyid/`;
  updateuserstatus = `${this.apiUrl}updateuserstatus/`;
  updateuserdetails = `${this.apiUrl}updateuserdetails/`;
  updateusrprofile = `${this.apiUrl}updateusrprofile/`;
  //vendor
  getallvendor = `${this.apiUrl}getallvendor/`;
  insertvendor = `${this.apiUrl}insertvendor/`;
  getcitiesbystate = `${this.apiUrl}getcitiesbystate/`;
  updatevendorstatus = `${this.apiUrl}updatevendorstatus/`;
  updatevendordetails = `${this.apiUrl}updatevendordetails/`;
  changevendorpassword = `${this.apiUrl}changevendorpassword/`;
  updatevndprodprice =  `${this.apiUrl}updatevndprodprice/`;

  //product categorys
  getallcategory = `${this.apiUrl}getallcategory/`;
  insertcategory = `${this.apiUrl}insertcategory/`;
  updatecategorystatus = `${this.apiUrl}updatecategorystatus/`;
  getcategorybyid = `${this.apiUrl}getcategorybyid/`;
  updatecategoryname = `${this.apiUrl}updatecategoryname/`;
  getproductsbycategory = `${this.apiUrl}getproductbycategory/`;
  getActiveCategory = `${this.apiUrl}getActiveCategory/`;
  getallprodmenus = `${this.apiUrl}getallprodmenus/`;
  getallactivectgs = `${this.apiUrl}getallactivectgs/`;
  getusrprodctg = `${this.apiUrl}getusrprodctg/`;
  
  //product catelogs
  getallproducts = `${this.apiUrl}getallproducts/`;
  getallbrand = `${this.apiUrl}getallbrand/`;
  getallunit = `${this.apiUrl}getallunit/`;
  insertproduct = `${this.apiUrl}insertproduct/`;
  updateproductstatus = `${this.apiUrl}updateproductstatus/`;
  getproductbyid = `${this.apiUrl}getproductbyid/`;
  updateproductdetails = `${this.apiUrl}updateproductdetails/`;
  getprodcatlogmenu = `${this.apiUrl}getprodcatlogmenuwithctg/`;
  //brands
  insertbrand = `${this.apiUrl}insertbrand/`;
  getbrandbyid = `${this.apiUrl}getbrandbyid/`;
  updatebrandname = `${this.apiUrl}updatebrandname/`;

  updatebrandstatus = `${this.apiUrl}updatebrandstatus/`;

  //unit type
  insertunit = `${this.apiUrl}insertunit/`;
  updateunit = `${this.apiUrl}updateunit/`;
  //cities
  getallcities = `${this.apiUrl}getallcities/`;


  // COMMUNITIES
  insertcommunity = `${this.apiUrl}insrtcommunity/`;
  allcommunities = `${this.apiUrl}getallcommunity/`;
  updatecommunities = `${this.apiUrl}updatecommunity/`;
  updatecommunitybyid = `${this.apiUrl}updatecommunitybyid/`;
  updatecommunitystatus = `${this.apiUrl}updatecommunitystatus/`;
  getactivecommunity = `${this.apiUrl}getactivecommunity/`;

  getcommunitybyid = `${this.apiUrl}getcommunitybyid/`;

  // UPLOAD IMAGE
  cmtinsertimg = `${this.apiUrl}cmtinsertimg/`;
  insertimg = `${this.apiUrl}insertimg/`;
  updateimg = `${this.apiUrl}getimage/`;
  profinsertimg = `${this.apiUrl}profinsertimg/`;

  // VENDOR PRODUCT MAPPING
  getvndProducts = `${this.apiUrl}getvendorprodmap/`;
  insertvendorprodmap = `${this.apiUrl}insertvendorprodmap/`;
  delvndProduct = `${this.apiUrl}deletevendorprod/`;

  //CREATE ORDERS
  insertuserorder = `${this.apiUrl}insertuserorder/`;
  //GET USER ORDERS
  getalluserorder = `${this.apiUrl}getalluserorder/`;
  
  getordcountbyordid = `${this.apiUrl}getordcountbyordid/`;
  //GET ORDERS BY USER ID
  getuserorderbyid = `${this.apiUrl}getuserorderbyid/`;
  //GET ORDER DETAILS BY ORDER ID
  getuserorderdetailbyid = `${this.apiUrl}getuserorderdetailbyid/`;

  // ISSUE TRACKING
  insertusrissue = `${this.apiUrl}insertusrissue/`;

  // ORDER GROUPING
  getoverallorders = `${this.apiUrl}getoverallorders/`;
  //PORTAL ADMIN LOGIN
  portaladminlogin = `${this.apiUrl}portaladminlogin/`
  getordersbyProdid = `${this.apiUrl}getordersbyProdid/`;
  getvndsbyProdid = `${this.apiUrl}getvndsbyProdid/`;
  upprtladminprofile = `${this.apiUrl}upprtladminprofile/`;
  getvndPrice = `${this.apiUrl}getvndPrice/`;

  // ASSIGNING PRODUCTS TO VENDOR
  orderprodvndassign = `${this.apiUrl}orderprodvndassign/`;

  dispatchorders = `${this.apiUrl}dispatchorders/`;
  vnddispatchorders = `${this.apiUrl}vnddispatchorders/`;
  vnddispatchorderdetails = `${this.apiUrl}vnddispatchorderdetails/`;
  cancordrs = `${this.apiUrl}cancordrs/`;
  acceptcordrs = `${this.apiUrl}acceptcordrs/`;

  //USER ORDER TRACKING DATA BY USER ID
  getordertrackingdatabyuserid = `${this.apiUrl}getordertrackingdatabyuserid/`;
  //PRODUCT STATUS IN ORDER TRACKING BY ORDER ID
  getproductsordertrackingbyorderid = `${this.apiUrl}getproductsordertrackingbyorderid/`;
  // USER ORDER CANCELLATION BY ORDER ID
  getuserordercancellation = `${this.apiUrl}getuserordercancellation/`;
  //PORTAL ADMIN PROFILE DATA
  getportaladminbyid = `${this.apiUrl}getportaladminbyid/`;

  adminproducts = `${this.apiUrl}adminproducts/`;

  // vendororders = `${this.apiUrl}vendororders/`;

  changeuserpassword = `${this.apiUrl}changeuserpassword/`;

  changeportaladminpassword = `${this.apiUrl}changeportaladminpassword/`;

  //DASH BROAD
  getuserorderscount = `${this.apiUrl}getuserorderscount/`;

  getuserorderscountforopenstatus = `${this.apiUrl}getuserorderscountforopenstatus/`;

  getuserorderscountforinprogressstatus = `${this.apiUrl}getuserorderscountforinprogressstatus/`;

  getuserorderscountforclosestatus = `${this.apiUrl}getuserorderscountforclosestatus/`;

  getusertotalproductscount = `${this.apiUrl}getusertotalproductscount/`;

  getvendorsorderscount = `${this.apiUrl}getvendorsorderscount/`;

  getvendorsordersproductcount = `${this.apiUrl}getvendorsordersproductcount/`;

  getvendorassignproductcount = `${this.apiUrl}getvendorassignproductcount/`;

  getvendorshippedproductcount = `${this.apiUrl}getvendorshippedproductcount/`;

  //GET ALL PAYMENTS
  getallpayments = `${this.apiUrl}getallpayments/`;
  // GET PAYMENT DETAILS BY ID
  getpaymentdetailsbyid = `${this.apiUrl}getpaymentdetailsbyid/`;

  insertpayments = `${this.apiUrl}insertpayments/`;

  insertadminpayments = `${this.apiUrl}insertadminpayments/`;

  getadminpayments = `${this.apiUrl}getadminpayments/`;

  getbalanceamt = `${this.apiUrl}getbalanceamt/`;

  //DASH BROAD END


  // VENDOR ORDERS
  acceptedvendororders = `${this.apiUrl}acceptedvendororders/`;
  pendingvendororders = `${this.apiUrl}pendingvendororders/`;
  vendorpndordersbyuserId = `${this.apiUrl}vendorpndordersbyuserId/`;
  vendoracptordersbyuserId = `${this.apiUrl}vendoracptordersbyuserId/`;
  shippedvendororders = `${this.apiUrl}shippedvendororders/`;
  vendorshpordersbyuserId = `${this.apiUrl}vendorshpordersbyuserId/`;
  vendordelvordersbyuserId = `${this.apiUrl}vendordelvordersbyuserId/`;

  vendorpendingcount = `${this.apiUrl}vendorpendingcount/`

  // PORTAL ORDERS
  getprtlpndordbydt = `${this.apiUrl}getprtlpndordbydt/`;
  getprtlorddetbyusr = `${this.apiUrl}getprtlorddetbyusr/`;
  getusrordbydatebyusrid = `${this.apiUrl}getprtlusrord/`;
  getprtlusrdet = `${this.apiUrl}getprtlusrdet/`;
  

  constructor(private http: HttpClient,private toast :ToasterService,private message:MessageService) {
  }


  getWithoutLoader(url): Observable<any> {
    return this.http.get(url);
  }

  post(url, data): Observable<any> {
    // let token = this.storage.retrieve('token');
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': token
    //   })
    // };
    // return this.http.post(url, data, httpOptions);
    return this.http.post(url, data)
    // .map(res=>{
    //   return res

    // })
    // .catch((error: any)=>
    // {
    //   if (error.status === 500) {
    //     this.toast.showerror(this.message.errMessage500);
    //   }
    //   else if (error.status === 404) {
    //     this.toast.showerror(this.message.errMessage404);
    //   }
    //   else if (error.status === 400) {
    //     this.toast.showerror(this.message.errMessage400);
    //   }
    //   else if (error.status === 408) {
    //     this.toast.showerror(this.message.errMessage408);
    //   }
    //   else if (error.status === 405) {
    //     this.toast.showerror(this.message.errMessage405);
    //   }
    //   else if (error.status === 401) {
    //     this.toast.showerror(this.message.errMessage401);
    //   }
    //   else if (error.status === 503 || error.status === 0) {
    //      this.toast.showerror(this.message.errMessage503);
    //   }
    //   else {
    //     return Observable.throw(new Error(error.status));
    //   }

    // });

  }

  loginpost(url, data): Observable<any> {
    return this.http.post(url, data)
  }

  get(url): Observable<any> {
    // this.loader.show();
    // let token = this.storage.retrieve('token');
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': token
    //   })
    // };
    return this.http
      // .get(url, httpOptions)
      .get(url)
      .map(res => {
        // this.loader.hide();
        return res;
      })
    //.catch(res => Observable.throw(this.handleErr(res))
    //)
  }
  handleErr(err: any) {
    // this.loader.hide();
    return err;
  }
  //preview data
  get_previewdata(previewUrl): Observable<any> {
    return this.http
      .get(previewUrl)
      .map(res => {
        // this.loader.hide();
        return res;
      })
      .catch(res => Observable.throw(this.handleErr(res))
      )

  }

  // cliek(){
  //   return this.http.post(url, data);
  // }



}
