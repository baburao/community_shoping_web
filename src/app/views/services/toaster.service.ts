import { Injectable } from '@angular/core';
 import { ToastrService, GlobalConfig } from 'ngx-toastr';

@Injectable()
export class ToasterService {

  options: GlobalConfig;
  title = '';
  message = '';
  private lastInserted: number[] = [];

  constructor(public toastrService: ToastrService) {
    this.options = this.toastrService.toastrConfig;
  }

  showsuccess(message) {
    this.toastrService.success(message);
  }

  showerror(message) {
    this.toastrService.error(message);
  }

  showinfo(message) {
    this.toastrService.info(message);
  }

  showwarning(message) {
    this.toastrService.warning(message);
  }


  clearToasts() {
    this.toastrService.clear();
  }
  clearLastToast() {
    this.toastrService.clear(this.lastInserted.pop());
  }

}
