
import { Injectable } from '@angular/core';
import { ToasterService } from './toaster.service';

@Injectable()
export class ErrorService{
    constructor( private toast: ToasterService) { }
    display_error = 1;
    hide_error = 2;
    display_custom_error = 2;
    LogError(error,error_code,custom_error_message) {
        console.log("Actual Error");
        console.log(error);
        if(error_code == this.display_error){
            console.log("");
            console.log("Error Message");
            console.log(error.error.text);
            this.toast.showerror(error.error.text);
        } else if(error_code == this.display_custom_error){
            console.log("");
            console.log("Custom Message");
            console.log(custom_error_message);
            this.toast.showerror(custom_error_message);
        }


    } 
}