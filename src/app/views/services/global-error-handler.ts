import {ErrorHandler, Injectable, NgModule } from '@angular/core';
import {ErrorLogService } from './error-log.service';
import { throwError } from 'rxjs';

@Injectable()
export class GlobalErrorHandler extends ErrorHandler {
    constructor( 
        private errorLogService: ErrorLogService
        ) {
        super();
    }

    handleError(error) {
        this.errorLogService.logError(error);
        // console.log("global error");
        // console.log(error);
        
        // IMPORTANT: Rethrow the error otherwise it gets swallowed
        throw error;
    }
}