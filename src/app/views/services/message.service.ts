
import { Injectable, RendererFactory2, Renderer2 } from '@angular/core';
import { ToasterService } from './toaster.service';

@Injectable()
export class MessageService {

    renderer2: Renderer2;

    constructor(
        private toast: ToasterService,

        rendererFactory: RendererFactory2
    ) {

        this.renderer2 = rendererFactory.createRenderer(null, null);
    }

    validatepriceformat = /^\d+\.\d{0,2}$/;
    numbersmatch = /^-?[0-9]*[1-9][0-9]*$/;
    number = /^[0-9]{1,6}$/
    emailformat = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;



    //login form
    emailid = "Please Enter Email Id";
    Password = "Please Enter Password";
    selectrole = "Please Select role";
    mobilenum = "Please enter Mobile number"
    
    //  COMMUNITY 
    regNumber = "Enter Registration No";
    cmmtName = "Enter Community Name";
    cmmtAddress = "Enter Address";
    cmmtCity = "Select City";
    cmmtState = "Select State";
    cmmtPincode = "Enter PinCode";
    cntPersonName = "Enter Contact Name";
    cntPersonNumber = "Enter Contact Phone No";
    cntPersonDesgn = "Enter Designation";
    cntPersonEmail = "Enter Contact Email Id";
    CmtActive = "Community Activated";
    CmtInActive = "Community InActive";
    CmtRegistered = "Community Registered";
    UpCommunity = "Community Updated";
    CmtDuplicate = "Duplicate Exists";


    //product category
    insertProductCategory = "Product Category Inserted Successfully";
    deactiveProductCategory = "Product Category Deactivated Successfully";
    activeProductCategory = "Product Category activated Successfully";
    updateProductCategoryName = "Product Category Updated";
    productCategoryName = "Enter Category Name";
    duplicateproductCategoryName = "Product Category Name Already Exist";

    //product catalog
    insertProduct = "Product Inserted Successfully";
    deactiveProduct = "Product Deactivated Successfully";
    activeProduct = "Product activated Successfully";
    updateProduct = "Product Updated Successfully";
    duplicateProduct = "Product Name/Code Already Exist";
    productName = "Please Enter Product Name";
    productCode = "Please Enter Product Code";
    productHSNCode = "Please Enter Product HSN Code";
    categoryName = "Please Select Category Name";
    brandName = "Please Select Brand Name";
    unitType = "Please Enter Unit Type";
    measurement = "Please Enter Measurement";
    productprice = "Please Enter Product Price";
    productdesc = "Please Enter Product Description"
    imageUpload = "Please Upload Image";
    prodType = "Please Select Product Tpye";

    //unit type
    insertUnitType = "Unit Type  Inserted Successfully";
    addUnittypeName = "Enter Unit Name"
    dupUnitName = "Unit Name already Exist"

    //brands
    enterbrand = "Enter Brand Name";
    insertbrands = "Brand  Inserted Successfully";
    deactiveBrand = "Brand Deactivated Successfully";
    activeBrand = "Brand activated Successfully";
    updateBrand = "Brand Updated Successfully";
    duplicateBrand = "Brand Name Already Exist";

    // VENDORS
    upVendor = "Vendor Updated";
    crtVendor = "Vendor Created";
    vndInactive = "Vendor In-Activated";
    vndActive = "Vendor Activated";
    vndftName = "Enter First Name";
    vndltName = "Enter Last Name";
    vndNum = "Enter Phone Number";
    vndEmail = "Enter Email Id";
    vndPswd = "Enter Passowrd";
    vndRePswd = "Enter Confirm Password";
    account_no = "Enter Account No";
    ifsccode = "Enter IFSC Code";
    branch = "Enter Branch";
    bankname = "Enter Bank Name";
    holdername = "Enter Holder Name";
    accounttype = "Enter Account Type";
    stateid = "Select State";
    cityid = "Select City";
    // VENDOR PRODUCT MAPPING
    prodSelected = "Product Already Selected";
    dupProdExits = "Highlighted Products are already Assigned";
    vndPrice = "Enter Vendor Price";

    // USERS
    usrftName = "Enter First Name";
    usrltName = "Enter Last Name";
    usrNum = "Enter Phone No";
    usrCmtId = "Select Community Name";
    usrBlkName = "Enter Block Name";
    usrFltNum = "Enter Flat No";
    usrEmail = "Enter Email Id";
    usrPswd = "Enter Password";
    usrRePswd = "Enter Confirm Password";
    usrUpdate = "User Updated";
    usrCreate = "User Created";
    usrinActive = "User In-Active";
    usrActive = "User Active";
    usrprofUpdated = "Profile Updated";
    usrDuplicate = "Duplicate Exists";
    usrDuplicateExists = "User Duplicate Exists";

    // cmtblockname = "Enter Block Name";
    // cmtflatname = "Enter Flat No";

    //PAYMENTS
    chequeno = "Please Enter Cheque No";
    acno = "Please Enter Account No";
    bankAcName = "Please Enter Bank Account Name";
    bankName = "Please Enter Bank Name";
    BranchName = "Please Enter Branch Name";
    ifscCode = "Please Enter IFSC Code";
    depositeDate = "Please Enter Deposite Date";
    amount = "Please Enter Amount";
    refno = "Please enter Ref No";
    paysuccess = "Payment Successful";
    paydescp = "Enter Description";
    payexcess = "Paid Amount is Greater than Payable Amount "

    // ORDERED PRODUCTS ASSIGNING TO VENDOR
    prodAssigned = "Assigned to Vendor";
    vndSelected = "Products Are Already Assigned";
    productsShipped = "Products Shipped";
    delivered = "Items Delivered"
    selVendor = "Select vendor Name";
    cancorders = "Order Cancelled";
    acceptorder = "Order Accepted";

    orderproducts = "Your shipping cart is empty.";

    //PORTAL ADMIN
    prtlprofUpdated = "Portal Profile Updated";

    //signin
    //  Invalid_Login = "Your login information is incorrect. Please try again."
    Invalid_Login = "Invalid Credentials Please try again."
    User_Inactive = "User Is InActive";

    prodassigntoVnd = "Products Assigned to Vendor "

    orderCreated = "Order created successfully."
    orderUnsuccessfull = "Order Unsuccessfull, Try Again"
    orderCancelled = "Order Cancelled successfully."

    // CHANGE PASSWORD
    changePassword = "Password Changed Sucessfully";
    oldPassword = "Enter Old Password";
    newPassword = "Enter New Password";
    confirmPassword = "Enter Confirm Password";
    pswdmismatch = "Password and ConfimPassword don't match";
    pswdexists = "Old Password Should be different from New Password";

    // PORTAL ORDERS
    entfmdt ="Select From Date";
    enttodt ="Select To Date";
    selstatus = "Select Status";
    selcustname = 'Select a Customer';

    validate_empty_text(value, message, id) {
        if (value == '' || value == null) {
            this.toast.showwarning(message)
            this.renderer2.selectRootElement(id).focus();
            return true;
        } else {
            return false;
        }
    }

    validate_select_element(value, message, element) {
        if (value == '' || value == null) {
            this.toast.showwarning(message)
            element.focus();
            return true;
        } else {
            return false;
        }
    }

    validate_number(value, message, id) {
        if (!value.match(this.numbersmatch)) {
            this.toast.showwarning(message)
            this.renderer2.selectRootElement(id).focus();
            return true
        } else {
            return false
        }
    }

    validate_email(value, message, id) {
        if (!value.match(this.emailformat)) {
            this.toast.showwarning(message)
            this.renderer2.selectRootElement(id).focus();

            return true
        } else {
            return false
        }
    }

    validate_price(value, message, id) {
        if (!value.match(this.validatepriceformat)) {
            this.toast.showwarning(message)
            this.renderer2.selectRootElement(id).focus();
            return true
        } else {
            return false
        }
    }


      //Exception Errors
  errMessage500 = 'Internal Server Error, Please Try After Sometime';
  errMessage404 = 'Page Not Found';
  errMessage400 = 'Bad Request';
  errMessage408 = 'Request Timeout';
  errMessage503 = 'Service Unavailable';
  errMessage504 = 'Gateway Timeout';
  errMessage401 = 'UnAuthorized';
  errMessage405 = 'Method Not Allowed';





}