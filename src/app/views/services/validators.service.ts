import { Injectable } from '@angular/core';
@Injectable()
export class ValidatorService {
  constructor() { }
  // ALLOWING ONLY NUMBERS
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // ONLY NUMERICS AND ALPHABETS
  nosplChar(event): boolean {
    var upperCase = new RegExp('[A-Z]');
    var lowerCase = new RegExp('^[a-z]');
    var numbers = new RegExp('^[0-9]');
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode != 32)) {
      return false;
    }
    return true;
  }
  //  EMAIL VALIDATIONS ALLOWING CHARACTERS & NUMERICS & @ & .
  validateEmail(event): boolean {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var upperCase = new RegExp('[A-Z]');
    var lowerCase = new RegExp('^[a-z]');
    var numbers = new RegExp('^[0-9]');
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode != 32) && (charCode != 64) && (charCode != 46)) {
      return false
    }
    return true
  }

  // ONLY  ALPHABETS
  alphabetsonly(event): boolean {
    var upperCase = new RegExp('[A-Z]');
    var lowerCase = new RegExp('^[a-z]');
    var numbers = new RegExp('^[0-9]');
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 &&  (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode != 32)) {
      return false;
    }
    return true;
  }
}