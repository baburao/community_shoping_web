import { Routes } from '@angular/router';

import { ProductCategoryComponent } from './productcategory.component';


export const ProductCategoryRoutes: Routes = [
  { path: '', component: ProductCategoryComponent }
];