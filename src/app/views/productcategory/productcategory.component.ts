//NAME : BABURAO M
//DESCRIPTION : PRODUCT CATEGORY INSERT AND MODIFICATION DATA
//DATE : 09/05/2019



import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup, MatExpansionPanel } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';



@Component({
  selector: 'app-login',
  templateUrl: './productcategory.component.html',
  styleUrls: ['./productcategory.component.css']
})
export class ProductCategoryComponent implements OnInit {



  productcategorytable: boolean;
  noRecords: boolean;
  productCategoryList;
  allprodCategoryList: any;
  //CATEGORY OBJECT
  categoryObj = {
    category_name: '',
    category_id: '',
    created_by: '',
    updated_by: ''
  }
  constructor(private router: Router, private service: ApiService,
    private message: MessageService,
    private toast: ToasterService,
    private dialog: MatDialog,
    @Inject(LOCAL_STORAGE) private storage: StorageService, private spinner: NgxSpinnerService,
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['category_name', 'Actions'];

  ngOnInit() {
    this.spinner.show();
    this.getCategoryList();
   
  }

  //GET PRODUCT CATEGORY LIST DATA
  getCategoryList() {
    this.service.post(this.service.getallcategory, '').subscribe((res) => {
      if (res.length >= 1) {
        this.productCategoryList = res;
        this.allprodCategoryList = res;
        this.productCategoryList = new MatTableDataSource(res);
        this.productCategoryList.paginator = this.paginator;
        this.spinner.hide();
        this.productcategorytable = false;
        this.noRecords = true;
      } else {
        this.productcategorytable = true;
        this.noRecords = false;
        this.spinner.hide();
      }
    });
  }
  //PRODUCT CATEGORY ADD BUTTON
  addProductCategory() {
    this.openDialog1('add');
  }
  //INACTIVE PRODUCT CATEGORY
  inactiveProductCategoryName(productCategoryId) {
    this.spinner.show();
    let statusProductCategoryObj = {
      category_id: productCategoryId,
      is_active: "N"
    }
    this.service.post(this.service.updatecategorystatus, statusProductCategoryObj).subscribe((res) => {
      this.toast.showsuccess(this.message.deactiveProductCategory);
      this.getCategoryList();
    });
  }

  //ACTIVE PRODUCT CATEGORY
  activeProductCategoryName(productCategoryId) {
    this.spinner.show();
    let statusProductCategoryObj = {
      category_id: productCategoryId,
      is_active: "Y"
    }
    this.service.post(this.service.updatecategorystatus, statusProductCategoryObj).subscribe((res) => {
      this.toast.showsuccess(this.message.activeProductCategory);
      this.getCategoryList();
    });
  }
  //EDIT PRODUCT CATEGORY LIST
  editProductCategory(categoryid) {
    this.spinner.show();
    let editProductCategoryObj = {
      category_id: categoryid,
    }
    this.service.post(this.service.getcategorybyid, editProductCategoryObj).subscribe((res) => {
      this.categoryObj.category_name = res[0]['category_name'];
      this.categoryObj.category_id = res[0]['category_id'];
      this.spinner.hide();
      this.openDialog1('edit');
    });
  }
  //DIALOG BOX
  openDialog1(val): void {

    const dialogRef = this.dialog.open(ProductCategoryDialog, {
      width: '750px',
      disableClose: true,
      data: {
        status: val,
        category_id: this.categoryObj.category_id,
        category_name: this.categoryObj.category_name,
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      this.getCategoryList();
      this.categoryObj.category_id = '';
      this.categoryObj.category_name = '';
      this.categoryObj.created_by = '';
      this.categoryObj.updated_by = '';
    });
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allprodCategoryList.filter(function (categiry) {
        return (
          categiry.category_name.toLowerCase().indexOf(event.toLowerCase()) > -1
        )
      });
      if (temp.length !== 0) {
        this.productCategoryList = new MatTableDataSource(temp);
        this.productCategoryList.paginator = this.paginator;
        this.productcategorytable = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.productcategorytable = true;
        this.noRecords = false;
        this.productCategoryList = new MatTableDataSource(temp);
        this.productCategoryList.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.productCategoryList = this.allprodCategoryList;
      this.productCategoryList = new MatTableDataSource(this.allprodCategoryList);
      this.productCategoryList.paginator = this.paginator;
      this.productcategorytable = false;
      this.noRecords = true;
    }
  }
  
  

}

@Component({
  selector: 'productcategory',
  templateUrl: 'productcategory.html',
})
export class ProductCategoryDialog {
  constructor(private service: ApiService,
    public dialogRef: MatDialogRef<ProductCategoryDialog>, private validations: ValidatorService,
    private message: MessageService,
    private toast: ToasterService,
    private spinner: NgxSpinnerService,
    
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit() {
  }
  Closedailog() {
    this.dialogRef.close();
  }
  //ADDING CATEGORY
  addprodcategory(ctgName) {
    if (ctgName == '' || ctgName == undefined) {
      this.toast.showwarning(this.message.productCategoryName);
      return
    } else {
      this.spinner.show();
      let categoryObj = {
        category_name: ctgName,
        created_by: this.storage.get('portal_admin_id'),
        updated_by: this.storage.get('portal_admin_id')
      }
      this.service.post(this.service.insertcategory, categoryObj).subscribe(res => {
        this.spinner.hide();
        if (res == "Duplicate") {
          this.toast.showwarning(this.message.duplicateproductCategoryName);

        } else {
          this.toast.showsuccess(this.message.insertProductCategory);
          this.Closedailog();
        }
      })
    }
  }
  // EDITING CATEGORY
  editprodcategory(ctgId, ctgName) {
    if (ctgName == '' || ctgName == undefined) {
      this.toast.showwarning(this.message.productCategoryName);
      return
    } else {
      this.spinner.show();
      let categoryObj = {
        category_id: ctgId,
        category_name: ctgName,
        created_by: this.storage.get('portal_admin_id'),
        updated_by: this.storage.get('portal_admin_id')
      }
      this.service.post(this.service.updatecategoryname, categoryObj).subscribe(res => {
        this.spinner.hide();
        if (res == "Duplicate") {
          this.toast.showwarning(this.message.duplicateproductCategoryName);

        } else {
          this.toast.showsuccess(this.message.updateProductCategoryName);
          this.Closedailog();
        }
      })
    }
  }

  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }
}

export interface DialogData {
  category_id;
  category_name;
  status,
}