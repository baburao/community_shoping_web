import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxLoadingModule } from 'ngx-loading';
//mport {MessageService } from '../services/message.service';
//import { ToasterService } from '../services/toaster.service';
import {
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatProgressBarModule,
  MatButtonModule,  
  MatChipsModule,
  MatListModule,
  MatGridListModule,
  MatTabsModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,

} from '@angular/material';
import { ProductCategoryComponent,ProductCategoryDialog } from './productcategory.component';
import { ProductCategoryRoutes } from './productcategory.routing';
// import { CommonDirectivesModule } from './sdirectives/common/common-directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatProgressBarModule,
    MatButtonModule,  
    MatChipsModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
  
    

    RouterModule.forChild(ProductCategoryRoutes),
    NgxLoadingModule.forRoot({})
  ],
  declarations: [ProductCategoryComponent,ProductCategoryDialog],
  providers: [
    
    //MessageService
   
  ],
  entryComponents:[ProductCategoryDialog]
})
export class ProductCategoryModule { }