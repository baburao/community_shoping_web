import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ApiService } from '../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-userissue',
  templateUrl: './userissue.component.html',
  styleUrls: ['./userissue.component.scss']
})
export class UsserIssueComponent implements OnInit {
  vndproducts: any;
  orderid:'';
  prodName:'';
  subject:'';
  description:'';
  userRole:'';
  countProducts = 0;
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, private service: ApiService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    if((this.storage.get("cartitem")==null)) {
      this.countProducts = 0;
    }else{
      this.countProducts = this.storage.get("cartitem").length;
    }
    let userId = this.storage.get('user_id');
    this.spinner.hide();

  }
  insertIssue(odId, prodName, Sub, Descrip) {
    let userRole = this.storage.get('userrole');
    let userId = this.storage.get('user_id');
    let usrissue = {
      "usrOrdId": odId,
      "usrProdName": prodName,
      "usrSubject": Sub,
      "usrDescp": Descrip,
      "role": userRole,
      "user_id": userId
    }
    this.service.post(this.service.insertusrissue, usrissue).subscribe(res => {
      this.orderid = '';
      this.prodName = '';
      this.subject = '';
      this.description = '';
    })
  }


}
