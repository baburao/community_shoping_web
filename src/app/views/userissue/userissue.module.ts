import { Userissuesroutes } from './usserissue.routing';
import { ApiService } from '../services/api.service';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { UsserIssueComponent } from './userissue.component';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms'

import {
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatExpansionModule,
    MatSliderModule,
    MatSidenavModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatRippleModule,
    MatTabsModule,
    MatSnackBarModule
} from '@angular/material';
import { ToasterService } from '../services/toaster.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StarRatingModule } from 'angular-star-rating';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatOptionModule,
        MatCardModule,
        MatListModule,
        MatToolbarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        CommonModule,
        MatInputModule,
        FormsModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatExpansionModule,
        MatSliderModule,

        CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatRippleModule,
    MatTabsModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatListModule,
    MatSidenavModule,
    StarRatingModule,
    NgxPaginationModule,
    NgxDatatableModule,
    RouterModule.forChild(Userissuesroutes),
    ],
    providers: [ApiService,ToasterService],
    declarations: [UsserIssueComponent
    ]
})

export class UserIssueModule { }