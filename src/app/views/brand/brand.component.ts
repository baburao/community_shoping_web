//NAME : BABURAO M
//DESCRIPTION : ADDING BRAND AND MODIFICATIONS
//DATE : 08/05/2019


import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener, TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup, MatExpansionPanel } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';

// LOADER CODE HERE
const PrimaryWhite = '#ffffff';
const SecondaryGrey = '#ccc';
const PrimaryRed = '#dd0031';
const SecondaryBlue = '#006ddd';
//END
@Component({
  selector: 'app-login',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {



  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;


  brandstable: boolean;
  noRecords: boolean;
  brandList: any;
  allbrandlist: any;
  lengthofevent = 0;
  //INSERT BRAND OBJECT
  brandObj = {
    brand_name: '',
    brand_id: '',
    created_by: '',
    updated_by: ''
  }

  constructor(private router: Router, private service: ApiService,
    private message: MessageService,

    private toast: ToasterService,
    public dialog: MatDialog, private spinner: NgxSpinnerService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['brand_name', 'Actions'];

  ngOnInit() {
    this.spinner.show();
    this.getBrandList();
  }

  //GET BRANDS LIST
  getBrandList() {
    this.service.post(this.service.getallbrand, '').subscribe((res) => {
      if (res.length >= 1) {
        this.brandList = res;
        this.allbrandlist = res;
        this.brandList = new MatTableDataSource(res);
        this.brandList.paginator = this.paginator;
        this.spinner.hide();
        this.brandstable = false;
        this.noRecords = true;
      } else {
        this.spinner.hide();
        this.brandstable = true;
        this.noRecords = false;
      }
    });
  }

  //INSERT BRAND DATA
  addBrand() {
    this.openDialog1('add');
    let userpage = "add";
    this.storage.set('userpage', userpage);
  }

  //INACTIVE BRANDS
  inactiveBrand(brand_id) {
    this.spinner.show();
    let statusBrandObj = {
      brand_id: brand_id,
      is_active: "N"
    }
    this.service.post(this.service.updatebrandstatus, statusBrandObj).subscribe((res) => {
      this.toast.showsuccess(this.message.deactiveBrand);
      this.getBrandList();
    });
  }

  //ACTIVE BRANDS
  activeBrand(brand_id) {
    this.spinner.show();
    let statusBrandObj = {
      brand_id: brand_id,
      is_active: "Y"
    }
    this.service.post(this.service.updatebrandstatus, statusBrandObj).subscribe((res) => {
      this.toast.showsuccess(this.message.activeBrand);
      this.getBrandList();
    });
  }


  //EDIT BRAND
  editbrand(brand_id) {
    this.spinner.show();
    let editBrandObj = {
      brand_id: brand_id,
    }
    this.service.post(this.service.getbrandbyid, editBrandObj).subscribe((res) => {
      this.brandObj.brand_name = res[0]['brand_name'];
      this.brandObj.brand_id = res[0]['brand_id'];
      this.openDialog1('edit');
      let userpage = "edit";
      this.storage.set('userpage', userpage);
      this.spinner.hide();
    });
  }



  openDialog1(val): void {

    const dialogRef = this.dialog.open(BrandsDialog, {
      width: '750px',
      disableClose: true,
      data: {
        brand_id: this.brandObj.brand_id,
        brand_name: this.brandObj.brand_name,
        created_by: this.brandObj.created_by,
        updated_by: this.brandObj.updated_by,
        status: val

      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getBrandList();
      this.brandObj.brand_name = '';
    });
  }
  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.allbrandlist.filter(function (categiry) {
        return (
          categiry.brand_name.toLowerCase().indexOf(event.toLowerCase()) > -1)
      });
      if (temp.length !== 0) {
        this.brandList = new MatTableDataSource(temp);
        this.brandList.paginator = this.paginator;
        this.brandstable = false;
        this.noRecords = true;
      } else if (temp.length == 0) {
        this.brandstable = true;
        this.noRecords = false;
        this.brandList = new MatTableDataSource(temp);
        this.brandList.paginator = this.paginator;
      }
    } else if (eventlength <= this.lengthofevent) {
      this.brandList = this.allbrandlist;
      this.brandList = new MatTableDataSource(this.allbrandlist);
      this.brandList.paginator = this.paginator;
      this.brandstable = false;
      this.noRecords = true;
    }
  }
}

@Component({
  selector: 'brands',
  templateUrl: 'brands.html',
})
export class BrandsDialog {

  constructor(private service: ApiService,
    public dialogRef: MatDialogRef<BrandsDialog>,
    private toast: ToasterService, private message: MessageService,
    private validations: ValidatorService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }
  userpage;
  ngOnInit() {
    this.userpage = this.storage.get('userpage');
    if (this.userpage == "add") {
      this.data.brand_name = '';
    } else {

    }
  }
  // CLOSING DIALOG
  onNoClick(): void {
    this.dialogRef.close();
  }
  // ADDING AND UPDATING BRAND NAMES
  brandsform(brand_id, brand_name, status) {
    if (brand_name == null || brand_name == '') {
      this.toast.showwarning(this.message.enterbrand);
      return
    } else if (status == 'add') {
      let brandObj = {
        "brand_name": brand_name,
        "created_by": this.storage.get('portal_admin_id'),
        "updated_by": this.storage.get('portal_admin_id'),
      }
      this.service.post(this.service.insertbrand, brandObj).subscribe(res => {
        if (res == "Inserted") {
          this.toast.showsuccess(this.message.insertbrands);
          this.onNoClick();
        }
        else {
          this.toast.showwarning(this.message.duplicateBrand);
        }
      })
    }
    else {
      let brandObj = {
        "brand_name": brand_name,
        "brand_id": brand_id,
        "updated_by": this.storage.get('portal_admin_id'),
      }
      this.service.post(this.service.updatebrandname, brandObj).subscribe(res => {
        if (res == 'Updated') {
          this.toast.showsuccess(this.message.updateBrand);
          this.onNoClick();
        } else {
          this.toast.showwarning(this.message.duplicateBrand);
        }
      })
    }

  }

  // RETURN ALPHABETS
  returnalphaonly(event): boolean {
    return this.validations.alphabetsonly(event)
  }



}
export interface DialogData {
  brand_name;
  brand_id;
  status;

}
