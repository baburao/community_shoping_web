import { Routes } from '@angular/router';

import { BrandComponent } from './brand.component';


export const BrandRoutes: Routes = [
  { path: '', component: BrandComponent }
];