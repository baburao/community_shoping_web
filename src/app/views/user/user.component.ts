//NAME : BABURAO M
//DESCRIPTION : USER CREATIONS AND MODIFICATIONS
//DATE : 12/05/2019


import { Component, ViewEncapsulation, OnInit, Input, Output, SimpleChange, EventEmitter, OnChanges, ViewChild, Inject, ChangeDetectorRef, HostListener } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup, MatDialogConfig } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';

import { MatAutocompleteModule, MatPaginator, MatTableDataSource, MatDatepickerInputEvent, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MessageService } from '../services/message.service';
import { ToasterService } from '../services/toaster.service';

import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidatorService } from '../services/validators.service';
@Component({
  selector: 'app-login',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


  userData;
  userslist: any;
  user_id;
  usertable: boolean;
  usrnorecords: boolean;
  //USER OBJECT
  userObj = {
    first_name: '',
    last_name: '',
    phone_no: '',
    email_id: '',
    password: '',
    re_password: '',
    user_id: '',
    created_by: '',
    updated_by: '',
    community_id: '',
    block_name: '',
    flat_no: '',
  }

  constructor(private router: Router, private service: ApiService,
    public dialogue: MatDialog,
    private message: MessageService,
    private toast: ToasterService,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['name', 'phone_no', 'email_id', 'community_name', 'block_name', 'flat_no', 'Actions'];

  ngOnInit() {
    this.spinner.show();
    this.getuserlisrdata();
    this.usertable = false;
  }

  //get user lisr data
  getuserlisrdata() {

    this.service.post(this.service.getalluser, '').subscribe((res) => {

      this.userData = res;
      this.userslist = res;
      this.userData = new MatTableDataSource(res);
      this.userData.paginator = this.paginator;
      if (res.length != 0) {
        this.usertable = false;
        this.usrnorecords = true;
      }
      else {
        this.usertable = true;
        this.usrnorecords = false;
      }
      this.spinner.hide();
    });
  }


  //ADD USER BUTTON
  addUser() {
    this.openDialog1('add');
  }
  //INACTIVE USER
  inactiveUser(userid) {
    this.spinner.show();
    let statusUserObj = {
      user_id: userid,
      is_active: "N"
    }
    this.service.post(this.service.updateuserstatus, statusUserObj).subscribe((res) => {
      this.toast.showsuccess(this.message.usrinActive);
      this.getuserlisrdata();
    });
  }

  //ACTIVE USER
  activeUser(userid) {
    this.spinner.show();
    let statusUserObj = {
      user_id: userid,
      is_active: "Y"
    }

    this.service.post(this.service.updateuserstatus, statusUserObj).subscribe((res) => {
      this.toast.showsuccess(this.message.usrActive);
      this.getuserlisrdata();
    });
  }

  //eEDIT USER
  editUser(userid) {
    this.spinner.show();
    let editUserObj = {
      user_id: userid,

    }
    this.user_id = userid;

    this.service.post(this.service.getuserbyid, editUserObj).subscribe((res) => {
      this.userObj.user_id = res[0]['user_id'];
      this.userObj.first_name = res[0]['first_name'];
      this.userObj.last_name = res[0]['last_name'];
      this.userObj.phone_no = res[0]['phone_no'];
      this.userObj.email_id = res[0]['email_id'];
      this.userObj.password = res[0]['password'];
      this.userObj.re_password = res[0]['re_password'];
      this.userObj.community_id = res[0]['community_id'];
      this.userObj.block_name = res[0]['block_name'];
      this.userObj.flat_no = res[0]['flat_no'];
      this.spinner.hide();
      this.openDialog1('edit');
    });
  }


  //MODEL DIALOG CODE HERE
  openDialog1(val): void {

    const dialogRef = this.dialog.open(UserDialog, {
      width: '750px',
      disableClose: true,
      data: {
        user_id: this.userObj.user_id,
        first_name: this.userObj.first_name,
        last_name: this.userObj.last_name,
        phone_no: this.userObj.phone_no,
        community_id: this.userObj.community_id,
        flat_no: this.userObj.flat_no,
        block_name: this.userObj.block_name,
        password: this.userObj.password,
        re_password: this.userObj.re_password,
        email_id: this.userObj.email_id,
        status :val
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      this.getuserlisrdata();
      this.userObj.user_id = '';
      this.userObj.first_name = '';
      this.userObj.last_name = '';
      this.userObj.phone_no = '';
      this.userObj.community_id = '';
      this.userObj.flat_no = '';
      this.userObj.block_name = '';
      this.userObj.password = '';
      this.userObj.re_password = '';
      this.userObj.email_id = '';
    });
  }

  // SEARCH FUNCTIONALITY
  applyFilter(event) {
    let lengthofevent = 0;
    let eventlength = event.length;
    if (eventlength != "" && eventlength > lengthofevent) {
      let temp = this.userslist.filter(function (categiry) {
        return (
          categiry.community_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.email_id.indexOf(event) > -1 ||
          categiry.name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.flat_no.indexOf(event) > -1 ||
          categiry.block_name.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          categiry.phone_no.indexOf(event) > -1
        )
      });
      if (temp.length !== 0) {
        this.userData = new MatTableDataSource(temp);
        this.userData.paginator = this.paginator;
        this.usertable = false;
        this.usrnorecords = true;
      } else if (temp.length == 0) {
        this.usertable = true;
        this.usrnorecords = false;
        this.userData = new MatTableDataSource(temp);
        this.userData.paginator = this.paginator;
      }
    } else if (eventlength <= lengthofevent) {
      this.userData = this.userslist;
      this.userData = new MatTableDataSource(this.userslist);
      this.userData.paginator = this.paginator;
      this.usertable = false;
      this.usrnorecords = true;
    }
  }


}

@Component({
  selector: 'user-model',
  templateUrl: 'usersmodel.html',
})
export class UserDialog {
  constructor(private service: ApiService,
    public dialogRef: MatDialogRef<UserDialog>,
    private toast: ToasterService, private message: MessageService, private validations: ValidatorService,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }
  states;
  cityes;
  city_id;
  state_id;
  pwd: boolean;
  repwd: boolean;
  communitydata;
  community_id;

  ngOnInit() {
    if (this.data.status == "add") {
      this.pwd = false;
      this.repwd = false;
      this.data.user_id = '';
      this.data.first_name = '';
      this.data.last_name = '';
      this.data.phone_no = '';
      this.data.flat_no = '';
      this.data.block_name = '';
      this.data.password = '';
      this.data.re_password = '';
      this.data.email_id = '';
    } else {
      this.pwd = true;
      this.repwd = true;
    }
    this.getCommunityData();
  }
  //GER COMMUNITITY DATA
  getCommunityData() {
    this.service.post(this.service.getactivecommunity, '').subscribe((res) => {
      this.communitydata = res;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  //GET COMMUNITY ID
  getcommunityid(communityid) {
    this.community_id = communityid
  }

  // INSERTING AND UPDATING FORM
  userform(user_id, first_name, last_name, phone_no, community_id, block_name, flat_no, email_id, password, re_password, status) {
    if (first_name == null || first_name == '') {
      this.toast.showwarning(this.message.usrftName);
      return
    } else if (last_name == null || last_name == '') {
      this.toast.showwarning(this.message.usrltName);
      return
    } else if (phone_no == null || phone_no == '') {
      this.toast.showwarning(this.message.usrNum);
      return
    } else if (community_id == null || community_id == '') {
      this.toast.showwarning(this.message.usrCmtId);
      return
    } else if (block_name == null || block_name == '') {
      this.toast.showwarning(this.message.usrBlkName);
      return
    } else if (flat_no == null || flat_no == '') {
      this.toast.showwarning(this.message.usrFltNum);
      return
    } else if (status == 'add') {
      if (email_id == null || email_id == '') {
        this.toast.showwarning(this.message.usrEmail);
        return
      } else if (password == null || password == '') {
        this.toast.showwarning(this.message.usrPswd);
        return
      } else if (re_password == null || re_password == '') {
        this.toast.showwarning(this.message.usrRePswd);
        return
      } else if (password != re_password) {
        this.toast.showwarning(this.message.pswdmismatch);
        return
      }
      else {
        let usrObj = {
          "first_name": first_name,
          "last_name": last_name,
          "phone_no": phone_no,
          "email_id": email_id,
          "password": password,
          "community_id": community_id,
          "block_name": block_name,
          "flat_no": flat_no,
          "created_by": this.storage.get('portal_admin_id'),
          "updated_by": this.storage.get('portal_admin_id')
        }
        this.service.post(this.service.insertuser, usrObj).subscribe((res) => {
          if (res == 'Inserted') {
            this.onNoClick()
            this.toast.showsuccess(this.message.usrCreate);
          }
          else {
            this.toast.showwarning(this.message.usrDuplicate);
          }
        })
      }
    } else {
      let usrObj = {
        "first_name": first_name,
        "last_name": last_name,
        "phone_no": phone_no,
        "email_id": email_id,
        "community_id": community_id,
        "block_name": block_name,
        "flat_no": flat_no,
        "updated_by": this.storage.get('portal_admin_id'),
        "user_id": user_id,
      }
      this.service.post(this.service.updateuserdetails, usrObj).subscribe((res) => {
        if (res == 'Updated') {
          this.onNoClick()
          this.toast.showsuccess(this.message.usrUpdate);
        }
        else {
          this.toast.showwarning(this.message.usrDuplicate);
        }
      })
    }
  }


  // RETURN ONLY NUMBERS
  returnnumber(event): boolean {
    return this.validations.numberOnly(event)
  }

  // CHECKING EMAIL VALIDATION
  chkemail(event): boolean {
    return this.validations.validateEmail(event)
  }
  // RETURN ALPHANUMERIC
  returnnosplchar(event): boolean {
    return this.validations.nosplChar(event)
  }
}
export interface DialogData {
  user_id;
  first_name;
  last_name;
  phone_no;
  community_id;
  block_name;
  flat_no;
  email_id;
  password;
  re_password;
  status;
}
