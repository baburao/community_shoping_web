//NAME : BABURAO M
//DESCRIPTION : DISPLAY STATE DATA
//DATE : 10/05/2019

import { Component, OnInit, ViewChild,TemplateRef } from '@angular/core';
import { MatProgressBar, MatButton, MatRadioButton, MatRadioGroup } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AppLoaderService } from '../../shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSelect, MatDialog } from '@angular/material';
import { ngxLoadingAnimationTypes, NgxLoadingComponent } from 'ngx-loading';
// LOADER CODE HERE
const PrimaryWhite = '#ffffff';
const SecondaryGrey = '#ccc';
const PrimaryRed = '#dd0031';
const SecondaryBlue = '#006ddd';
//END
@Component({
  selector: 'app-login',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {

  //LOADER CODE HERE
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>;
  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  public loading = true;
  public primaryColour = PrimaryWhite;
  public secondaryColour = SecondaryGrey;
  public coloursEnabled = false;
  public loadingTemplate: TemplateRef<any>;
  public config = { animationType: ngxLoadingAnimationTypes.none, primaryColour: this.primaryColour, secondaryColour: this.secondaryColour, tertiaryColour: this.primaryColour, backdropBorderRadius: '3px' };

  //END

  tableData: any;


  constructor(private router: Router, private loader: AppLoaderService, private service: ApiService,

  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['state_id', 'state_name'];

  ngOnInit() {
    this.loading = true;
    this.service.post(this.service.getallstate, '').subscribe((res) => {
      this.tableData = res;
      this.tableData = new MatTableDataSource(res);
      this.tableData.paginator = this.paginator;
      this.loading = false;
    });
  }
}
