import { Routes } from '@angular/router';

import { StateComponent } from './state.component';


export const StateRoutes: Routes = [
  { path: '', component: StateComponent }
];