// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  apiURL: 'developmentApi',
  templatesURL: 'http://qa.gathi.in/assets/templates/',

  // logApiURL: 'http://183.82.98.147:9500/api/log/',
  // staticURL: 'http://183.82.98.147:9500/static/',
  // apiEndpoint: 'http://183.82.98.147:9500/api/',


  apiEndpoint: ' http://127.0.0.1:9000/api/',
  logApiURL: 'http://127.0.0.1:9000/api/log/',
  staticURL: 'http://127.0.0.1:9000/static/',
};
